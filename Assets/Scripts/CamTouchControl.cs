﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CamTouchControl : MonoBehaviour {
    [SerializeField]
    private LineRenderer SelectionLine;
    private Camera thisCam;
    public float ZOffset = 0.0f;
	// Use this for initialization
	void Start () {
        thisCam = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
        // Track a single touch as a direction control.
        if (Input.touchCount == 2)
        {
            Touch touchA = Input.GetTouch(0);
            Touch touchB = Input.GetTouch(1);
            Vector3 CameraStartPos = thisCam.transform.position;
            Vector3 startPos = new Vector3(0, 0, 0);
            Vector3 endPos = new Vector3(0, 0, 0);
            float originalScale = thisCam.orthographicSize;
            float startDistance = 1.0f;
            float endDistance = 1.0f;

            // Handle finger movements based on TouchPhase
            switch (touchB.phase)
            {
                //When a touch has first been detected, change the message and record the starting position
                case TouchPhase.Began:
                    // Record initial touch position.
                    startPos = thisCam.ScreenToWorldPoint((touchA.position + touchB.position)/2.0f);
                    startDistance = Vector2.Distance(touchA.position, touchB.position);
                    break;

                //Determine if the touch is a moving touch
                case TouchPhase.Moved:
                    // Determine direction by comparing the current touch position with the initial one
                    endPos = thisCam.ScreenToWorldPoint((touchA.position + touchB.position) / 2.0f);
                    thisCam.transform.position = new Vector3(CameraStartPos.x + (startPos.x - endPos.x), CameraStartPos.y + (startPos.y - endPos.y), CameraStartPos.z);
                    endDistance = Vector2.Distance(touchA.position, touchB.position);
                    thisCam.orthographicSize = originalScale * (startDistance / endDistance);
                    break;

                case TouchPhase.Ended:
                    // Report that the touch has ended when it ends
                    break;
            }
        }
        else if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            Vector3 startPos = new Vector3(0, 0, 0);
            Vector3 endPos = new Vector3(0, 0, 0);

            // Handle finger movements based on TouchPhase
            switch (touch.phase)
            {
                //When a touch has first been detected, change the message and record the starting position
                case TouchPhase.Began:
                    // Record initial touch position.
                    startPos = thisCam.ScreenToWorldPoint(touch.position);
                    startPos = new Vector3(startPos.x, startPos.y, ZOffset);
                    break;

                //Determine if the touch is a moving touch
                case TouchPhase.Moved:
                    // Determine direction by comparing the current touch position with the initial one
                    endPos = thisCam.ScreenToWorldPoint(touch.position);
                    endPos = new Vector3(startPos.x, startPos.y, ZOffset);
                    break;

                case TouchPhase.Ended:
                    // Report that the touch has ended when it ends
                    break;
            }
        }

    }
}
