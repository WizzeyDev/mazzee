﻿using UnityEngine;
using UnityEngine.UI;
using Vuforia;
using System.Collections;

public class DataScanner : MonoBehaviour, ICloudRecoEventHandler
{
    private CloudRecoBehaviour mCloudRecoBehaviour;
    public ImageTargetBehaviour ImageTargetTemplate;
    private bool mIsScanning = false;
    private string mTargetMetadata = "";
    public Text CloudStatusText;
    Coroutine statusCorotine;

    // Use this for initialization
    void Start()
    {
       // ump.Path = "file:///" + Application.persistentDataPath + @"\" + "Video1.mp4";
        // register this event handler at the cloud reco behaviour
        mCloudRecoBehaviour = GetComponent<CloudRecoBehaviour>();

        if (mCloudRecoBehaviour)
        {
            mCloudRecoBehaviour.RegisterEventHandler(this);
        }
    }

    public void OnInitError(TargetFinder.InitState initError)
    {
        Debug.Log("Cloud initialization error " + initError.ToString());
        CloudStatusText.text = "Cloud initialization error \n" + initError.ToString();
    }

    public void OnInitialized()
    {
        Debug.Log("Cloud Reco initialized");
        if (!CloudStatusText.text.Contains("SUSPENDED"))
        {
            if (statusCorotine != null)
                StopCoroutine(statusCorotine);
            statusCorotine = StartCoroutine(showStatus(0, "Connecting to " + gameObject.name + "...", 5));
        }
    }

    public void OnNewSearchResult(TargetFinder.TargetSearchResult targetSearchResult)
    {
        CloudStatusText.text = "Detected Target: \n" + targetSearchResult.TargetName + ",\n ID: "+targetSearchResult.UniqueTargetId;
        // do something with the target metadata
        TargetFinder.CloudRecoSearchResult cloudResult = (TargetFinder.CloudRecoSearchResult)targetSearchResult;
        mTargetMetadata = cloudResult.MetaData;
        // stop the target finder (i.e. stop scanning the cloud)
        mCloudRecoBehaviour.CloudRecoEnabled = true;
        // int linkNum = int.Parse(targetSearchResult.TargetName.Split('_')[1]);
        ImageTargetBehaviour newTarget = Instantiate(ImageTargetTemplate);
        if (newTarget)
        {
            // enable the new result with the same ImageTargetBehaviour:
            ObjectTracker tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
            ImageTargetBehaviour imageTargetBehaviour =
                (ImageTargetBehaviour)tracker.TargetFinder.EnableTracking(
                    targetSearchResult, newTarget.gameObject);
            newTarget.gameObject.name = targetSearchResult.TargetName;
            Debug.Log("SERVER-DATA TARGET DETECTED: " + targetSearchResult.TargetName);
            ServerData ServerDataConnectionJson = new ServerData();
            ServerDataConnectionJson = JsonUtility.FromJson<ServerData>(mTargetMetadata);
            if (ServerDataConnectionJson != null)
            {
                StartCoroutine(setupServerConnection(cloudResult.MetaData, targetSearchResult.TargetName, targetSearchResult.UniqueTargetId));
            }
            //newTarget.GetComponent<VideoTrackableEventHandler>().getVideoLinkFromID(targetSearchResult.UniqueTargetId);
        }
    }

    public void OnStateChanged(bool scanning)
    {
    }

    public void OnUpdateError(TargetFinder.UpdateState updateError)
    {
        Debug.Log("Cloud Reco update error " + updateError.ToString());
        if (updateError.ToString().Contains("SUSPEND"))
        {
           // spearCloudObject.SetActive(true);
           // gameObject.SetActive(false);
            if (!CloudStatusText.text.Contains("SUSPENDED"))
            {
                if (statusCorotine != null)
                    StopCoroutine(statusCorotine);
                statusCorotine = StartCoroutine(showStatus(0, gameObject.name + " is <b><color=red>SUSPENDED!</color></b>\nTrying to reconnect in ", 10, 10));
            }
        }
        else if (updateError.ToString().Contains("SERVICE_NOT_AVAILABLE"))
        {
            if (!CloudStatusText.text.Contains("No Internet"))
            {
                if (statusCorotine != null)
                    StopCoroutine(statusCorotine);
                statusCorotine = StartCoroutine(showStatus(0, "<b><color=red>No Internet Connection!</color></b>\nTrying to reconnect in ", 10, 10));
            }
        }
        else
        {
            if (statusCorotine != null)
                StopCoroutine(statusCorotine);
            statusCorotine = StartCoroutine(showStatus(0, "<b><color=red>ERROR!\n" + updateError.ToString() + "</color></b>", 5));
        }
        CloudStatusText.text = updateError.ToString();
    }

    IEnumerator showStatus(float waitBefore, string shownText, float showTime)
    {
        yield return new WaitForSeconds(waitBefore);
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / showTime)
        {
            CloudStatusText.text = shownText;
            yield return null;
        }
        CloudStatusText.text = "";
    }

    IEnumerator setupServerConnection(string metadata, string name, string id)
    {
        yield return new WaitForSeconds(1.5f);
        ServerData ServerDataConnectionJson = new ServerData();
        ServerDataConnectionJson = JsonUtility.FromJson<ServerData>(metadata);
        if (string.IsNullOrEmpty(ServerDataConnectionJson.pass_hash))
            Message.showDialog("Set connection to " + name + "?", DialogIcon.Info, DialogType.YesNo, () =>
            {
                if (ServerDataConnectionJson != null)
                {
                    if (ServerDataConnectionJson.cloud_hash != null)
                    {
                        ServerDataConnectionJson.server_name = name;
                        PlayerPrefs.SetString("SERVER_KEY", JsonUtility.ToJson(ServerDataConnectionJson));
                        StartCoroutine(connectAndOpenScanner(ServerDataConnectionJson));
                    }
                }
            });
        else
            Message.showDialog("Enter the password for " + name + ", and press connect.", DialogIcon.Info, ServerDataConnectionJson.pass_hash, () =>
            {
                if (ServerDataConnectionJson != null)
                {
                    if (ServerDataConnectionJson.cloud_hash != null)
                    {
                        ServerDataConnectionJson.server_name = name;
                        PlayerPrefs.SetString("SERVER_KEY", JsonUtility.ToJson(ServerDataConnectionJson));
                        StartCoroutine(connectAndOpenScanner(ServerDataConnectionJson));
                    }
                }
            },() =>
            {
                Message.showDialog("Wrong password!, try again and press connect.", DialogIcon.Info, ServerDataConnectionJson.pass_hash, () =>
                {
                    if (ServerDataConnectionJson != null)
                    {
                        if (ServerDataConnectionJson.cloud_hash != null)
                        {
                            ServerDataConnectionJson.server_name = name;

                            PlayerPrefs.SetString("SERVER_KEY", JsonUtility.ToJson(ServerDataConnectionJson));
                            StartCoroutine(connectAndOpenScanner(ServerDataConnectionJson));
                        }
                    }
                });
            });
    }

    IEnumerator connectAndOpenScanner(ServerData newConnection)
    {
        yield return new WaitForSeconds(0.8f);
        ServerData l = new ServerData();
        l.server_name = newConnection.server_name;
        l.cloud_hash = newConnection.cloud_hash;
        l.pass_hash = newConnection.pass_hash;
        l.key1 = newConnection.key1;
        l.key2 = newConnection.key2;
        l.key3 = newConnection.key3;
        l.key4 = newConnection.key4;
        l.key5 = newConnection.key5;
        PlayerPrefs.SetString("SERVER_KEY", JsonUtility.ToJson(l));
        PlayerPrefs.SetString("SCENE_STATE", "SCANNER");
        Application.LoadLevel(1);
    }
    IEnumerator waitAndSpawnError(string serverName)
    {
        yield return new WaitForSeconds(0.8f);
        Message.showDialog("Error! Couldn't connect to to server " + name + "!", DialogIcon.Error, DialogType.Ok, () => {});
    }

    IEnumerator showStatus(float waitBefore, string shownText, float showTime, float retryConnectionTime)
    {
        yield return new WaitForSeconds(waitBefore);
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / showTime)
        {
            CloudStatusText.text = shownText + (int)(showTime - (showTime * t)) + "...";
            yield return null;
        }
        mCloudRecoBehaviour.RegisterEventHandler(this);
        CloudStatusText.text = "";
    }

    public void OnInitialized(TargetFinder targetFinder)
    {
        //throw new System.NotImplementedException();
    }
}
[System.Serializable]
public class ServerData
{
    public string server_name;
    public string cloud_hash;
    public string pass_hash;
    public string key1;
    public string key2;
    public string key3;
    public string key4;
    public string key5;
}
