﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.Networking;
using System.Security.Cryptography;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class UploadManager : MonoBehaviour {
    string slash = @"\";
    //92jHmcyE6aAAAAAAAAACZNX41T8r3aQGz7VNIq2jzezBD3fUMyuVNIp2wdXEA7EX - Test app access token
    //HijZltjcDaAAAAAAAAAAQie6rsuHJpmzk-QFlJzVBrsrDQalQYys3MLvjij6G0dA - Release access token
    private static string dropbox_access_token = "";
    public static string access_key = "";//"457fbde493da747fd83efaaa722cf240dd2d70c4";//
    public static string Bububu_key = "";//"744a954f9b98d8d61031d4f04aa37b2b76e1fe5a";

    public static string serverList_access_key = "0162fef8541929bfceb405b79d5de5f45b7ac897";
    public static string serverList_secret_key = "f65950b1889fa9d3c98103b1db40cb0599a4ab41";     
    // Use this for initialization 

    void Start () {
#if UNITY_ANDROID
        slash = "/";
#elif UNITY_IOS
        slash = "/";
#endif
        UpdateKeyVariables();
    }

    public static void UpdateKeyVariables()
    {
        dropbox_access_token = KeyManager.cloud_hash;
        access_key = KeyManager.key3;
        Bububu_key = KeyManager.key4;
    }

    public UnityWebRequest CloudRename(string oldName, string newName)
    {
        string serviceURI = "https://api.dropboxapi.com/2/files/move_v2";//postRequest.setURI(new URI(url + "/targets"));
        string contentType = "application/json";
        Rename rename = new Rename();
        rename.from_path = "/" + oldName;
        rename.to_path = "/" + newName;
        rename.autorename = true;
        byte[] jsonRaw = System.Text.Encoding.UTF8.GetBytes(JsonUtility.ToJson(rename));
        var request = new UnityWebRequest(serviceURI, "POST");
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Authorization", string.Format("Bearer {0}", dropbox_access_token));
        request.SetRequestHeader("Content-Type", contentType);
        return request;
    }

    bool isVudoriaResponse(string json)
    {
        try
        {
            if (JsonUtility.FromJson<VudoriaResponse>(json) != null)
            {
                Debug.Log("Json Type Length:" + json.Length);
                return true;
            }
            else return false;
        }
        catch {
            Debug.Log("Wrong json string Type!");
            return false;
        }
    }

    bool isTypeOfNewTargetData(string json)
    {
        try
        {
            if (JsonUtility.FromJson<NewTargetData>(json) != null)
            {
                Debug.Log("Json Type Length:" + json.Length);
                return true;
            }
            else return false;
        }
        catch
        {
            Debug.Log("Wrong json string Type!");
            return false;
        }
    }

    public void DeleteAllData()
    {
        Message.showDialog("Are you sure you want to delete all the data??", DialogIcon.Warning, DialogType.YesNo, () =>
        {
            PlayerPrefs.DeleteAll();
        });
    }

    IEnumerator PostRequest(UnityWebRequest request)
    {
        RequestElement currentElement = new RequestElement();
        yield return request.Send().progress;
        while (!request.isDone)
        {
            yield return null;
        }
        switch (request.responseCode)
        {
            case 0:
                Debug.Log(currentElement.status_text + " Failed!\n<size=30>"+"Code:"+request.responseCode+"\n"+request.downloadHandler.text+"</size>");
                // No Internet Connection!
                break;
            case 200:
                // Request Success code!
                //if (currentElement.status_text != "")
                    //StartCoroutine(PlayDoneAnimation(false, currentElement.status_text + " Has finished!"));
                    /*
                Debug.Log("Request Handler text: "+request.downloadHandler.text);
                if (currentElement.status_group == "download_file" || currentElement.status_group.Contains("download_list_element_"))//get_target_ids
                {
                    saveFile file = new saveFile();
                    file = JsonUtility.FromJson<saveFile>(currentElement.metadata);
                    string path = Application.persistentDataPath + slash + file.name + "." + file.format;
                    File.WriteAllBytes(path, request.downloadHandler.data);
                    if (file.format == "jpg")
                    {
                        if (File.Exists(Application.persistentDataPath + slash + file.name + ".txt"))
                        {
                            string targetMetadata = File.ReadAllText(Application.persistentDataPath + slash + file.name + ".txt");
                            NewTargetData meta = new NewTargetData();
                            meta = JsonUtility.FromJson<NewTargetData>(targetMetadata);
                            meta.Target_id = file.name;
                            if (meta.quality <= 0)
                                RetrieveTargetRecord(meta.Target_id, "update_list_element_" + meta.Target_id, JsonUtility.ToJson(meta));
                        }
                    }
                    if (file.format == "txt")
                    {
                        string targetMetadata = File.ReadAllText(path);
                        NewTargetData meta = new NewTargetData();
                        meta = JsonUtility.FromJson<NewTargetData>(targetMetadata);
                        meta.Target_id = file.name;
                        if(meta.quality <= 0)
                        RetrieveTargetRecord(meta.Target_id, "update_list_element_" + meta.Target_id, JsonUtility.ToJson(meta));
                    }
                }
                else if (currentElement.status_group == "get_target_ids")//get_target_ids//sync_target_ids
                {
                    TargetsIdList targets = new TargetsIdList();
                    targets = JsonUtility.FromJson<TargetsIdList>(request.downloadHandler.text);
                    onScreenDebugText.text += "\nFull Sync, Targets found on server: " + targets.results.Length;
                    list.checkIds(targets.results,true);
                    StartCoroutine(fullSyncShowText(targets.results.Length));
                }
                else if (currentElement.status_group == "sync_target_ids")//get_target_ids//sync_target_ids
                {
                    TargetsIdList targets = new TargetsIdList();
                    targets = JsonUtility.FromJson<TargetsIdList>(request.downloadHandler.text);
                    onScreenDebugText.text += "\nQuick Sync, Targets found on server: " + targets.results.Length;
                    list.checkIds(targets.results,true,true);
                }
                else if (currentElement.status_group == "put_update_target")//get_target_ids//Deleting_Target_
                {
                    VuforiaTransaction transaction = new VuforiaTransaction();
                    transaction = JsonUtility.FromJson<VuforiaTransaction>(request.downloadHandler.text);
                    NewTargetData meta = new NewTargetData();
                    meta = JsonUtility.FromJson<NewTargetData>(currentElement.metadata);
                    list.AddOrSetListElement(meta.Target_id, meta);
                    RetrieveTargetRecord(meta.Target_id, "updated_target_with_id_" + meta.Target_id, JsonUtility.ToJson(meta));
                    if (currentElement.status_text != "")
                        StartCoroutine(PlayDoneAnimation(true, currentElement.status_text + " Has finished!"));
                    viewListButton.SetActive(true);
                }
                else if (currentElement.status_group == "update_key")//get_target_ids//Deleting_Target_
                {
                    Message.showDialog("Server Key updated successfully! you'll be returned to main menu.", DialogIcon.Success, () => 
                    {
                        Application.LoadLevel(0);
                        StartCoroutine(PlayDoneAnimation(true, "Server key updated!"));
                    });
                }
                else if (currentElement.status_group.Contains("Deleting_Target_"))//get_target_ids//Deleting_Target_
                {
                    VuforiaTransaction transaction = new VuforiaTransaction();
                    transaction = JsonUtility.FromJson<VuforiaTransaction>(request.downloadHandler.text);
                    NewTargetData current = new NewTargetData();
                    current = JsonUtility.FromJson<NewTargetData>(currentElement.metadata);
                    Destroy(list.FindID(current.Target_id));
                    if (currentElement.status_text != "")
                        StartCoroutine(PlayDoneAnimation(true, currentElement.status_text + " Has finished!"));
                    DeleteFileFromDropBox(current.Target_id + ".jpg");
                    if (current.VideoPath != "")
                        DeleteFileFromDropBox(current.Target_id + getFormat(current.VideoPath));
                    DeleteFileFromDropBox(current.Target_id + ".txt");
                    viewListButton.GetComponent<Button>().onClick.Invoke();
                }
                else if (JsonUtility.FromJson<VudoriaResponse>(request.downloadHandler.text) != null)
                {

                    lastResp = JsonUtility.FromJson<VudoriaResponse>(request.downloadHandler.text);
                    Debug.Log("Target Status: " + lastResp.status);
                    if (lastResp.status == "processing")
                    {
                        RetrieveTargetRecord(lastResp.target_record.target_id, currentElement.status_group, currentElement.metadata);
                        list.FindID(lastResp.target_record.target_id).GetComponent<ListElement>().targetName.text = "Processing...";
                        list.FindID(lastResp.target_record.target_id).GetComponent<ListElement>().UpdatePreview(Application.persistentDataPath + slash + lastResp.target_record.target_id + ".jpg");
                        yield return new WaitForSeconds(10.0f);
                    }
                    else if (lastResp.status == "success")
                    {
                        lastResp = JsonUtility.FromJson<VudoriaResponse>(request.downloadHandler.text);
                        if (lastResp.target_record != null)
                        {
                            targetRecord target = new targetRecord();
                            target = lastResp.target_record;
                            NewTargetData dataUpdate = new NewTargetData();
                            dataUpdate = JsonUtility.FromJson<NewTargetData>(currentElement.metadata);
                            dataUpdate.TargetName = target.name;
                            dataUpdate.quality = target.tracking_rating;
                            Debug.Log("Target Record Completed Processing: " + target.target_id);
                            list.AddOrSetListElement(target.target_id, dataUpdate);
                            list.FindID(target.target_id).GetComponent<ListElement>().UpdatePreview(Application.persistentDataPath + slash + target.target_id + ".jpg");
                        }
                        else
                        {
                            Debug.Log("Target Record Found as NULL!");
                        }
                    }
                }*/
                break;
            case 201:
                /*
                lastPost = JsonUtility.FromJson<PostTargetResponse>(request.downloadHandler.text);
                if (lastPost != null)
                {
                    Debug.Log(currentElement.status_text + "Code " + request.responseCode + ": Target Record ID: " + lastPost.target_id);
                    ListImageData newListData = new ListImageData();
                    newListData = JsonUtility.FromJson<ListImageData>(currentElement.metadata);
                    NewTargetData meta = new NewTargetData();
                    meta = JsonUtility.FromJson<NewTargetData>(newListData.metadata);
                    RenameMetadataCreateNewListElement(meta.TempName, lastPost.target_id, meta.VideoPath);
                    newListData.target_id = lastPost.target_id;
                    meta.Target_id = lastPost.target_id;
                    meta.quality = -1;
                    list.AddDataToArray(newListData);
                    list.UpdateOrAddListData(meta.Target_id, JsonUtility.ToJson(meta));
                    viewListButton.SetActive(true);
                    if (currentElement.status_text != "")
                        StartCoroutine(PlayDoneAnimation(true, currentElement.status_text + " Has finished!"));
                    list.AddOrSetListElement(lastPost.target_id, meta);
                    RetrieveTargetRecord(lastPost.target_id, "create_target_with_id_" + lastPost.target_id, JsonUtility.ToJson(meta));
                }*/
                break;
            case 401:/*
                // Authentication failure!
                StartCoroutine(PlayDoneAnimation(false, currentElement.status_text + " Failed!\n<size=30>Last Action Group: " + currentElement.status_group + "\n" + "Code:" + request.responseCode + "\n" + request.downloadHandler.text + "</size>"));
                break;
            case 403:
                if (request.downloadHandler.text.Contains("TargetNameExist"))
                {
                    changeNameInprogress = true;
                    newNameField.gameObject.transform.parent.parent.gameObject.SetActive(true);
                    NewTargetData meta = new NewTargetData();
                    meta = JsonUtility.FromJson<NewTargetData>(currentElement.metadata);
                    StartCoroutine(PlayDoneAnimation(false, "Target named:\n<color=red>" +meta.TargetName+"</color>\nAlready exists! try using a different name:"));
                    newNameField.text = meta.TargetName + "_" + (int)UnityEngine.Random.Range(0, 9999);
                }
                */
                //Forbidden! Target with that name already exists!
                //else
                //Forbidden! Request timestamp outside allowed range!
                //StartCoroutine(PlayDoneAnimation(false, currentElement.status_text + " Failed!\n<size=30>" + "Code:" + request.responseCode + "\n" + request.downloadHandler.text + "</size>"));
                break;
            case 404:
                //Internal Error!
                Debug.Log(currentElement.status_text + " Failed!\n<size=30>Last Action Group: " + currentElement.status_group + "\n" + "Code:" + request.responseCode + "\n" + request.downloadHandler.text + "</size>"); break;
            case 409:
                //Internal Error!
                Debug.Log("FAILURE IN ACTION GROUP: " + currentElement.status_group);
                //StartCoroutine(PlayDoneAnimation(false, currentElement.status_text + " Failed!\n<size=30>Last Action Group: " + currentElement.status_group + "\n" + "Code:" + request.responseCode + "\n" + request.downloadHandler.text + "</size>"));
                break;
            case 422:
                //containts("BadImage")
                //    Unprocessable Entity, Image corrupted or format not supported!;
                //Contains("ImageTooLarge")
                //    Unprocessable Entity, Image size is too big!
                //Contains("MetadataTooLarge")
                //    Unprocessable Entity, Metadata size is too big!
                //else
                //Error! Unprocessable Entity!
                Debug.Log(currentElement.status_text + " Failed!\n<size=30>Last Action Group: " + currentElement.status_group + "\n" + "Code:" + request.responseCode + "\n" + request.downloadHandler.text + "</size>"); break;
            case 500:
                //The server encountered an internal error; please retry the request!"
                Debug.Log(currentElement.status_text + " Failed!\n<size=30>Last Action Group: " + currentElement.status_group + "\n" + "Code:" + request.responseCode + "\n" + request.downloadHandler.text + "</size>"); break;
            default:
                Debug.Log(currentElement.status_text + " Failed!\n<size=30>Last Action Group: " + currentElement.status_group +"\n" + "Code:" + request.responseCode + "\n" + request.downloadHandler.text + "</size>"); break;
        }
        Debug.Log(currentElement.status_text + "Code:" + request.responseCode + "\n" + request.downloadHandler.text);
        yield return new WaitForEndOfFrame();
    }

    private IEnumerator LoadImage(string path)
    {
        var url = "file://" + path;
        var www = new WWW(url);
        yield return www;
        var texture = www.texture;
        if (texture == null)
        {
            Debug.LogError("Failed to load texture url:" + url);
        }
    }

  /*  public void RenameMetadataCreateNewListElement(string tempName, string target_id, string VideoFilePath)
    {
        CallRenameFileInDropBox(tempName + ".txt", target_id + ".txt");
        CallRenameFileInDropBox(tempName + ".jpg", target_id + ".jpg");
        if (VideoFilePath != "")
        {
            if(getFormat(VideoFilePath) == ".jpg" || getFormat(VideoFilePath) == ".png")
                CallRenameFileInDropBox("P_"+tempName + getFormat(VideoFilePath), "P_" + target_id + getFormat(VideoFilePath));
            else CallRenameFileInDropBox(tempName + getFormat(VideoFilePath), target_id + getFormat(VideoFilePath));
        }
    }*/

    public string getFormat(string path)
    {
        string[] fileFormat = path.Split('.');
        return "."+fileFormat[fileFormat.Length - 1];
    }

    public static UnityWebRequest UploadFileDropbox(string localFilePath, string fileTargetPath)
    {
        byte[] data = File.ReadAllBytes(localFilePath);
        string requestPath = "/upload";
        string serviceURI = @"https://content.dropboxapi.com/2/files" + requestPath;//postRequest.setURI(new URI(url + "/targets"));
        string contentType = "application/octet-stream";
        // if your texture2d has RGb24 type, don't need to redraw new texture2d
        DropboxData dropData = new DropboxData();
        dropData.path = "/Mazzee_Servers/" + UserManager.user_id  + "/" + KeyManager.key1 + "/" + fileTargetPath;
        dropData.autorename = true;
        dropData.mode = "overwrite";
        UnityWebRequest request = new UnityWebRequest(serviceURI, "POST");
        request.SetRequestHeader("Authorization", string.Format("Bearer {0}", dropbox_access_token));
        request.SetRequestHeader("Content-Type", contentType);
        request.SetRequestHeader("Dropbox-API-Arg", JsonUtility.ToJson(dropData));//"+'"'+"/*{"path":"/uploadUnity.jpg"}' */
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(data);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        return request;
    }

    public static UnityWebRequest UploadFileDropbox(internet_packge pack)
    {
        string json_package = JsonUtility.ToJson(pack);
        byte[] data = System.Text.Encoding.UTF8.GetBytes(json_package);
        string requestPath = "/upload";
        string serviceURI = @"https://content.dropboxapi.com/2/files" + requestPath;//postRequest.setURI(new URI(url + "/targets"));
        string contentType = "application/octet-stream";
        // if your texture2d has RGb24 type, don't need to redraw new texture2d
        DropboxData dropData = new DropboxData();
        dropData.path = "/testpackage.alt";
        dropData.autorename = true;
        dropData.mode = "overwrite";
        UnityWebRequest request = new UnityWebRequest(serviceURI, "POST");
        request.SetRequestHeader("Authorization", string.Format("Bearer {0}", "92jHmcyE6aAAAAAAAAAF30HXPbGd8tmlGGEcrIVdeEXwK5ebe5rKfdehDbz4Jc3r"));
        request.SetRequestHeader("Content-Type", contentType);
        request.SetRequestHeader("Dropbox-API-Arg", JsonUtility.ToJson(dropData));//"+'"'+"/*{"path":"/uploadUnity.jpg"}' */
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(data);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        return request;
    }

    public static UnityWebRequest UploadFileDropbox(byte[] data, string fileTargetPath)
    {
        string requestPath = "/upload";
        string serviceURI = @"https://content.dropboxapi.com/2/files" + requestPath;//postRequest.setURI(new URI(url + "/targets"));
        string contentType = "application/octet-stream";
        // if your texture2d has RGb24 type, don't need to redraw new texture2d
        DropboxData dropData = new DropboxData();
        dropData.path = "/Mazzee_Servers/" + UserManager.user_id + "/" + KeyManager.key1 + "/" + fileTargetPath;
        dropData.autorename = true;
        dropData.mode = "overwrite";
        UnityWebRequest request = new UnityWebRequest(serviceURI, "POST");
        request.SetRequestHeader("Authorization", string.Format("Bearer {0}", dropbox_access_token));
        request.SetRequestHeader("Content-Type", contentType);
        request.SetRequestHeader("Dropbox-API-Arg", JsonUtility.ToJson(dropData));//"+'"'+"/*{"path":"/uploadUnity.jpg"}' */
        try
        {
            request.uploadHandler = (UploadHandler)new UploadHandlerRaw(data);
        }
        catch { }
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        return request;
    }

    public static int generateValue()
    {
        int i = 0;
        i += DateTime.Now.Year*365;
        i += DateTime.Now.Month*30;
        i += DateTime.Now.Day;
        return i;
    }

    public static UnityWebRequest RetrieveTargetRecord(string targetId)
    {
        string requestPath = "/targets/" + targetId;
        string serviceURI = @"https://vws.vuforia.com" + requestPath;//postRequest.setURI(new URI(url + "/targets"));
        string httpAction = "GET";//HttpPost postRequest = new HttpPost();
        string date = string.Format("{0:r}", DateTime.Now.ToUniversalTime());
        Debug.Log(date);
        MD5 md5 = MD5.Create();
        var contentMD5bytes = md5.ComputeHash(System.Text.Encoding.ASCII.GetBytes(""));
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        for (int i = 0; i < contentMD5bytes.Length; i++)
            sb.Append(contentMD5bytes[i].ToString("x2"));
        string contentMD5 = sb.ToString();
        string stringToSign = string.Format("{0}\n{1}\n{2}\n{3}\n{4}", httpAction, contentMD5, "", date, requestPath);
        HMACSHA1 sha1 = new HMACSHA1(System.Text.Encoding.ASCII.GetBytes(Bububu_key));
        byte[] sha1Bytes = System.Text.Encoding.ASCII.GetBytes(stringToSign);
        MemoryStream stream = new MemoryStream(sha1Bytes);
        byte[] sha1Hash = sha1.ComputeHash(stream);
        string signature = System.Convert.ToBase64String(sha1Hash);
        var request = new UnityWebRequest(serviceURI, "GET");
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("host", @"https://vws.vuforia.com");// 3 days this wasnt working because of a FUKCING CAPPITAL LETTER AT Host and DDDAATE!!!
        request.SetRequestHeader("date", date);
        request.SetRequestHeader("Authorization", string.Format("VWS {0}:{1}", access_key, signature));
        return request;
    }

    public static UnityWebRequest GetTargetsIDs()
    {

        string requestPath = "/targets";
        string serviceURI = @"https://vws.vuforia.com" + requestPath;//postRequest.setURI(new URI(url + "/targets"));
        string httpAction = "GET";//HttpPost postRequest = new HttpPost();
        string date = string.Format("{0:r}", DateTime.Now.ToUniversalTime());
        Debug.Log(date);

        MD5 md5 = MD5.Create();
        var contentMD5bytes = md5.ComputeHash(System.Text.Encoding.ASCII.GetBytes(""));
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        for (int i = 0; i < contentMD5bytes.Length; i++)
        {
            sb.Append(contentMD5bytes[i].ToString("x2"));
        }
        
        string contentMD5 = sb.ToString();
        Debug.Log("<color=red>MD5 binary: " + contentMD5 + "</color>");

        string stringToSign = string.Format("{0}\n{1}\n{2}\n{3}\n{4}", httpAction, contentMD5, "", date, requestPath);

        HMACSHA1 sha1 = new HMACSHA1(System.Text.Encoding.ASCII.GetBytes(Bububu_key));
        byte[] sha1Bytes = System.Text.Encoding.ASCII.GetBytes(stringToSign);
        MemoryStream stream = new MemoryStream(sha1Bytes);
        byte[] sha1Hash = sha1.ComputeHash(stream);
        string signature = System.Convert.ToBase64String(sha1Hash);
        var request = new UnityWebRequest(serviceURI, "GET");
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("host", @"https://vws.vuforia.com");// 3 days this wasnt working because of a FUKCING CAPPITAL LETTER AT Host and DDDAATE!!!
        request.SetRequestHeader("date", date);
        //request.SetRequestHeader("Content-Type", contentType);
        request.SetRequestHeader("Authorization", string.Format("VWS {0}:{1}", access_key, signature));
        return request;
    }

    void GetTargetsIDs(bool syncExistingOnly)
    {

        string requestPath = "/targets";
        string serviceURI = @"https://vws.vuforia.com" + requestPath;//postRequest.setURI(new URI(url + "/targets"));
        string httpAction = "GET";//HttpPost postRequest = new HttpPost();
        string date = string.Format("{0:r}", DateTime.Now.ToUniversalTime());
        Debug.Log(date);

        MD5 md5 = MD5.Create();
        var contentMD5bytes = md5.ComputeHash(System.Text.Encoding.ASCII.GetBytes(""));
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        for (int i = 0; i < contentMD5bytes.Length; i++)
        {
            sb.Append(contentMD5bytes[i].ToString("x2"));
        }

        string contentMD5 = sb.ToString();
        Debug.Log("<color=red>MD5 binary: " + contentMD5 + "</color>");

        string stringToSign = string.Format("{0}\n{1}\n{2}\n{3}\n{4}", httpAction, contentMD5, "", date, requestPath);

        HMACSHA1 sha1 = new HMACSHA1(System.Text.Encoding.ASCII.GetBytes(Bububu_key));
        byte[] sha1Bytes = System.Text.Encoding.ASCII.GetBytes(stringToSign);
        MemoryStream stream = new MemoryStream(sha1Bytes);
        byte[] sha1Hash = sha1.ComputeHash(stream);
        string signature = System.Convert.ToBase64String(sha1Hash);
        var request = new UnityWebRequest(serviceURI, "GET");
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("host", @"https://vws.vuforia.com");// 3 days this wasnt working because of a FUKCING CAPPITAL LETTER AT Host and DDDAATE!!!
        request.SetRequestHeader("date", date);
        //request.SetRequestHeader("Content-Type", contentType);
        request.SetRequestHeader("Authorization", string.Format("VWS {0}:{1}", access_key, signature));
        //addRequestToArray(request, "", "sync_target_ids", false);
    }

    public UnityWebRequest DownloadFromDropbox(string theTargetName, string format, string action_group)
    {
        string requestPath = "/download";
        string serviceURI = @"https://content.dropboxapi.com/2/files" + requestPath;//postRequest.setURI(new URI(url + "/targets"));
        string contentType = "application/octet-stream";
        DropboxPath dropData = new DropboxPath();
        dropData.path = "/" + theTargetName + "."+ format;
        var request = new UnityWebRequest(serviceURI, "POST");
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Authorization", string.Format("Bearer {0}", dropbox_access_token));
        request.SetRequestHeader("Content-Type", contentType);
        request.SetRequestHeader("Dropbox-API-Arg", JsonUtility.ToJson(dropData));
        saveFile file = new saveFile();
        file.name = theTargetName;
        file.format = format;
        return request;
    }

    public static UnityWebRequest PostNewTarget(string imagePath, VuforiaTargetMetadata NewMetadata)
    {
        string requestPath = "/targets";
        string serviceURI = @"https://vws.vuforia.com" + requestPath;//postRequest.setURI(new URI(url + "/targets"));
        string httpAction = "POST";//HttpPost postRequest = new HttpPost();
        string contentType = "application/json";
        string date = string.Format("{0:r}", DateTime.Now.ToUniversalTime());
        byte[] image = File.ReadAllBytes(imagePath);
        Texture2D tex = new Texture2D(2, 2, TextureFormat.RGB24, false);
        tex.LoadImage(image);
        image = tex.EncodeToJPG(98);
        Debug.Log("Image Size Added: " + image.Length);
        if (image.Length > 1153433)
        {
            image = tex.EncodeToJPG(96);
            Debug.Log("Image New Size: " + image.Length);
        }
        byte[] metadata = System.Text.ASCIIEncoding.ASCII.GetBytes(JsonUtility.ToJson(NewMetadata));
        Target model = new Target();
        Debug.Log("Generated Target Name: " + "u_" + UserManager.user_id + "t_" + image.Length);
        model.name = "u_"+UserManager.user_id +"t_" + image.Length;
        model.width = 10.0f; // don't need same as width of texture
        model.image = System.Convert.ToBase64String(image);
        model.application_metadata = System.Convert.ToBase64String(metadata);
        string requestBody = JsonUtility.ToJson(model);
        MD5 md5 = MD5.Create();
        var contentMD5bytes = md5.ComputeHash(System.Text.Encoding.ASCII.GetBytes(requestBody));
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        for (int i = 0; i < contentMD5bytes.Length; i++)
        {
            sb.Append(contentMD5bytes[i].ToString("x2"));
        }
        string contentMD5 = sb.ToString();
        string stringToSign = string.Format("{0}\n{1}\n{2}\n{3}\n{4}", httpAction, contentMD5, contentType, date, requestPath);
        HMACSHA1 sha1 = new HMACSHA1(System.Text.Encoding.ASCII.GetBytes(Bububu_key));
        byte[] sha1Bytes = System.Text.Encoding.ASCII.GetBytes(stringToSign);
        MemoryStream stream = new MemoryStream(sha1Bytes);
        byte[] sha1Hash = sha1.ComputeHash(stream);
        string signature = System.Convert.ToBase64String(sha1Hash);
        Debug.Log("Signature: " + signature);
        var request = new UnityWebRequest(serviceURI, "POST");
        byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(JsonUtility.ToJson(model));
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("host", @"https://vws.vuforia.com");
        request.SetRequestHeader("date", date);
        request.SetRequestHeader("Content-Type", contentType);
        request.SetRequestHeader("Authorization", string.Format("VWS {0}:{1}", access_key, signature));
        return request;
    }

    public static UnityWebRequest PutUpdateTarget(string targetId, string imagePath, string metadataJson, string updatePrefix)
    {
        string requestPath = "/targets/" + targetId;
        string serviceURI = @"https://vws.vuforia.com" + requestPath;//postRequest.setURI(new URI(url + "/targets"));
        string httpAction = "PUT";//HttpPost postRequest = new HttpPost();
        string contentType = "application/json";
        string date = string.Format("{0:r}", DateTime.Now.ToUniversalTime());
        Debug.Log(date);
        // if your texture2d has RGb24 type, don't need to redraw new texture2d
        byte[] image = File.ReadAllBytes(imagePath);
        Texture2D tex = new Texture2D(2, 2, TextureFormat.RGB24, false);
        tex.LoadImage(image);
        image = tex.EncodeToJPG(100);
        Debug.Log("Image Size Added: " + image.Length);
        if (image.Length > 1153433)
        {
            image = tex.EncodeToJPG(96);
            Debug.Log("Image New Size: " + image.Length);
        }
        Debug.Log("Update Web Request: targetid: " + targetId);
        Debug.Log("Update Web Request: imagePath: " + imagePath);
        Debug.Log("Update Web Request: metadataJson: " + metadataJson);
        Debug.Log("Update Web Request: updatePrefix: " + updatePrefix);
        byte[] metadata = System.Text.ASCIIEncoding.ASCII.GetBytes(metadataJson);
        string requestBody = "";
        //PutUpdateName
        //PutUpdateMetaData
        //PutUpdateTargetImage
        //PutUpdateNameMetadata
        //PutUpdateNameTargetImage
        //PutUpdateMetadataTargetImage
        if (updatePrefix.Contains("N") && updatePrefix.Length == 1)
        {
            Debug.Log("Update Web Request: Updating Name");
            TargetName model = new TargetName();
            model.name = JsonUtility.FromJson<NewTargetData>(metadataJson).TargetName;
            model.active_flag = true;
        }
        else if (updatePrefix.Contains("M") && updatePrefix.Length == 1)
        {
            Debug.Log("Update Web Request: Updating Metadata");
            TargetMetadata model = new TargetMetadata();
            model.application_metadata = System.Convert.ToBase64String(metadata);
            model.active_flag = true;
            requestBody = JsonUtility.ToJson(model);
        }
        else if (updatePrefix.Contains("I") && updatePrefix.Length == 1)
        {
            Debug.Log("Update Web Request: Updating Image");
            TargetImage model = new TargetImage();
            model.width = 10.0f;//don't need same as width of texture
            model.image = System.Convert.ToBase64String(image);
            model.active_flag = true;
            requestBody = JsonUtility.ToJson(model);
        }
        else if (updatePrefix.Contains("NM"))
        {
            Debug.Log("Update Web Request: Updating Name and Meta");
            Debug.Log("Update Web Request: Updating Name and Meta");
            TargetNameMetadata model = new TargetNameMetadata();
            model.name = JsonUtility.FromJson<NewTargetData>(metadataJson).TargetName;
            model.application_metadata = System.Convert.ToBase64String(metadata);
            model.active_flag = true;
            requestBody = JsonUtility.ToJson(model);
        }
        else if (updatePrefix.Contains("MI"))
        {
            Debug.Log("Update Web Request: Updating meta and Image");
            TargetMetadataImage model = new TargetMetadataImage();
            model.width = 10.0f;//don't need same as width of texture
            model.image = System.Convert.ToBase64String(image);
            model.application_metadata = System.Convert.ToBase64String(metadata);
            model.active_flag = true;
            requestBody = JsonUtility.ToJson(model);
        }
        else if (updatePrefix.Contains("NI"))
        {
            Debug.Log("Update Web Request: Updating Name and Image");
            TargetNameImage model = new TargetNameImage();
            model.name = JsonUtility.FromJson<NewTargetData>(metadataJson).TargetName;
            model.width = 10.0f;//don't need same as width of texture
            model.image = System.Convert.ToBase64String(image);
            model.active_flag = true;
            requestBody = JsonUtility.ToJson(model);
        }
        else
        {
            Debug.Log("Update Web Request: Updating full target");
            Target model = new Target();
            model.name = JsonUtility.FromJson<NewTargetData>(metadataJson).TargetName;
            model.width = 10.0f;//don't need same as width of texture
            model.image = System.Convert.ToBase64String(image);
            model.application_metadata = System.Convert.ToBase64String(metadata);
            requestBody = JsonUtility.ToJson(model);
        }
        MD5 md5 = MD5.Create();
        var contentMD5bytes = md5.ComputeHash(System.Text.Encoding.ASCII.GetBytes(requestBody));
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        for (int i = 0; i < contentMD5bytes.Length; i++)
        {
            sb.Append(contentMD5bytes[i].ToString("x2"));
        }

        string contentMD5 = sb.ToString();

        string stringToSign = string.Format("{0}\n{1}\n{2}\n{3}\n{4}", httpAction, contentMD5, contentType, date, requestPath);

        HMACSHA1 sha1 = new HMACSHA1(System.Text.Encoding.ASCII.GetBytes(Bububu_key));
        byte[] sha1Bytes = System.Text.Encoding.ASCII.GetBytes(stringToSign);
        MemoryStream stream = new MemoryStream(sha1Bytes);
        byte[] sha1Hash = sha1.ComputeHash(stream);
        string signature = System.Convert.ToBase64String(sha1Hash);

        Debug.Log("<color=green>Signature: " + signature + "</color>");

        var request = new UnityWebRequest(serviceURI, "PUT");
        byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(requestBody);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("host", @"https://vws.vuforia.com");// 3 days this wasnt working because of a FUKCING CAPPITAL LETTER AT Host and DDDAATE!!!
        request.SetRequestHeader("date", date);
        request.SetRequestHeader("Content-Type", contentType);
        request.SetRequestHeader("Authorization", string.Format("VWS {0}:{1}", access_key, signature));
        return request;
    }

    public static UnityWebRequest ServerKey_Update(string key5, ServerData server_data)
    {
        string requestPath = "/targets/" + key5;
        string serviceURI = @"https://vws.vuforia.com" + requestPath;//postRequest.setURI(new URI(url + "/targets"));
        string httpAction = "PUT";//HttpPost postRequest = new HttpPost();
        string contentType = "application/json";
        string date = string.Format("{0:r}", DateTime.Now.ToUniversalTime());
        Debug.Log(date);
        byte[] metadata = System.Text.ASCIIEncoding.ASCII.GetBytes(JsonUtility.ToJson(server_data));
        string requestBody = "";
        TargetMetadata model = new TargetMetadata();
        model.application_metadata = System.Convert.ToBase64String(metadata);
        requestBody = JsonUtility.ToJson(model);
        MD5 md5 = MD5.Create();
        var contentMD5bytes = md5.ComputeHash(System.Text.Encoding.ASCII.GetBytes(requestBody));
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        for (int i = 0; i < contentMD5bytes.Length; i++)
        {
            sb.Append(contentMD5bytes[i].ToString("x2"));
        }
        string contentMD5 = sb.ToString();
        string stringToSign = string.Format("{0}\n{1}\n{2}\n{3}\n{4}", httpAction, contentMD5, contentType, date, requestPath);
        HMACSHA1 sha1 = new HMACSHA1(System.Text.Encoding.ASCII.GetBytes(serverList_secret_key));
        byte[] sha1Bytes = System.Text.Encoding.ASCII.GetBytes(stringToSign);
        MemoryStream stream = new MemoryStream(sha1Bytes);
        byte[] sha1Hash = sha1.ComputeHash(stream);
        string signature = System.Convert.ToBase64String(sha1Hash);
        Debug.Log("<color=green>Signature: " + signature + "</color>");
        var request = new UnityWebRequest(serviceURI, "PUT");
        byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(requestBody);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("host", @"https://vws.vuforia.com");// 3 days this wasnt working because of a FUKCING CAPPITAL LETTER AT Host and DDDAATE!!!
        request.SetRequestHeader("date", date);
        request.SetRequestHeader("Content-Type", contentType);
        request.SetRequestHeader("Authorization", string.Format("VWS {0}:{1}", serverList_access_key, signature));
        return request;
    }

    public void UploadDatakeyUpdate(string targetId, Texture2D tex, string new_name, string metadataJson)
    {
        string requestPath = "/targets/" + targetId;
        string serviceURI = @"https://vws.vuforia.com" + requestPath;//postRequest.setURI(new URI(url + "/targets"));
        string httpAction = "PUT";//HttpPost postRequest = new HttpPost();
        string contentType = "application/json";
        string date = string.Format("{0:r}", DateTime.Now.ToUniversalTime());
        Debug.Log(date);
        // if your texture2d has RGb24 type, don't need to redraw new texture2d
        byte[] metadata = System.Text.ASCIIEncoding.ASCII.GetBytes(metadataJson);
        string requestBody = "";
        //PutUpdateName
        //PutUpdateMetaData
        //PutUpdateTargetImage
        //PutUpdateNameMetadata
        //PutUpdateNameTargetImage
        //PutUpdateMetadataTargetImage
        byte[] image = tex.EncodeToJPG(100);
        Debug.Log("Image Size Added: " + image.Length);
        if (image.Length > 204000)
        {
            Debug.Log("Image Size Too Large!");
            image = tex.EncodeToJPG(90);
            Debug.Log("Image New Size: " + image.Length);
        }
        Target model = new Target();
        model.name = new_name;
        model.application_metadata = System.Convert.ToBase64String(metadata);
        model.width = 10.0f;//don't need same as width of texture
        model.image = System.Convert.ToBase64String(image);
        requestBody = JsonUtility.ToJson(model);
        MD5 md5 = MD5.Create();
        var contentMD5bytes = md5.ComputeHash(System.Text.Encoding.ASCII.GetBytes(requestBody));
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        for (int i = 0; i < contentMD5bytes.Length; i++)
        {
            sb.Append(contentMD5bytes[i].ToString("x2"));
        }
        string contentMD5 = sb.ToString();
        string stringToSign = string.Format("{0}\n{1}\n{2}\n{3}\n{4}", httpAction, contentMD5, contentType, date, requestPath);
        HMACSHA1 sha1 = new HMACSHA1(System.Text.Encoding.ASCII.GetBytes("f65950b1889fa9d3c98103b1db40cb0599a4ab41"));//serverSecretKey
        byte[] sha1Bytes = System.Text.Encoding.ASCII.GetBytes(stringToSign);
        MemoryStream stream = new MemoryStream(sha1Bytes);
        byte[] sha1Hash = sha1.ComputeHash(stream);
        string signature = System.Convert.ToBase64String(sha1Hash);
        Debug.Log("<color=green>Signature: " + signature + "</color>");
        var request = new UnityWebRequest(serviceURI, "PUT");
        byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(requestBody);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("host", @"https://vws.vuforia.com");// 3 days this wasnt working because of a FUKCING CAPPITAL LETTER AT Host and DDDAATE!!!
        request.SetRequestHeader("date", date);
        request.SetRequestHeader("Content-Type", contentType);
        request.SetRequestHeader("Authorization", string.Format("VWS {0}:{1}", "0162fef8541929bfceb405b79d5de5f45b7ac897", signature));//ServerAccessKey
        //addRequestToArray(request, "Updating Key...", "update_key", metadataJson, true);
    }

    public static UnityWebRequest RequestDeleteTarget(string targetId)
    {
        string requestPath = "/targets/"+targetId;
        string serviceURI = @"https://vws.vuforia.com" + requestPath;//postRequest.setURI(new URI(url + "/targets"));
        string httpAction = "DELETE";//HttpPost postRequest = new HttpPost();
        string contentType = "application/json";
        string date = string.Format("{0:r}", DateTime.Now.ToUniversalTime());
        Debug.Log(date);
        MD5 md5 = MD5.Create();
        var contentMD5bytes = md5.ComputeHash(System.Text.Encoding.ASCII.GetBytes(""));
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        for (int i = 0; i < contentMD5bytes.Length; i++)
        {
            sb.Append(contentMD5bytes[i].ToString("x2"));
        }
        string contentMD5 = sb.ToString();
        Debug.Log("<color=red>MD5 binary: " + contentMD5 + "</color>");
        string stringToSign = string.Format("{0}\n{1}\n{2}\n{3}\n{4}", httpAction, contentMD5, contentType, date, requestPath);
        HMACSHA1 sha1 = new HMACSHA1(System.Text.Encoding.ASCII.GetBytes(Bububu_key));
        byte[] sha1Bytes = System.Text.Encoding.ASCII.GetBytes(stringToSign);
        MemoryStream stream = new MemoryStream(sha1Bytes);
        byte[] sha1Hash = sha1.ComputeHash(stream);
        string signature = System.Convert.ToBase64String(sha1Hash);
        var request = new UnityWebRequest(serviceURI, "DELETE");
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("host", "vws.vuforia.com");// 3 days this wasnt working because of a FUKCING CAPPITAL LETTER AT Host and DDDAATE!!!
        request.SetRequestHeader("date", date);
        request.SetRequestHeader("Content-Type", contentType);
        request.SetRequestHeader("Authorization", string.Format("VWS {0}:{1}", access_key, signature));
        return request;
        //addRequestToArray(request, "Deleting Target...", "Deleting_Target_" + targetId,TargetJson, false);
    }

    public static UnityWebRequest DeleteFileFromDropBox(string Target_ID)
    {
        string serviceURI = "https://api.dropboxapi.com/2/files/delete_v2";//postRequest.setURI(new URI(url + "/targets"));
        string contentType = "application/json";
        DropboxPath deletePath = new DropboxPath();
        deletePath.path = "/Mazzee_Servers/" + UserManager.user_id + "/" + KeyManager.key1 + "/"+ Target_ID;
        byte[] jsonRaw = System.Text.Encoding.UTF8.GetBytes(JsonUtility.ToJson(deletePath));
        var request = new UnityWebRequest(serviceURI, "POST");
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Authorization", string.Format("Bearer {0}", dropbox_access_token));
        request.SetRequestHeader("Content-Type", contentType);
        return request;
    }

    void readResponce(string responce)
    {
        switch (responce)
        {

        }
    }
}

public class saveFile
{
    public string name;
    public string format;
}

public class Target//NMI
{
    public string name;
    public float width;
    public string image;
    public string application_metadata;
}
[System.Serializable]
public class VuforiaTransaction
{
    public string result_code;
    public string transaction_id;
}
[System.Serializable]
public class TargetsIdList
{
    public string result_code;
    public string transaction_id;
    public string[] results;
}

public class TargetName//N
{
    public string name;
    public bool active_flag;
}

public class TargetMetadata//M
{
    public bool active_flag;
    public string application_metadata;
}

public class TargetImage//I
{
    public float width;
    public string image;
    public bool active_flag;
}

[System.Serializable]
public class TempLink
{
    public DropboxResponce metadata;
    public string link;
}

public class TargetNameMetadata//NM
{
    public string name;
    public bool active_flag;
    public string application_metadata;
}

public class TargetNameImage//NI
{
    public string name;
    public float width;
    public string image;
    public bool active_flag;
}

public class TargetMetadataImage//MI
{
    public float width;
    public string image;
    public bool active_flag;
    public string application_metadata;
}

public class DropboxData
{
    public string path;
    public bool autorename;
    public string mode;
}

public class DropboxPath
{
    public string path;
}

public class Rename
{
    public string from_path;
    public string to_path;
    public bool autorename;
}
[System.Serializable]
public class VudoriaResponse
{
    public string result_code;
    public string transaction_id;
    public targetRecord target_record;
    public string status;
}

public class PostTargetResponse
{
    public string result_code;
    public string transaction_id;
    public string target_id;
}

public class DropboxResponce
{
    public string name;
    public string path_lower;
    public string path_display;
    public string id;
    public string client_modified;
    public string server_modified;
    public string rev;
    public int size;
    public string content_hash;
}

[System.Serializable]
public class targetRecord
{
    public string target_id;
    public bool active_flag;
    public string name;
    public float width;
    public int tracking_rating;
}

public class RequestElement
{
    public string status_text;
    public string status_group;
    public string metadata;
}


