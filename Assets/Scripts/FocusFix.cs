﻿using UnityEngine;
using Vuforia;

public class FocusFix : MonoBehaviour
{
    void Start()
    {
        var vuforia = VuforiaARController.Instance;
        vuforia.RegisterVuforiaStartedCallback(OnVuforiaStarted);
        vuforia.RegisterOnPauseCallback(OnPaused);
    }

    private void OnVuforiaStarted() // turn on autofocus on application start.
    {
        CameraDevice.Instance.SetFocusMode(
        CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }

    private void OnPaused(bool paused)
    {
        if (!paused) // resumed
        {
            // Set again autofocus mode when app is resumed
            CameraDevice.Instance.SetFocusMode(
            CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        }
    }

    public void Pause()// Use this function to pause the camera view in runtime from script or button.
    {
        CameraDevice.Instance.Stop();
    }

    public void UnPause()// Same back, to resume the camera for scanning and tracking.
    {
        CameraDevice.Instance.Start();
    }

    public void focus()// use this function to trigger autofocus in runtime from script or button.
    {
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
        
    }

}
