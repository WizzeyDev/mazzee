﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasRenderer))]
public class UILoadBlinkRotator : MonoBehaviour {

    float rotaionSpeed = 1.0f;
    public float maxRotationSpeed = 100.0f;
    public float blinkRate = 1.0f;
    public bool AnimateScaleOpacity = false;
    public bool ReverseAnimation = false;
    public float scaleSize = 1.0f;
    float newRotationSpeed = 1.0f;
    float lerpTimeMult = 1.0f;
    float phase = -1.0f;
    public bool rotateOn = true;
    CanvasRenderer me;
	// Use this for initialization
	void Start () {
        me = GetComponent<CanvasRenderer>();
        phase = Random.Range(-1.00f, 1.00f);
        StartCoroutine(changeSpeed());
    }
	
	// Update is called once per frame
	void Update () {
        if (phase < 1.0f)
            phase += Time.deltaTime * blinkRate;
        else phase = -1.0f;
        me.SetAlpha(Mathf.Abs(phase));
        if(rotateOn)
        transform.Rotate(0, 0, rotaionSpeed);
        if (AnimateScaleOpacity)
        {
            if(!ReverseAnimation)
            transform.localScale = new Vector3(((phase + 1) / 2) * scaleSize, ((phase + 1) / 2) * scaleSize, ((phase + 1) / 2) * scaleSize);
            else transform.localScale = new Vector3(Mathf.Abs(phase) * scaleSize, Mathf.Abs(phase) * scaleSize, Mathf.Abs(phase) * scaleSize);
            me.SetAlpha(1.0f - ((phase + 1) / 2));
        }
	}

    IEnumerator changeSpeed()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(1.0f, 3.0f));
            newRotationSpeed = Random.Range(-maxRotationSpeed, maxRotationSpeed);
            lerpTimeMult = Random.Range(0.9f, 3.0f);
            for (float i = 0; i < 1.0f; i += Time.deltaTime/lerpTimeMult)
            {
                rotaionSpeed = Mathf.Lerp(rotaionSpeed, newRotationSpeed, i);
                yield return null;
            }
        }
    }
}
