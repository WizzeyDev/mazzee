﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Security.Cryptography;

public class AppSceneManager : MonoBehaviour {

    public GameObject TargetList;
    public GameObject MainMenuActivity;
    public GameObject TutorialPageActivity;
    public GameObject ImageEditor;
    public EditPicture editpicturePref;
    LevelState levels;

    private void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        Application.targetFrameRate = 60;
        try
        {
            levels = JsonUtility.FromJson<LevelState>(PlayerPrefs.GetString("LEVEL_STATE"));
           
            if (levels.targetListActive != null)
                TargetList.SetActive(levels.targetListActive);
            if (levels.imageEditor != null)
            {
                //if (levels.imageEditor)
                //   UITransactionManager.Transact(ImageEditor.GetComponent<CanvasRenderer>(), UITransactionManager.CanvasTransaction.FadeZoomIn, true);
                ImageEditor.SetActive(levels.imageEditor);
                if(levels.imageEditor)
                editpicturePref.loadImageFromPath(levels.image_path);
            }
        }
        catch { }
        //if (/*!TargetList.active && !ImageEditor.active &&*/ PlayerPrefs.GetString("SHOW_TUTORIAL_NEXT_LAUNCH", "true") == "true")
        //{
            TutorialPageActivity.SetActive(true);
            MainMenuActivity.SetActive(false);
            //PlayerPrefs.SetString("SHOW_TUTORIAL_NEXT_LAUNCH", "false");
       // }
        //else
        //{
        //    TutorialPageActivity.SetActive(false);
        //    MainMenuActivity.SetActive(true);
        //}

        PlayerPrefs.DeleteKey("LEVEL_STATE");
        StartCoroutine(SyncOnListView());
    }

    public IEnumerator SyncOnListView()
    {
        while (!TargetList.active)
            yield return null;
        while (ImageEditor.active)
            yield return null;
      //  editpicturePref.UP.SyncOn();
    }

    // Use this for initialization
    public void loadScene(int scene)
    {
        PlayerPrefs.SetString("SCENE_STATE", "NATIVE_CAMERA");
        Application.LoadLevel(scene);
        
    }
    public void loadScene(string scene)
    {
        PlayerPrefs.SetString("SCENE_STATE", "NATIVE_CAMERA");
        Application.LoadLevel(scene);
    }

    public void loadScanner()
    {
        PlayerPrefs.SetString("SCENE_STATE", "SCANNER");
        Application.LoadLevel(1);
    }

    public void loadServerDataScanner()
    {
        PlayerPrefs.SetString("SCENE_STATE", "ACCESS");
        Application.LoadLevel(1);
    }

    public void reloadScene()
    {
        Application.LoadLevel(0);
    }

    public void testPass()
    {
        string hash = "c87e6d499e49888d1b70f3b67c47b410";
        Message.showDialog("Please, enter the test password below: ", DialogIcon.Info, hash, () =>
        {
            Message.showDialog("Nice! the password is correct!!");
        }, () =>
        {
            Message.showDialog("Wrong password!, please try again: ", DialogIcon.Error, hash, () =>
            {
                Message.showDialog("Nice! the password is correct!!");
            });
        });
    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.SetString("SHOW_TUTORIAL_NEXT_LAUNCH", "true");
    }
}
