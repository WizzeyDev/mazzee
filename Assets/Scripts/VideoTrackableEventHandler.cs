/*==============================================================================
Copyright (c) 2017 PTC Inc. All Rights Reserved.

Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Protected under copyright and other laws.
==============================================================================*/

using UnityEngine;
using UnityEngine.UI;
using Vuforia;
using UnityEngine.Networking;
using System.Collections;
using System.IO;

/// <summary>
/// A custom handler that implements the ITrackableEventHandler interface.
///
/// Changes made to this file could be overwritten when upgrading the Vuforia version.
/// When implementing custom event handler behavior, consider inheriting from this class instead.
/// </summary>
public class VideoTrackableEventHandler : MonoBehaviour, ITrackableEventHandler
{
    public TMPro.TextMeshProUGUI mTestTxt;
    public GameObject mCloseVideoOverlayBut;
    #region PROTECTED_MEMBER_VARIABLES
    public AudioSource scanSound;
    public UniversalMediaPlayer ump;
    public VuforiaTargetMetadata metadata;
    LocalTargetElement targetElement;
    DropboxTargetElement dropboxElement;
    public GameObject UIVideoCanvas;
    public GameObject[] HideOnDetect;
    public string onDetectMessage = "";
    public RawImage PhisicalObjCanvas;
    public Text loadingPRecentage;
    public Text debug;
    private bool activationInProgress = false;
    Texture2D pictureFile;
    char fuckingSlash = '/';//@"\"
    private bool Downloading_File = false;

    protected TrackableBehaviour mTrackableBehaviour;
    protected TrackableBehaviour.Status m_PreviousStatus;
    protected TrackableBehaviour.Status m_NewStatus;
    private bool detected = false;
    private bool download_Failed = false;

    #endregion // PROTECTED_MEMBER_VARIABLES

    #region UNITY_MONOBEHAVIOUR_METHODS

    protected virtual void Start()
    {
#if UNITY_EDITOR
        string lol = "\\";
        fuckingSlash = lol[0];
#endif
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
            mTrackableBehaviour.RegisterTrackableEventHandler(this);     
      
    }

    protected virtual void OnDestroy()
    {
        //ump.Stop(); //Re-open if you go back to 1 VideoTrackable per trackable
        //ump.Release();

        if (mTrackableBehaviour)
            mTrackableBehaviour.UnregisterTrackableEventHandler(this);
    }

    #endregion // UNITY_MONOBEHAVIOUR_METHODS

    #region PUBLIC_METHODS

    /// <summary>
    ///     Implementation of the ITrackableEventHandler function called when the
    ///     tracking state changes.
    /// </summary>
    public void OnTrackableStateChanged(
        TrackableBehaviour.Status previousStatus,
        TrackableBehaviour.Status newStatus)
    {
        m_PreviousStatus = previousStatus;
        m_NewStatus = newStatus;
        debug.text = "Previous Status: " + previousStatus.ToString();
        debug.text = "New Status: " + newStatus.ToString();
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
            OnTrackingFound();
            if(!string.IsNullOrEmpty(onDetectMessage) && onDetectMessage.Length > 1)
            TipMessage.showTip(onDetectMessage, 3.0f);
            detected = true;
            foreach(GameObject go in HideOnDetect)
                go.SetActive(false);
            if (!activationInProgress)
                StartCoroutine(activateFunctions());
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                 newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
            OnTrackingLost();
            detected = false;
            foreach (GameObject go in HideOnDetect)
                go.SetActive(true);
        }
        else
        {
            // For combo of previousStatus=UNKNOWN + newStatus=UNKNOWN|NOT_FOUND
            // Vuforia is starting, but tracking has not been lost or found yet
            // Call OnTrackingLost() to hide the augmentations
            OnTrackingLost();
        }
    }

    #endregion // PUBLIC_METHODS

    #region PROTECTED_METHODS
    protected virtual void OnTrackingFound()
    {
        #region -------------- Refactor ME!--------------------------
        var rendererComponents = GetComponentsInChildren<Renderer>(true);
        var colliderComponents = GetComponentsInChildren<Collider>(true);
        var canvasComponents = GetComponentsInChildren<Canvas>(true);

        // Enable rendering:
        foreach (var component in rendererComponents)
            component.enabled = true;

        // Enable colliders:
        foreach (var component in colliderComponents)
            component.enabled = true;

        // Enable canvas':
        foreach (var component in canvasComponents)
            component.enabled = true;
    }


    protected virtual void OnTrackingLost() //TODO: MAZ-18 FIX
    {

        if (mTestTxt != null && TestManager.GetIsDevVerstion) //Debug
            mTestTxt.text = metadata.video_angle.ToString();

        var rendererComponents = GetComponentsInChildren<Renderer>(true);
        var colliderComponents = GetComponentsInChildren<Collider>(true);
        var canvasComponents = GetComponentsInChildren<Canvas>(true);

        // Disable rendering:
        foreach (var component in rendererComponents)
            component.enabled = false;

        // Disable colliders:
        foreach (var component in colliderComponents)
            component.enabled = false;

        // Disable canvas':
        foreach (var component in canvasComponents)
            component.enabled = false;
        #endregion  -------------- Refactor ME!--------------------------

        if (ump.IsPlaying)
        {
            UIVideoCanvas.SetActive(true);
            mCloseVideoOverlayBut.SetActive(true);
            UIVideoCanvas.transform.GetComponent<RectTransform>().Rotate(new Vector3(0, 0, metadata.video_angle)); //TODO: Resize Positsion to fit screen depaneds on angle make sure the exit button stays

            //UIVideoCanvas.GetComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
            if (metadata.video_angle != 0)
            {
                //UIVideoCanvas.GetComponent<RectTransform>().sizeDelta = new Vector3
            }
            Debug.Log("Video Aspects " + UIVideoCanvas.GetComponent<RectTransform>().lossyScale + " P " + UIVideoCanvas.GetComponent<RectTransform>().position + " Aspect: " + metadata.aspect_ratio);
        }
    }

    public string getFormat(string path)
    {
        string[] fileFormat = path.Split('.');
        return "." + fileFormat[fileFormat.Length - 1];
    }

    #endregion // PROTECTED_METHODS


    public void DeserializeMetadata(string textData, string id)
    {
        metadata = new VuforiaTargetMetadata();
        metadata = JsonUtility.FromJson<VuforiaTargetMetadata>(textData);
        string[] format; //TODO: fix memory Waste
        //metadata.Target_id = id;
        dropboxElement = new DropboxTargetElement();
        targetElement = new LocalTargetElement();
        targetElement.Unique_ID = id;
        targetElement.Local_TargetPicture_Path = Application.persistentDataPath + fuckingSlash + UserManager.user_id + fuckingSlash+"Downloaded"+fuckingSlash + id + fuckingSlash+"target.jpg";
        if (!string.IsNullOrEmpty(metadata.MediaPicture_Name)) //Load Video from DropBox?
        {
            format = metadata.MediaPicture_Name.Split('.');
            targetElement.Local_MediaPicture_Path = Application.persistentDataPath + fuckingSlash + UserManager.user_id + fuckingSlash+"Downloaded"+fuckingSlash + id + fuckingSlash+"pic." + format[format.Length - 1];
            dropboxElement.Dropbox_MediaPicture_Path = "/"+"Mazzee_Servers" + "/" + UserManager.user_id + "/" + KeyManager.key1 + "/" + id + "/" + "pic." + format[format.Length - 1];
            Debug.Log("Deserialized Metadata: TargetID: " + id + ", Format: " + format[format.Length - 1]);
        }
        if (!string.IsNullOrEmpty(metadata.Video_Name))
        {
            format = metadata.Video_Name.Split('.');
            targetElement.Local_Video_Path = Application.persistentDataPath + fuckingSlash + UserManager.user_id + fuckingSlash+"Downloaded"+fuckingSlash + id + fuckingSlash+"video." + format[format.Length - 1];
            dropboxElement.Dropbox_Video_Path = "/" + "Mazzee_Servers" + "/" + UserManager.user_id + "/" + KeyManager.key1 + "/" + id + "/" + "video." + format[format.Length - 1];
            Debug.Log("Deserialized Metadata: TargetID: " + id + ", Format: " + format[format.Length - 1]);
        }
        float size = 500.0f;
        PhisicalObjCanvas.transform.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(size * metadata.aspect_ratio, size);
        if(!activationInProgress)
        StartCoroutine(activateFunctions());
    }

    public void PlayVideo()
    {
        StartCoroutine(playVideo());    
    }

    IEnumerator playVideo()//TODO: Fix here for video diractsion
    {

        ump.Play();
        setAlpha(0.0f);        
        while (ump.VideoSize.x == 0 || ump.VideoSize.y == 0 || !ump.IsPlaying)
        yield return null;
        if(ump.VideoSize.x / ump.VideoSize.y != 0 && !float.IsNaN(ump.VideoSize.x / ump.VideoSize.y) && !float.IsInfinity(ump.VideoSize.x / ump.VideoSize.y))
        setAspect(ump.VideoSize.x / ump.VideoSize.y);
        setAlpha(1.0f);
    }

    void setAlpha(float a)
    {
        PhisicalObjCanvas.GetComponent<CanvasRenderer>().SetAlpha(a);
        UIVideoCanvas.GetComponent<CanvasRenderer>().SetAlpha(a);
    }

    void setAspect(float a)
    {
        PhisicalObjCanvas.GetComponent<AspectRatioFitter>().aspectRatio = a;
        UIVideoCanvas.GetComponent<AspectRatioFitter>().aspectRatio = a;
    }

    IEnumerator activateFunctions() //Runs when an imager was detacted and starts all the loading process + video handleling 
    {
        activationInProgress = true;
        PhisicalObjCanvas.gameObject.SetActive(true);
        UIVideoCanvas.SetActive(false);
        loadingPRecentage.text = "";
        Debug.Log("ACTIVATING TARGET.");
        while (metadata == null)
            yield return null;
        Debug.Log("HAS VIDEO: " + !string.IsNullOrEmpty(metadata.Video_Name) + ", EXISTS LOCALY: " + File.Exists(targetElement.Local_Video_Path));
        Debug.Log("VDIR: " + targetElement.Local_Video_Path);
        if (!string.IsNullOrEmpty(metadata.Video_Name))
        {
            if (File.Exists(targetElement.Local_Video_Path))
            {
                if (ump.IsPlaying && ump.Path != targetElement.Local_Video_Path)
                    ump.Stop();
                ump.Path = targetElement.Local_Video_Path;
                PhisicalObjCanvas.transform.rotation = Quaternion.Euler(0, 0, metadata.video_angle);
                //if(targetElement.video_angle == 90 || targetElement.video_angle == -90 || targetElement.video_angle == 270 || targetElement.video_angle == -270)
                PlayVideo();
            }
            else
            {
                Debug.Log("DOWNLOAD STARTED!");
                if (!Downloading_File)
                    StartCoroutine(DownloadFromDropbox(dropboxElement.Dropbox_Video_Path, metadata.Video_Name));
                while (Downloading_File)
                    yield return null;
                if (!download_Failed)
                {
                    if (ump.IsPlaying && ump.Path != targetElement.Local_Video_Path)
                        ump.Stop();
                    Debug.Log("LOCAL VIDEO PATH:" + targetElement.Local_Video_Path);
                    ump.Path = targetElement.Local_Video_Path;
                    PlayVideo();
                }
                else loadingPRecentage.text = "Picture Download Failed :(";
            }
        }
        else if (!string.IsNullOrEmpty(metadata.MediaPicture_Name))
        {
            PhisicalObjCanvas.gameObject.SetActive(true);
            if (File.Exists(targetElement.Local_MediaPicture_Path))
            {
                if (ump.IsPlaying)
                    ump.Stop();
                StartCoroutine(LoadImage(targetElement.Local_MediaPicture_Path));
            }
            else
            {
                if (!Downloading_File)
                    StartCoroutine(DownloadFromDropbox(dropboxElement.Dropbox_MediaPicture_Path, metadata.MediaPicture_Name));
                while (Downloading_File)
                    yield return null;
                if (!download_Failed)
                {
                    if (ump.IsPlaying)
                        ump.Stop();
                    StartCoroutine(LoadImage(targetElement.Local_MediaPicture_Path));
                }
                else loadingPRecentage.text = "Picture Download Failed :(";
                
            }
        }
        else if (!string.IsNullOrEmpty(metadata.URL_Path))
        {
            Debug.Log("FOUND URL: " + metadata.URL_Path);
            string url = metadata.URL_Path;
            if (url.Contains("https://youtu.be") ||
                url.Contains("https://www.youtu") ||
                url.Contains("http://youtu.be") ||
                url.Contains("http://www.youtu") ||
                url.Contains("http://m.youtu") ||
                url.Contains("https://m.youtu"))
            {
                PhisicalObjCanvas.gameObject.SetActive(true);
                Debug.Log("URL IS YOUTUBE VIDEO! LOL! STARTING PLAYER...");
                if (ump.IsPlaying && ump.Path != metadata.URL_Path)
                    ump.Stop();
                ump.Path = metadata.URL_Path;
                PlayVideo();
            }
            else
            {
                Debug.Log("OPENING LINK IN BROWSER...");
                InAppBrowser.OpenURL(url);
            }
        }
        activationInProgress = false;
    }

    private IEnumerator LoadImage(string path)
    {
        if (pictureFile == null)
        {
            var url = "file:/"+fuckingSlash + path;
            var www = new WWW(url);
            yield return www;

            var texture = www.texture;
            if (texture == null)
            {
                Debug.LogError("Failed to load texture url:" + url);
            }
            else
            {
                scanSound.Play();
                yield return new WaitForSeconds(1.5f);
                PhisicalObjCanvas.gameObject.SetActive(true);
                pictureFile = texture;
                for (int i = 0; i < ump.RenderingObjects.Length; i++)
                    if (ump.RenderingObjects[i].GetComponent<AspectRatioFitter>() != null)
                    {
                        ump.RenderingObjects[i].GetComponent<CanvasRenderer>().SetAlpha(1.0f);
                        ump.RenderingObjects[i].GetComponent<RawImage>().texture = pictureFile;
                        ump.RenderingObjects[i].GetComponent<AspectRatioFitter>().aspectRatio = (float)texture.width / (float)texture.height;
                    }
            }
        }
        else
        {
            scanSound.Play();
            yield return new WaitForSeconds(1.5f);
            PhisicalObjCanvas.gameObject.SetActive(true);
            for (int i = 0; i < ump.RenderingObjects.Length; i++)
                if (ump.RenderingObjects[i].GetComponent<AspectRatioFitter>() != null)
                {
                    ump.RenderingObjects[i].GetComponent<CanvasRenderer>().SetAlpha(1.0f);
                    ump.RenderingObjects[i].GetComponent<RawImage>().texture = pictureFile;
                    ump.RenderingObjects[i].GetComponent<AspectRatioFitter>().aspectRatio = (float)pictureFile.width / (float)pictureFile.height;
                }
        }
    }

    public IEnumerator DownloadFromDropbox(string dropbox_source_path, string metadata_name_format)
    {
        string local_target_path = Application.persistentDataPath + fuckingSlash + UserManager.user_id + fuckingSlash+"Downloaded"+fuckingSlash + targetElement.Unique_ID;
        Downloading_File = true;
        PhisicalObjCanvas.gameObject.SetActive(false);
        string requestPath = fuckingSlash+"download";
        string serviceURI = @"https://content.dropboxapi.com/2/files" + requestPath;//postRequest.setURI(new URI(url + fuckingSlash+"targets"));
        string contentType = "application/octet-stream";
        DropboxPath dropData = new DropboxPath();
        dropData.path = dropbox_source_path;
        var request = new UnityWebRequest(serviceURI, "POST");
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Authorization", string.Format("Bearer {0}", "92jHmcyE6aAAAAAAAAACZNX41T8r3aQGz7VNIq2jzezBD3fUMyuVNIp2wdXEA7EX"));
        request.SetRequestHeader("Content-Type", contentType);
        request.SetRequestHeader("Dropbox-API-Arg", JsonUtility.ToJson(dropData));
        yield return request.Send().progress;
        while (!request.isDone)
        {
            loadingPRecentage.text = "Loading " + (int)(request.downloadProgress * 100.0f) + "%";
            yield return null;
        }
        loadingPRecentage.text = "" ;
        switch (request.responseCode)
        {
            case 200:
                PhisicalObjCanvas.gameObject.SetActive(true);
                Debug.Log("DOWNLOAD ENDED: " + request.downloadHandler.text);
                Debug.Log("TARGET DIR EXISTS?: " + Directory.Exists(local_target_path) + ", CREATING");
                Debug.Log("DIR: " + local_target_path);
                Directory.CreateDirectory(local_target_path);
                Debug.Log("NOW EXISTS?: " + Directory.Exists(local_target_path) + ", GOOD! WRITING FILES...");
                File.WriteAllBytes(local_target_path+fuckingSlash+ metadata_name_format, request.downloadHandler.data);
                break;
            default:
                Debug.LogWarning("DOWNLOAD ERROR: " + request.downloadHandler.text);
                Debug.LogWarning("DOWNLOAD ERROR: source_path:" + dropbox_source_path);
                Debug.LogWarning("DOWNLOAD ERROR: target_path:" + metadata_name_format);
                if (dropbox_source_path == dropboxElement.Dropbox_Video_Path)
                    dropboxElement.Video_Exists = false;
                else if (dropbox_source_path == dropboxElement.Dropbox_MediaPicture_Path)
                    dropboxElement.MediaPicture_Exists = false;
                download_Failed = true;
                loadingPRecentage.text = "Load Error :(";
                break;
        }
        Downloading_File = false;
    }
}

