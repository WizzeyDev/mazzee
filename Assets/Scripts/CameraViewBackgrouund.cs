﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraViewBackgrouund : MonoBehaviour {

	// Use this for initialization
	void Start () {
        WebCamDevice[] webDevices = WebCamTexture.devices;
        WebCamTexture webcamTexture = new WebCamTexture(webDevices[webDevices.Length-1].name);
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = webcamTexture;
        webcamTexture.Play();
    }
}
