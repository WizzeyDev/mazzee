﻿using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BrowseImage : MonoBehaviour {

   // [SerializeField]
   // private Unimgpicker imagePicker;

    [SerializeField]
    private Text debugText;

    [SerializeField]
    private CanvasRenderer imageRenderer;

    [SerializeField]
    private Button upload;

    public Texture2D openedTexture;
    public long coins = 0;
    public long currentShown = 0;

    public void PickVideo()
    {
        NativeGallery.Permission permission = NativeGallery.GetImagesFromGallery((path) =>
        {
            Debug.Log("Video path: " + path);
            if (path != null)
            {
                // Play the selected video
                debugText.text = "";
                StartCoroutine(LoadImage(path, imageRenderer));
            }
        }, "Select a video");

        Debug.Log("Permission result: " + permission);
    }
    private IEnumerator LoadImage(string path, CanvasRenderer output)
    {
        var url = "file://" + path;
        var www = new WWW(url);
        yield return www;

        var texture = www.texture;
        if (texture == null)
        {
            Debug.LogError("Failed to load texture url:" + url);
        }
        else
        {
            GameObject clone = Instantiate(output.gameObject);
            clone.transform.parent = output.transform.parent;
            openedTexture = texture;
            clone.GetComponent<CanvasRenderer>().SetTexture(texture);
            Sprite textSprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            clone.GetComponent<Image>().overrideSprite = textSprite;
            if (clone.GetComponent<Image>() != null)
                clone.GetComponent<Image>().preserveAspect = true;
            clone.SetActive(true);
            upload.gameObject.SetActive(true);
        }
    }

    private IEnumerator LoadImage(string[] paths, CanvasRenderer output)
    {
        string url;
        WWW www;
        for (int i = 0; i < paths.Length; i++)
        {
            yield return new WaitForEndOfFrame();
            url = "file://" + paths[i];
            www = new WWW(url);
            yield return www;
            var texture = www.texture;
            if (texture == null)
            {
                Debug.LogError("Failed to load texture url:" + url);
            }
            else
            {
                GameObject clone = Instantiate(output.gameObject);
                clone.transform.parent = output.transform.parent;
                openedTexture = texture;
                clone.GetComponent<CanvasRenderer>().SetTexture(texture);
                Sprite textSprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));
                clone.GetComponent<Image>().overrideSprite = textSprite;
                if (clone.GetComponent<Image>() != null)
                    clone.GetComponent<Image>().preserveAspect = true;
                clone.SetActive(true);
                upload.gameObject.SetActive(true);
            }
        }
    }

    public void TakePicture()
    {
        TakePicture(512);
    }

    public void RecorddVideo()
    {
        StartCoroutine(RecordVideo());
    }

    private void TakePicture(int maxSize)
    {
        NativeCamera.Permission permission = NativeCamera.TakePicture((path) =>
        {
            Debug.Log("Image path: " + path);
            if (path != null)
            {
                // Create a Texture2D from the captured image
                Texture2D texture = NativeCamera.LoadImageAtPath(path, maxSize);
                if (texture == null)
                {
                    Debug.Log("Couldn't load texture from " + path);
                    return;
                }
                else
                {
                    GameObject clone = Instantiate(imageRenderer.gameObject);
                    clone.transform.parent = imageRenderer.transform.parent;
                    openedTexture = texture;
                    clone.GetComponent<CanvasRenderer>().SetTexture(texture);
                    Sprite textSprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));
                    clone.GetComponent<Image>().overrideSprite = textSprite;
                    if (clone.GetComponent<Image>() != null)
                        clone.GetComponent<Image>().preserveAspect = true;
                    clone.SetActive(true);
                    upload.gameObject.SetActive(true);
                }
                // Assign texture to a temporary quad and destroy it after 5 seconds
            }
        }, maxSize);

        Debug.Log("Permission result: " + permission);
    }

    IEnumerator RecordVideo()
    {
        yield return new WaitForEndOfFrame();
        NativeCamera.Permission permission = NativeCamera.RecordVideo((path) =>
        {
            Debug.Log("Video path: " + path);
            if (path != null)
            {
                // Play the recorded video
                GameObject clone = Instantiate(imageRenderer.gameObject);
                clone.transform.parent = imageRenderer.transform.parent;
                clone.SetActive(true);
            }
            
        });
        yield return new WaitForEndOfFrame();

        Debug.Log("Permission result: " + permission);
    }

    public void shareImage()
    {
     //   StartCoroutine(TakeSSAndShare());
    }

}
