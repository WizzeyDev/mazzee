﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestory : MonoBehaviour {

    CanvasRenderer me;
	// Use this for initialization
	void Start () {
        me = GetComponent<CanvasRenderer>();
        StartCoroutine(fadeOutAndDIE(0.5f));
	}

    IEnumerator fadeOutAndDIE(float time)
    {
        for (float i = 0.0f; i < time; i += Time.deltaTime)
        {
            me.SetAlpha(1.0f - (i / time));
            transform.Rotate(0, 0, 4.0f);
            yield return null;
        }
        Destroy(gameObject);
    }
}
