﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animateSync : MonoBehaviour {

    CanvasRenderer me;
    GameObject icon;
    float phase = -1.0f;
	// Use this for initialization
	void Start () {
        me = GetComponent<CanvasRenderer>();
        icon = transform.GetChild(0).gameObject;
    }
	
	// Update is called once per frame
	void Update () {
        if (phase < 1.0f)
            phase += Time.deltaTime * 2.0f;
        else phase = -1.0f;
        me.SetAlpha(0.4f + Mathf.Abs(phase) * 0.6f);
        icon.GetComponent<CanvasRenderer>().SetAlpha(0.4f + Mathf.Abs(phase) * 0.6f);
        icon.transform.Rotate(0, 0, -10.0f);
    }
}
