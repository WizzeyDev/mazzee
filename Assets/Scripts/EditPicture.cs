﻿using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EditPicture : MonoBehaviour {
    [HideInInspector]
    public Texture2D TargetTexture;
    public RectTransform cropFrame;
    public GameObject[] controlUIElements;
    public UniversalMediaPlayer ump;
    public LocalTargetElement TargetData;
    public GameObject editorWindow;
    public GameObject EditingTarget;

    public Transform TranTest;

    [SerializeField]
    private RawImage imageRenderer;
    [SerializeField]
    private GameObject prefab_TargetElement;
    [SerializeField]
    private GameObject NewTargetList;
    [SerializeField]
    private UIRectSlide LinkSlider;
    [SerializeField]
    private UIRectSlide MediaSlider;
    [SerializeField]
    private TMP_InputField URL;
    [SerializeField]
    private TMP_Text UploadButtonText;
    [SerializeField]
    private GameObject PreviewWindow;
    GameObject playPauseButton;
    [SerializeField]
    private PreviewAngle PreviewAngle;
    bool isCropOn = false;
    #region --------------- Needed? ----------------------
    float FullSizeX = 0.00f;
    float FullSizeY = 0.00f; 
    float ParentSizeX = 0.00f;
    float ParentSizeY = 0.00f;
    string VideoPath = "";
    string TargetPath = "";
    string ImagePath = "";
    string LinkUrl = "";
    string browsedMetadata = "";
    #endregion   --------------- Needed? ----------------------
    //string editedPrefx = "";
    public bool updatingExisting = false;
    Coroutine fadeEffect;
    Coroutine cropEffect;
    Coroutine fadeSetActive;
    Vector2 FullSize = new Vector2(0, 0);
    public bool autorename = false;
    InAppBrowserBridge bridge;
    bool imageChanged = false;
    byte g = 255;
    string slash = @"/";
    private int TargetsCount = 0;
    private bool targetLoaded = false;
    private bool editingDone = false;
    [SerializeField]
    private Button ReturnToMainActivity;
   // private TargetElement TargetListInstance;
    InAppBrowser.DisplayOptions displayOptions;

    private static EditPicture instance;
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this);
    }

    void Start() {
#if UNITY_ANDROID
        slash = "/";
#elif UNITY_IOS
        slash = "/";
#endif
        bridge = FindObjectOfType<InAppBrowserBridge>();
        TargetsCount = PlayerPrefs.GetInt(KeyManager.key1 + "_TARGETS_COUNT", 0);
        TargetData = new LocalTargetElement();
        LoadLocalTargets();
        displayOptions = new InAppBrowser.DisplayOptions();
        //displayOptions.hi
        displayOptions.hidesTopBar = false;
        displayOptions.pinchAndZoomEnabled = true;
        displayOptions.backButtonText = "Get URL";
        displayOptions.backButtonFontSize = "24"; 
        displayOptions.pageTitle = "MaZZee";
        playPauseButton = PreviewWindow.transform.GetChild(0).gameObject;
    }
    public static void ReSyncTargets()
    {
        instance.LoadLocalTargets();
    }
    public static void LoadImageFromList(LocalTargetElement TargetData)
    {
        instance.loadImageFromList(TargetData);
    }

    public static Transform GetTransform { get { return instance.transform; } }
    public void createTargetAndUpload()
    {
        TargetsCount = PlayerPrefs.GetInt(KeyManager.key1 + "_TARGETS_COUNT", 0);
        TargetData.video_angle = PreviewAngle.current_angle;
        if (TargetData.Force_TargetPicture_Update)
        {
            byte[] Cropped = TargetTexture.EncodeToJPG(98);
            File.WriteAllBytes(TargetData.Local_TargetPicture_Path, Cropped);
            TargetData.TargetPicture_Size = Cropped.Length;
        }
        Debug.Log("UPLOAD PUSH: is null or empty?: " + string.IsNullOrEmpty(TargetData.Unique_ID));
        Debug.Log("Unique ID: " + TargetData.Unique_ID);
        if (string.IsNullOrEmpty(TargetData.Unique_ID))
        {
            Debug.Log("Target creating.");
            for (int i = 0; i < TargetsCount + 5; i++) //Fix: he tried do max out
            {
                Debug.Log("Prefs ["+ KeyManager.key1 + "_TARGET_NUM_" + i + "] is null or empty: " + string.IsNullOrEmpty(PlayerPrefs.GetString(KeyManager.key1 + "_TARGET_NUM_" + i, "")));
                if (string.IsNullOrEmpty(PlayerPrefs.GetString(KeyManager.key1 + "_TARGET_NUM_" + i, "")))
                {
                    Debug.Log("Creating new taget instance...");
                    GameObject targetClone = Instantiate(prefab_TargetElement);
                    //Debug.Log("Parenting the new taget instance...");
                    //targetClone.transform.SetParent(RequestFlowManager.GetMyTransform.GetChild(0));
                
                    //targetClone.transform.SetParent(transform);
                    targetClone.transform.parent = transform;
                    Debug.LogError("<color=red> Transformer: </color>" );
                    //Debug.Log("Reseting scale of the new taget instance...");
                    targetClone.transform.localScale = new Vector3(1, 1, 1);
                    //Debug.Log("Activating the new instance...");
                    targetClone.SetActive(true);
                    //Debug.Log("Transfering target element data from the editor...");
                    targetClone.GetComponent<TargetElement>().targetElement = TargetData;
                    //Debug.Log("Started sync of the new taget instance...");
                    targetClone.GetComponent<TargetElement>().StartSync(TargetData, i);
                    Debug.Log("Saving in prefs under key: " + KeyManager.key1 + "_TARGETS_COUNT = " + (TargetsCount + 1));
                    PlayerPrefs.SetInt(KeyManager.key1 + "_TARGETS_COUNT", TargetsCount + 1);
                    break;
                }
            }
        }
        else if (EditingTarget != null)
        {
            Debug.Log("Target updating.");
            TargetData.UpdateRequested = true;
            EditingTarget.GetComponent<TargetElement>().UpdateSync(TargetData);
        }
    }
    public void LoadLocalTargets()
    {
        if (!string.IsNullOrEmpty(UserManager.user_id) && !targetLoaded)
            StartCoroutine(LocalTargetsLoadStart());
    }

    IEnumerator LocalTargetsLoadStart() // here are the Picture loading happeneds
    {
        TargetsCount = PlayerPrefs.GetInt(KeyManager.key1 + "_TARGETS_COUNT", 0);
        int skipCount = 0;
        for (int i = 0; i < TargetsCount; i++)
        {
            if (!string.IsNullOrEmpty(PlayerPrefs.GetString(KeyManager.key1 + "_TARGET_NUM_" + i, "")))
            {
                GameObject targetClone = Instantiate(prefab_TargetElement);
                //targetClone.transform.SetParent(prefab_TargetElement.transform.parent);
                targetClone.transform.parent = transform;
                targetClone.transform.localScale = new Vector3(1, 1, 1);
                targetClone.SetActive(true);
                targetClone.GetComponent<TargetElement>().StartLoad(i); //FIX: MAZ -4
                yield return new WaitForSeconds(0.16f);
            }
        }
    }

    private void LoadImage(byte[] image)
    {
        Texture2D texture = new Texture2D(2, 2);
        texture.LoadImage(image);
        TargetTexture = texture;
        imageRenderer.texture = texture;
        imageRenderer.GetComponent<AspectRatioFitter>().aspectRatio = ((float)texture.width / (float)texture.height);
        editorWindow.SetActive(true);
        imageChanged = true;
    }

    public void loadImageFromPath(string path)
    {
        StartCoroutine(LoadImage(path));
        //fullImagePath = path;
        TargetPath = path;
        UploadButtonText.text = "UPLOAD";
    }

    public void loadImageFromList(LocalTargetElement TargetData) //FIX: Should I be here?
    {        
        Debug.Log("Loading Target from list... Unique ID: " + this.TargetData.Unique_ID);
        UploadButtonText.text = "UPDATE";
        UITransactionManager.Transact(editorWindow.GetComponent<CanvasRenderer>(), UITransactionManager.CanvasTransaction.FadeZoomIn, true);
        //editorWindow.SetActive(true); // the savee as the line above, but without animation.
        StartCoroutine(LoadImage(this.TargetData.Local_TargetPicture_Path)); //Fix: is this needed??
        TargetPath = this.TargetData.Local_TargetPicture_Path;
        if (!string.IsNullOrEmpty(this.TargetData.Local_URL_Link))
        {
            URL.text = this.TargetData.Local_URL_Link;
            LastPage(this.TargetData.Local_URL_Link);
        }
        if (!string.IsNullOrEmpty(this.TargetData.Local_Video_Path))
        {
            controlUIElements[1].SetActive(false);
            ump.Path = this.TargetData.Local_Video_Path;
            MediaSlider.targetRequest = false;
        }
    }

    public void DeleteTarget()
    {
        if (EditingTarget != null)
            EditingTarget.GetComponent<TargetElement>().DeleteTarget();
        UITransactionManager.Transact(editorWindow.GetComponent<CanvasRenderer>(), UITransactionManager.CanvasTransaction.FadeZoomOut, false);
    }

    public void loadExistingTarget(string path)
    {
        StartCoroutine(LoadImage(path));
        //fullImagePath = path;  
        TargetPath = path;
    }

    // 255 - 255
    // 9 - 00
    private IEnumerator LoadImage(string path)
    {
        if(TargetData == null)
        TargetData = new LocalTargetElement();
        var url = "file://" + path;
        var www = new WWW(url);
        imageChanged = true;
        yield return www;
        var texture = www.texture;
        if (texture == null)
        {
            Debug.LogError("Failed to load texture url:" + url);
        }
        else
        {
            TargetData.Local_TargetPicture_Path = path;
            string[] videoFilePath = path.Split('/');
            TargetTexture = texture;
            imageRenderer.texture = texture;
            imageRenderer.GetComponent<AspectRatioFitter>().aspectRatio = ((float)texture.width / (float)texture.height);
            editorWindow.SetActive(true);
        }
        if (EditingTarget != null)
            controlUIElements[0].SetActive(true);
        else controlUIElements[0].SetActive(false);
    }

    private IEnumerator LoadImage(string path, RawImage targetCanvas)
    {
        var url = "file://" + path;
        var www = new WWW(url);
        yield return www;
        var texture = www.texture;
        if (texture == null)
        {
            Debug.LogError("Failed to load texture url:" + url);
        }
        else
        {
            targetCanvas.texture = texture;
            PreviewWindow.GetComponent<AspectRatioFitter>().aspectRatio = ((float)texture.width / (float)texture.height);
            playPauseButton.SetActive(false);
            targetCanvas.transform.GetChild(0).GetComponent<Button>().interactable = false;
            targetCanvas.transform.GetChild(0).GetComponent<CanvasRenderer>().SetAlpha(0.0f);
        }
    }


    private IEnumerator pickTargetCanvasImage(string path)
    {
        var url = "file://" + path;
        var www = new WWW(url);
        imageChanged = true;
        yield return www;

        var texture = www.texture;
        if (texture == null)
        {
            Debug.LogError("Failed to load texture url:" + url);
        }
        else
        {
           // targetElement.Local_MediaPicture_Path = path;
            string[] format = path.Split('.');
            string newName = "pic_" + Random.Range(00000, 99999) + "." + format[format.Length - 1];
            string newpath = Application.persistentDataPath + slash + UserManager.user_id + slash + "LocalData";
            Debug.LogWarning("DIR: " + newpath + " EXISTS: " + Directory.Exists(newpath));
            if (Directory.Exists(newpath))
            {
                newpath += slash + newName;
                File.Move(path, newpath);
                TargetData.Local_MediaPicture_Path = newpath;
            }
            else
            {
                Debug.LogWarning("CREATING DIR: " + newpath);
                Directory.CreateDirectory(newpath);
                Debug.LogWarning("NEW DIR: " + newpath + " EXISTS: " + Directory.Exists(newpath));
                if (Directory.Exists(newpath))
                {
                    newpath += slash + newName;
                    File.Move(path, newpath);
                    TargetData.Local_MediaPicture_Path = newpath;
                }
            }
            //string[] videoFilePath = path.Split('/');
            // Handheld.PlayFullScreenMovie("file://" + path);  
            //pickVideo(path);
        }
    }


    void closeEditor()
    {
        controlUIElements[0].SetActive(false);
        updatingExisting = false;
        editorWindow.SetActive(false);
    }

    /*
    public void loadTargetFromList(NewTargetData data)
    {
        updateData = data;
        updatingExisting = true;
        byte[] image = File.ReadAllBytes(Application.persistentDataPath + slash + updateData.Target_id + ".jpg");// Target Photo.
        LoadImage(image);
        targetNameField.text = updateData.TargetName;// Target Name.
        videoPath.text = updateData.VideoPath; // Target Video link.
        urlPath.text = updateData.Url;               // Target Target URL, Stream URL generated automatically.
        regularText.text = updateData.Text;         // Target Text Container
        isTrigger.isOn = !updateData.Tirggered;      // Target Trigger State
        imageChanged = false;
        if (updateData.Type == "STREAM")
        {
            ump.Path = "https://youtu.be/" + extractYoutubeVideoID(updateData.Url);
            StreamVideoPreview.SetActive(true);
            URLLinkPreview.SetActive(false);
            PicLinkActivity.SetActive(false);
            StartCoroutine(setAspectOnPreviewCanvases());

        }
        else if (updateData.Type == "URL")
        {
            StreamVideoPreview.SetActive(false);
            URLLinkPreview.SetActive(true);
            PicLinkActivity.SetActive(false);
        }
        else if (updateData.Type == "VIDEO")
        {
            PickVideoBlock.SetActive(false);
            VideoPreviewBlock.SetActive(true);
            string path = "";
            if (File.Exists(Application.persistentDataPath + "/" + updateData.Target_id + getFormat(updateData.VideoPath)))
                path = Application.persistentDataPath + "/" + updateData.Target_id + getFormat(updateData.VideoPath);
            else if (File.Exists(Application.persistentDataPath + "/" + updateData.TempName + getFormat(updateData.VideoPath)))
                path = Application.persistentDataPath + "/" + updateData.TempName + getFormat(updateData.VideoPath);
            else if (File.Exists(updateData.VideoPath))
                path = updateData.VideoPath;
            if (path != "")
            {
                ump.Path = path;
                StartCoroutine(setAspectOnPreviewCanvases());
            }
        }
        else if (updateData.Type == "PICTURE")
        {
            string path = "";
            if (File.Exists(Application.persistentDataPath + "/P_" + updateData.Target_id + getFormat(updateData.VideoPath)))
                path = Application.persistentDataPath + "/P_" + updateData.Target_id + getFormat(updateData.VideoPath);
            else if (File.Exists(Application.persistentDataPath + "/P_" + updateData.TempName + getFormat(updateData.VideoPath)))
                path = Application.persistentDataPath + "/P_" + updateData.TempName + getFormat(updateData.VideoPath);
            else if (File.Exists(updateData.VideoPath))
                path = updateData.VideoPath;
            if (path != "")
            {
                StartCoroutine(pickTargetCanvasImage(path));
            }
        }
    }
    */

    public void enableCropMode()
    {
        if (!isCropOn)
        {
            cropFrame.gameObject.SetActive(true);
            updateSize();
            for (int i = 0; i < controlUIElements.Length; i++)
                controlUIElements[i].SetActive(false);
            isCropOn = true;
        }
        else
        {
            crop();
            cropFrame.localScale = new Vector3(1, 1, 1);
            cropFrame.anchoredPosition = new Vector3(0, 0, 0);
            for (int i = 0; i < cropFrame.GetComponent<DragResize>().HandlePoints.Length; i++)
                cropFrame.GetComponent<DragResize>().HandlePoints[i].anchoredPosition = new Vector2(-3000, -3000);
            cropFrame.gameObject.SetActive(false);
            for (int i = 0; i < controlUIElements.Length; i++)
                    controlUIElements[i].SetActive(true);

            isCropOn = false;
        }
    }

    public void crop()
    {
        if (cropFrame.GetComponent<DragResize>().resized)
        {
            Vector2 position = cropFrame.GetComponent<DragResize>().getPosBounds();
            Debug.Log("Y POSITION: " + (int)(position.y * TargetTexture.height));
            Color[] c = TargetTexture.GetPixels((int)(position.x * TargetTexture.width), (int)(position.y * TargetTexture.height), (int)(cropFrame.localScale.x * TargetTexture.width), (int)(cropFrame.localScale.y * TargetTexture.height));
            Texture2D m2Texture = new Texture2D((int)(cropFrame.localScale.x * (float)TargetTexture.width), (int)(cropFrame.localScale.y * (float)TargetTexture.height));
            m2Texture.SetPixels(c);
            m2Texture.Apply();
            TargetTexture = m2Texture;
            imageRenderer.GetComponent<AspectRatioFitter>().aspectRatio = ((float)TargetTexture.width / (float)TargetTexture.height);
            imageRenderer.texture = TargetTexture;
        }
        updateSize();
        imageChanged = true;
        TargetData.Last_Modified_Date = string.Format("{0:r}", System.DateTime.Now.ToUniversalTime());
    }

    public string extractYoutubeVideoID(string link)
    {
        string output = "";
        string[] youtubeParser = link.Split('=', '/');
        for (int i = 0; i < youtubeParser.Length; i++)
            if (youtubeParser[i].Contains("watch?v") || youtubeParser[i].Contains("&v") || youtubeParser[i].Contains("?v"))
            {
                for (int c = 0; c < 11; c++)
                {
                    output += youtubeParser[i + 1][c];
                }
                break;
            }
        return output;
    }

    void updateSize()
    {
        cropFrame.GetComponent<RectTransform>().sizeDelta = imageRenderer.GetComponent<RectTransform>().sizeDelta;
        cropFrame.GetComponent<RectTransform>().anchoredPosition = imageRenderer.GetComponent<RectTransform>().anchoredPosition;
        cropFrame.GetComponent<DragResize>().handlersPositionReset();
        cropFrame.GetComponent<DragResize>().resized = false;
    }

  

   /* public void back(GameObject objectToHide)
    {
        if (updatingExisting && editedPrefxText().Length == 0)
        {
            objectToHide.SetActive(false);
            Application.LoadLevel(0);
            return;
        }
        Message.showDialog("Are you sure you want to quit from target editor?", DialogIcon.Info, DialogType.YesNo, () =>
        {
            objectToHide.SetActive(false);
            Application.LoadLevel(0);
        });
    }*/

    public void convert()
    {
        StartCoroutine(convefrt(@"C:\Users\User\Pictures\", "Fishes2.jpg", "keyTest.jpg"));
    }

    IEnumerator convefrt(string path, string fileName, string keyFileName)
    {
        byte[] input = File.ReadAllBytes(path + fileName);
        byte[] key = File.ReadAllBytes(path + keyFileName);
        string newByte = "";
        string acceptedByte = "";
        string wholeFile = "";
        string keyFile = "";
        string newFile = "";
        byte[] converted = new byte[input.Length];
        for (int i = 0; i < input.Length; i++)
        {
            if (input[i].ToString().Length == 3)
                wholeFile += "" + input[i];
            else if (input[i].ToString().Length == 2)
                wholeFile += "0" + input[i];
            else if (input[i].ToString().Length == 1)
                wholeFile += "00" + input[i];
            else wholeFile += "000";
            if (i % 1024 == 0)
            {
                //progress.text = ("byte:" + input[i].ToString() + ", File A: " + ((float)i / (float)input.Length) * 100.0f);
                yield return null;
            }
        }
        for (int i = 0; i < key.Length; i++)
        {
            if (key[i].ToString().Length == 3)
                keyFile += "" + key[i];
            else if (key[i].ToString().Length == 2)
                keyFile += "0" + key[i];
            else if (key[i].ToString().Length == 1)
                keyFile += "00" + key[i];
            else keyFile += "000";
            if (i % 1024 == 0)
            {
               // progress.text = ("byte:" + input[i].ToString() + ", Key File: " + ((float)i / (float)input.Length) * 100.0f);
                yield return null;
            }
        }
        yield return null;
        Debug.Log("Computing encrypted file...");
        newFile = StringMath.Sub(wholeFile, keyFile);
        yield return null;
        Debug.Log("Rest Length: " + newFile.Length);
        yield return null;
        Debug.Log("Started converting back to bytes...");
        yield return null;
        int byteNum = 0;
        for (int x = 0; x < newFile.Length;)
        {
            for (int newB = 0; newB < 3; newB++)
            {
                newByte += newFile[x + newB];
                if (newByte.Length > 0)
                    if (int.Parse(newByte) < 256)
                        acceptedByte = newByte;
            }
            converted[byteNum] = byte.Parse(acceptedByte);
            byteNum++;
            newByte = "";
            x += acceptedByte.Length;
            if (x % 900 == 0)
            {
                //progress.text = ("To Bytes: " + ((float)x / (float)wholeFile.Length) * 100.0f);
                yield return null;
            }
        }
        Debug.Log("Writing Bytes to File...");
        yield return null;
        File.WriteAllBytes(path + "RESULT_enc.jpg", converted);
        Debug.Log("Writing Bytes Completed!");
    }

   /* public string updateParameters()
    {
        string editedPrefx = "";
        if (updatingExisting)
        {
            if (targetNameField.text != updateData.TargetName && !editedPrefx.Contains("N"))
                editedPrefx += "N";
            if (urlPath.text != updateData.StreamUrl && !editedPrefx.Contains("M"))
                editedPrefx += "M";
            if (urlPath.text != updateData.Url && !editedPrefx.Contains("M"))
                editedPrefx += "M";
            if (regularText.text != updateData.Text && !editedPrefx.Contains("M"))
                editedPrefx += "M";
            if (isTrigger.isOn == updateData.Tirggered && !editedPrefx.Contains("M"))
                editedPrefx += "M";
            if (getUploadType() != getUploadType(JsonUtility.ToJson(updateData)) && !editedPrefx.Contains("M"))
                editedPrefx += "M";
            if (urlPath.text.Length > 0 && videoPath.text.Length > 0 && !editedPrefx.Contains("M"))
                editedPrefx += "M";
            if (imageChanged)
                editedPrefx += "I";
        }
        return editedPrefx;
    }*/

  /*  string editedPrefxText()
    {
        string output = "";
        if (updatingExisting)
        {
            if (targetNameField.text != updateData.TargetName)
                output += "new target name, ";
            if (videoPathFull != "" && videoPath.text != updateData.VideoPath)
                output += "new mp4 video, ";
            else if (videoPath.text != updateData.VideoPath && videoPathFull == "")
                output += "video removed, ";
            if (urlPath.text.Length > 0 && videoPath.text.Length > 0)
                output += "new url(attached mp4 video will be removed), ";
            else if (urlPath.text != updateData.Url)
            {
                if (urlPath.text.Length > 0)
                    output += "new url, ";
                else output += "url removed, ";
            }
            if (regularText.text != updateData.Text)
            {
                if (regularText.text.Length > 0)
                    output += "new text, ";
                else output += "text removed, ";
            }
            if (isTrigger.isOn == updateData.Tirggered)// false - trigger enambled, true - trigger disabled.
            {
                if (isTrigger.isOn)
                    output += "trigger Disabled, ";
                else output += "trigger Enbaled, ";
            }
            if (imageChanged)
                output += "changed image, ";
        }
        return output;
    }*/

    public void quit()
    {
        Message.showDialog("Are you sure you want to quit from the application?", DialogIcon.Info, DialogType.YesNo, () => Application.Quit(), null);
    }

    public void PickPicture()// new func
    {
        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
        {
            Debug.Log("Video path: " + path);
            ProcessMediaFromPath(path);
        }, "Select a video");
        Debug.Log("Permission result: " + permission);
    }

    public void logJsCallback(string js)
    {
       // jsonContainsText.text += "\n$" + js;
    }

    public void PickPictureForTargetCanvas()
    {
        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
        {
            Debug.Log("Video path: " + path);
            if (path != null)
            {
                StartCoroutine(pickTargetCanvasImage(path));
            }
        }, "Select a video");
        Debug.Log("Permission result: " + permission);
    }

    public void TakeNativePicture()
    {
        TakeNativePicture(512);
    }

    public void PlayUmpAndSetVideoAspect()
    {
        PreviewWindow.SetActive(true);
        ump.Play();
        StartCoroutine(setAspectOnPreviewCanvases());
    }

    public void BrowseVideoFile()
    {
        BrowseVideo();
    }
    public void RecordNewVideo()
    {
        ump.Stop();
        RecordVideo();
    }

    private void TakeNativePicture(int maxSize)
    {
        NativeCamera.Permission permission = NativeCamera.TakePicture((path) =>
        {
            Debug.Log("Image path: " + path);
            ProcessMediaFromPath(path);
        }, maxSize);
        Debug.Log("Permission result: " + permission);
    }

    public void OpenURL(string url)
    {
        InAppBrowser.OpenURL(url, displayOptions);
    }

    public void OpenOnceURL(string url)
    {
        if(TargetData.Local_URL_Link == "")
        InAppBrowser.OpenURL(url, displayOptions);
        else InAppBrowser.OpenURL(TargetData.Local_URL_Link, displayOptions);
    }

    public string ParseYoutubeLink(string InitialLink)
    {
        //https://www.youtube.com/watch?v=kJ-BWzOwbI4&feature=youtu.be&app=desktop
        string[] ax;
        if (InitialLink.Contains("=") && InitialLink.Contains("&"))
            ax = InitialLink.Split('=', '&');
        else if (InitialLink.Contains("="))
            ax = InitialLink.Split('=');
        else if (InitialLink.Contains("&"))
            ax = InitialLink.Split('&');
        else return InitialLink;
        for (int i = 0; i < ax.Length; i++)
        {
            Debug.Log("Link Elements "+i+"/"+ax.Length+": " + ax[i]);
            if (i + 1< ax.Length && ax[i].Contains("v"))
                return "https://youtu.be/" + ax[i + 1];
        }
        return InitialLink;
    }

    public void LastPage(string message)
    {
        if (message.Contains("youtu.be") ||
               message.Contains("www.youtu") ||
               message.Contains("m.youtu"))
        {
            TargetData.Local_URL_Link = ParseYoutubeLink(message);
            URL.text = ParseYoutubeLink(message);
        }
        else if(!message.Contains("https://") && !message.Contains("http://"))
        {
            TargetData.Local_URL_Link = "https://"+ message;
            URL.text = "https://" + message;
        }
        else
        {
            TargetData.Local_URL_Link = message;
            URL.text = message;
        }
        if (string.IsNullOrEmpty(message))
        {
            RemoveLink();
        }
        else
        {
            controlUIElements[2].SetActive(false);
            LinkSlider.targetRequest = true;
            // Additional actions to hide inputfield
            URL.gameObject.SetActive(false);
            controlUIElements[8].SetActive(true);
            controlUIElements[9].SetActive(true);
        }
    }

    public void PreviewLink()
    {
        if (URL.text.Contains("youtu.be") ||
               URL.text.Contains("www.youtu") ||
               URL.text.Contains("m.youtu"))
        {
            ump.Stop();
            ump.Path = URL.text;
            LinkSlider.targetRequest = true;
            PreviewWindow.SetActive(true);
            playPauseButton.SetActive(true);
            PreviewWindow.transform.GetChild(0).GetComponent<Button>().interactable = true;
            PreviewWindow.transform.GetChild(0).GetComponent<CanvasRenderer>().SetAlpha(1.0f);
            StartCoroutine(setAspectOnPreviewCanvases());
            Screen.orientation = ScreenOrientation.AutoRotation;
        }
        else
        {
            if (!InAppBrowser.IsInAppBrowserOpened())
                InAppBrowser.OpenURL(URL.text, displayOptions);
        }
    }
    public void RemoveLink()
    {
        TargetData.Local_URL_Link = "";
        URL.text = "";
        URL.gameObject.SetActive(true);
        controlUIElements[2].SetActive(true);
        URL.gameObject.SetActive(true);
        controlUIElements[8].SetActive(false);
        controlUIElements[9].SetActive(false);
    }

    public void OpenURL()
    {
      //  InAppBrowser.OpenURL(urlPath.text);
    }

    public void pickVideo(string path)
    {
        ump.Path = path;
        StartCoroutine(setAspectOnPreviewCanvases());
    }

    IEnumerator setAspectOnPreviewCanvases()
    {
        playPauseButton.SetActive(true);
        ump.Play();
        while (!ump.IsPlaying)
            yield return null;
        PreviewWindow.GetComponent<AspectRatioFitter>().aspectRatio = ((float)ump.VideoWidth / (float)ump.VideoHeight);
    }



    public string getFormat(string path)
    {
        string[] fileFormat = path.Split('.');
        return "." + fileFormat[fileFormat.Length - 1];
    }

    public void rotateImage()
    {
        /*Texture2D renTex = new Texture2D(imageRenderer.texture.width, imageRenderer.texture.height);
        int KernelHandler = customShader.FindKernel("RotateTexture");
        customShader.SetInt("width", renTex.width);
        customShader.SetInt("height", renTex.height);
        customShader.SetTexture(KernelHandler, "Result", renTex);
        customShader.SetTexture(KernelHandler, "InputTexture", imageRenderer.texture);
        customShader.Dispatch(KernelHandler, renTex.width / 8, renTex.height / 8, 1);*/
        TargetTexture = rotateTexture(TargetTexture, true);
        imageRenderer.GetComponent<AspectRatioFitter>().aspectRatio = ((float)TargetTexture.width / (float)TargetTexture.height);
        imageRenderer.texture = TargetTexture;
        imageChanged = true;
        TargetData.Last_Modified_Date = string.Format("{0:r}", System.DateTime.Now.ToUniversalTime());
        TargetData.Force_TargetPicture_Update = true;
    }

    public void showCloseDialog()
    {
        Message.showDialog("Are you sure you want to quit from target editor?", DialogIcon.Warning, DialogType.YesNo, () => editorWindow.SetActive(false));
    }

    Texture2D rotateTexture(Texture2D originalTexture, bool clockwise)
    {
        Color32[] original = originalTexture.GetPixels32();
        Color32[] rotated = new Color32[original.Length];
        int w = originalTexture.width;
        int h = originalTexture.height;

        int iRotated, iOriginal;

        for (int j = 0; j < h; ++j)  
        {
            for (int i = 0; i < w; ++i)
            {
                iRotated = (i + 1) * h - j - 1;
                iOriginal = clockwise ? original.Length - 1 - (j * w + i) : j * w + i;
                rotated[iRotated] = original[iOriginal];
            }
        }

        Texture2D rotatedTexture = new Texture2D(h, w);
        rotatedTexture.SetPixels32(rotated);
        rotatedTexture.Apply();
        return rotatedTexture;
    }

    static Color32[] RotateMatrix(Color32[] matrix, int n)
    {
        Color32[] ret = new Color32[n * n];

        for (int i = 0; i < n; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                ret[i * n + j] = matrix[(n - j - 1) * n + i];
            }
        }

        return ret;
    }

    private void RecordVideo()
    {
        NativeCamera.Permission permission = NativeCamera.RecordVideo((path) =>
        {
            Debug.Log("Video path: " + path);
            ProcessMediaFromPath(path);
        });
        Debug.Log("Permission result: " + permission);
    }

    string toSmallChars(string input)
    {
        string output = input;
        string big = "QWERTYUIOPASDFGHJKLZXCVBNM";
        string small = "qwertyuiopasdfghjklzxcvbnm";
        for (int x = 0; x < input.Length; x++)
            for (int y = 0; y < big.Length; y++)
                output = output.Replace(big[y], small[y]);
        return output;
    }

    public void ProcessMediaFromPath(string path)
    {
        Debug.LogWarning("Processing Taken Madia, at path: " + path);
        if (!string.IsNullOrEmpty(path))
        {
            string[] s = path.Split('.');
            string format = toSmallChars(s[s.Length - 1]);
            Debug.LogWarning("Processing Taken Madia, detected format: " + format);
            if (format == "mp4" || format == "mov")
            {
                // Play the recorded video
                //targetElement.Local_Video_Path = path;

                string newName = "video_" + Random.Range(00000, 99999) + "." + format;
                Debug.LogWarning("NEW NAME: " + newName);
                string newpath = Application.persistentDataPath + slash + UserManager.user_id + slash + "LocalData";
                Debug.LogWarning("NEW DIR: " + newpath + " EXISTS: " + Directory.Exists(newpath));
                if (Directory.Exists(newpath))
                {
                    newpath += slash + newName;
                    File.Move(path, newpath);
                    Debug.LogWarning("NEW FILE LOC EXISTS: " + Directory.Exists(newpath));
                    Debug.LogWarning("Processing Taken Madia, at path: " + path);
                    TargetData.Local_Video_Path = newpath;
                    TargetData.Local_MediaPicture_Path = "";
                }
                else
                {
                    Debug.LogWarning("CREATING DIR: " + newpath);
                    Directory.CreateDirectory(newpath);
                    Debug.LogWarning("NEW DIR: " + newpath + " EXISTS: " + Directory.Exists(newpath));
                    if (Directory.Exists(newpath))
                    {
                        newpath += slash + newName;
                        File.Move(path, newpath);
                        Debug.LogWarning("NEW FILE LOC EXISTS: " + Directory.Exists(newpath));
                        TargetData.Local_Video_Path = newpath;
                        TargetData.Local_MediaPicture_Path = "";
                    }
                }
                controlUIElements[1].SetActive(false);
                //string[] videoFilePath = path.Split('/');
                //videoPath.text = videoFilePath[videoFilePath.Length - 1];
                // Handheld.PlayFullScreenMovie("file://" + path);  
                //pickVideo(path);
                ump.Path = TargetData.Local_Video_Path;
                TargetData.Local_MediaPicture_Path = "";
                MediaSlider.targetRequest = false;
            }
            else
            {
                // Play the recorded video
                //targetElement.Local_Video_Path = path;
                string newName = "pic_" + Random.Range(00000, 99999) + "." + format;
                string newpath = Application.persistentDataPath + slash + UserManager.user_id + slash + "LocalData";
                Debug.LogWarning("DIR: " + newpath + " EXISTS: " + Directory.Exists(newpath));
                if (Directory.Exists(newpath))
                {
                    newpath += slash + newName;
                    File.Move(path, newpath);
                    TargetData.Local_MediaPicture_Path = newpath;
                    TargetData.Local_Video_Path = "";
                }
                else
                {
                    Debug.LogWarning("CREATING DIR: " + newpath);
                    Directory.CreateDirectory(newpath);
                    Debug.LogWarning("NEW DIR: " + newpath + " EXISTS: " + Directory.Exists(newpath));
                    if (Directory.Exists(newpath))
                    {
                        newpath += slash + newName;
                        File.Move(path, newpath);
                        TargetData.Local_MediaPicture_Path = newpath;
                        TargetData.Local_Video_Path = "";
                    }
                }
                controlUIElements[1].SetActive(false);
                //string[] videoFilePath = path.Split('/');
                //videoPath.text = videoFilePath[videoFilePath.Length - 1];
                // Handheld.PlayFullScreenMovie("file://" + path);  
                //pickVideo(path);
                ump.Path = TargetData.Local_MediaPicture_Path;
                TargetData.Local_Video_Path = "";
                MediaSlider.targetRequest = false;
                ShowMediaPreview();
            }
            ShowMediaPreview();
        }
        else
        {
            MediaSlider.targetRequest = true;
            controlUIElements[1].SetActive(true);
            //PickVideoBlock.SetActive(true);
            //VideoPreviewBlock.SetActive(false);
            RemoveMediaLink();
        }
    }

    public void ShowMediaPreview()
    {
        if (!string.IsNullOrEmpty(ump.Path))
        {
            string[] s = ump.Path.Split('.');
            string format = toSmallChars(s[s.Length - 1]);
            if (format == "mp4" || format == "mov")
            {
                PreviewWindow.SetActive(true);
                PreviewWindow.transform.GetChild(0).GetComponent<Button>().interactable = true;
                PreviewWindow.transform.GetChild(0).GetComponent<CanvasRenderer>().SetAlpha(1.0f);
                StartCoroutine(setAspectOnPreviewCanvases());
            }
            else
            {
                ump.Stop();
                PreviewWindow.SetActive(true);
                StartCoroutine(LoadImage(ump.Path, PreviewWindow.GetComponent<RawImage>()));
            }
            Screen.orientation = ScreenOrientation.Portrait;
        }
        else
        {
            PreviewWindow.SetActive(false);
        }
        //UITransactionManager.Transact(PreviewWindow.GetComponent<CanvasRenderer>(), UITransactionManager.CanvasTransaction.FadeZoomIn, true);
    }



    public void RemoveMediaLink()
    {
        ump.Stop();
        TargetData.Local_Video_Path = "";
        MediaSlider.targetRequest = true;
        controlUIElements[1].SetActive(true);
        PreviewWindow.SetActive(false);
        Screen.orientation = ScreenOrientation.Portrait;
    }

    public void setScreenPortrait()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }

    private void BrowseVideo()
    {
        NativeGallery.Permission permission = NativeGallery.GetVideoFromGallery((path) =>
        {
            Debug.Log("Video path: " + path);
            ProcessMediaFromPath(path);
        }, "Select a video");
        Debug.Log("Permission result: " + permission);
    }

    public void enableCrop()
    {
        if (cropEffect != null)
            StopCoroutine(cropEffect);
        cropEffect = StartCoroutine(cropEnable());
    }

    IEnumerator waitUntilStartsToPlay()
    {
        while (!ump.IsPlaying)
            yield return null;

        
    }

    IEnumerator cropEnable()
    {
        float newScale = 1.0f;
        Vector2 screen = new Vector2(Screen.width, Screen.height);
        if (!isCropOn)
        {
            setActive(false);
            if (imageRenderer.GetPixelAdjustedRect().size.x > screen.x)
            newScale = (screen.x / imageRenderer.GetPixelAdjustedRect().size.x)*0.9f;
            else if (imageRenderer.GetPixelAdjustedRect().size.y > screen.y)
                newScale = (screen.y / imageRenderer.GetPixelAdjustedRect().size.y) * 0.9f;
            for (float i = 0; i < 0.7f; i += Time.deltaTime * 1.5f)
            {
                imageRenderer.transform.localScale = Vector3.Lerp(imageRenderer.transform.localScale, new Vector3(newScale, newScale, newScale), i);
                yield return null;
            } 
            imageRenderer.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
            imageRenderer.GetComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.FitInParent;
            enableCropMode();
        }
        else
        {
            setActive(true);
            enableCropMode();
            yield return new WaitForEndOfFrame();
            if (imageRenderer.GetPixelAdjustedRect().size.x < screen.x)
                newScale = (screen.x / imageRenderer.GetPixelAdjustedRect().size.x) * 0.9f;
            else if (imageRenderer.GetPixelAdjustedRect().size.y < screen.y)
                newScale = (screen.y / imageRenderer.GetPixelAdjustedRect().size.y) * 0.9f;
            for (float i = 0; i < 0.7f; i += Time.deltaTime * 1.5f)
            {
                imageRenderer.transform.localScale = Vector3.Lerp(imageRenderer.transform.localScale, new Vector3(newScale, newScale, newScale), i);
                yield return null;
            }
            imageRenderer.transform.localScale = new Vector3(1, 1, 1);
            imageRenderer.GetComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
            TargetData.Force_TargetPicture_Update = true;
        }
    }

    public void showErrorMsg()
    {
        Message.showDialog("This feature temporary anavailable.", DialogIcon.Error);
    }

    public void setActive(bool active)
    {
        if (fadeSetActive != null)
            StopCoroutine(fadeSetActive);
        fadeSetActive = StartCoroutine(setActive(controlUIElements, active));
    }

    IEnumerator setActive(GameObject[] objects, bool active)
    {
        for (float i = 0; i < 1.0f; i += Time.deltaTime * 2.5f)
        {
            for (int o = 0; o < controlUIElements.Length; o++)
            {
                CanvasRenderer[] cr = controlUIElements[o].GetComponentsInChildren<CanvasRenderer>();
                foreach (CanvasRenderer c in cr)
                    if (active)
                        c.SetAlpha(i);
                    else c.SetAlpha(1.0f - i);
            }
                yield return null;
        }
        for (int i = 0; i < controlUIElements.Length; i++)
            controlUIElements[i].SetActive(active);
        if (string.IsNullOrEmpty(URL.text))
        {
            RemoveLink();
        }
    }
}
[System.Serializable]
public class NewTargetData
{
    public string Target_id;
    public string TempName;
    public string imagePath;
    public string TargetName;
    public string VideoPath;
    public string StreamUrl;
    public string Url;
    public string Text;
    public string Type;
    public int quality;
    public bool Tirggered;
}

public class TargetsArray
{
    public string[] target_ids;
    public int[] quality;
}

