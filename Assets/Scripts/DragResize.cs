﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DragResize : MonoBehaviour {

     RectTransform cropFrame;
    public RectTransform imageFrame;
    public RectTransform[] HandlePoints;
    Vector2 minimalPosition;
    Vector2 maximalPosition;
    bool[] HandleMoveActive;
    public Vector2 OriginalGapPosition;
    public Vector2 GapPosition;
    public Vector2 originalGapSize;
    public Vector2 GapSize;
    Vector3 startPos;
    Vector3 delta;
    public bool resized = false;

    // Use this for initialization
    void Start () {
        cropFrame = GetComponent<RectTransform>();
        HandleMoveActive = new bool[HandlePoints.Length];
        for (int i = 0; i < HandlePoints.Length; i++)
            HandleMoveActive[i] = false;
    }

    // Update is called once per frame
    void Update()
    {
        calculateTouchPhase();
    }

    private void calculateTouchPhase()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            // Handle finger movements based on TouchPhase
            switch (touch.phase)
            {
                //When a touch has first been detected, change the message and record the starting position
                case TouchPhase.Began:
                    // Record initial touch position.
                    startPos = touch.position;
                    break;

                //Determine if the touch is a moving touch
                case TouchPhase.Moved:
                    resized = true;
                    // Determine direction by comparing the current touch position with the initial one
                    if (new Vector2(startPos.x, startPos.y) != touch.position)
                    {
                        delta = touch.position - new Vector2(startPos.x, startPos.y);
                        for (int i = 0; i < HandlePoints.Length; i++)
                        {
                            if (HandleMoveActive[i])
                                HandlePoints[i].anchoredPosition += new Vector2(delta.x, delta.y);
                        }
                        HandlePoints[0].position = new Vector2(Mathf.Clamp(HandlePoints[0].position.x, minimalPosition.x, HandlePoints[1].position.x), Mathf.Clamp(HandlePoints[0].position.y, HandlePoints[1].position.y, minimalPosition.y));
                        HandlePoints[1].position = new Vector2(Mathf.Clamp(HandlePoints[1].position.x, HandlePoints[0].position.x, maximalPosition.x), Mathf.Clamp(HandlePoints[1].position.y, maximalPosition.y, HandlePoints[0].position.y));
                        GapSize = new Vector2(HandlePoints[1].position.x - HandlePoints[0].position.x, HandlePoints[1].position.y - HandlePoints[0].position.y);
                        GapPosition = new Vector2(OriginalGapPosition.x - HandlePoints[0].position.x, OriginalGapPosition.y - HandlePoints[0].position.y);
                        if (originalGapSize.x != 0 && originalGapSize.y != 0)
                        {
                            cropFrame.localScale = new Vector2(Mathf.Clamp((GapSize / originalGapSize).x, 0.1f, 1.0f), Mathf.Clamp((GapSize / originalGapSize).y, 0.1f, 1.0f));
                            cropFrame.position = (HandlePoints[0].position + HandlePoints[1].position) / 2.0f;
                            Debug.Log("POS: " + getPosBounds() + "GAP SIZE: " + (GapSize / originalGapSize));
                            Debug.Log("HANDLE 0 POS: " + HandlePoints[0].position);
                            Debug.Log("HANDLE 1 POS: " + HandlePoints[1].position);
                            startPos = touch.position;
                        }
                    }
                    break;

                case TouchPhase.Ended:
                    // Report that the touch has ended when it ends
                    break;
            }
            return;
        }
        if (Input.GetMouseButtonDown(0))
        {
            startPos = Input.mousePosition;
        }
        if (Input.GetMouseButton(0) && Input.mousePosition != startPos)
        {
            delta = Input.mousePosition - startPos;
            for (int i = 0; i < HandlePoints.Length; i++)
            {
                if (HandleMoveActive[i])
                    HandlePoints[i].anchoredPosition += new Vector2(delta.x, delta.y);
            }
            resized = true;
            HandlePoints[0].position = new Vector2(Mathf.Clamp(HandlePoints[0].position.x, minimalPosition.x, HandlePoints[1].position.x), Mathf.Clamp(HandlePoints[0].position.y, HandlePoints[1].position.y, minimalPosition.y));
            HandlePoints[1].position = new Vector2(Mathf.Clamp(HandlePoints[1].position.x, HandlePoints[0].position.x, maximalPosition.x), Mathf.Clamp(HandlePoints[1].position.y, maximalPosition.y, HandlePoints[0].position.y));
            GapSize = new Vector2(HandlePoints[1].position.x - HandlePoints[0].position.x, HandlePoints[1].position.y - HandlePoints[0].position.y);
            GapPosition = new Vector2(OriginalGapPosition.x - HandlePoints[0].position.x, OriginalGapPosition.y - HandlePoints[0].position.y);
            if (originalGapSize.x != 0 && originalGapSize.y != 0)
            {
                cropFrame.localScale = new Vector2(0.0001f + Mathf.Clamp((GapSize / originalGapSize).x, 0.1f, 1.0f), 0.0001f + Mathf.Clamp((GapSize / originalGapSize).y, 0.1f, 1.0f));
                cropFrame.position = (HandlePoints[0].position + HandlePoints[1].position) / 2.0f;
                Debug.Log("POS: " + getPosBounds() + "GAP SIZE: " + (GapSize / originalGapSize));
                Debug.Log("HANDLE 0 POS: " + HandlePoints[0].position);
                Debug.Log("HANDLE 1 POS: " + HandlePoints[1].position);
            }
            startPos = Input.mousePosition;
        }
    }
    public void enableHandle(int handleNum)
    {
        HandleMoveActive[handleNum] = true;
    }

    public void handlersPositionReset()
    {
            Debug.Log("HANDLE 0 POS: " + HandlePoints[0].position);
        Debug.Log("HANDLE 1 POS: " + HandlePoints[1].position);
        Debug.Log("CROP FRAME MIN OFFSET: " + cropFrame.offsetMin + ", MAX OFFSET: " + cropFrame.offsetMax);
        cropFrame.localScale = new Vector3(1, 1, 1);
        HandlePoints[0].anchoredPosition = new Vector2(0,0);
        HandlePoints[1].anchoredPosition = new Vector2(0,0);
        OriginalGapPosition = new Vector2(HandlePoints[1].position.x, HandlePoints[1].position.y);
        originalGapSize = new Vector2(HandlePoints[1].position.x- HandlePoints[0].position.x, HandlePoints[1].position.y - HandlePoints[0].position.y);
        GapSize = new Vector2(HandlePoints[1].position.x - HandlePoints[0].position.x, HandlePoints[1].position.y - HandlePoints[0].position.y);
        minimalPosition = HandlePoints[0].position;
        maximalPosition = HandlePoints[1].position;
    }

    public Vector2 getPosBounds()
    {
        return new Vector2((new Vector2(1.0f, 1.0f) - GapPosition / originalGapSize).x, 1.0f-((new Vector2(1.0f, 1.0f) - GapPosition / originalGapSize).y+cropFrame.localScale.y));
    }

    public void disableHandle(int handleNum)
    {
        HandleMoveActive[handleNum] = false;
    }
}
