﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Screenshoter : MonoBehaviour
{

    public int resWidth = 2550;
    public int resHeight = 3300;
    public Camera camInstance;
    public bool takeScreenShot = false;

    public static string ScreenShotName()
    {
        string name = string.Format("{0}/ar-image_" + Random.Range(00000, 99999) + ".jpg", Application.persistentDataPath);
        PlayerPrefs.SetString("AR_IMAGE_PATH", name);
        return name;
    }

    public void TakeHiResShot()
    {
        takeScreenShot = true;
    }
   
    public void back()
    {
        LevelState ll = new LevelState();
        ll.targetListActive = false;
        ll.newTargetMenu = false;
        ll.imageEditor = false;
        PlayerPrefs.SetString("LEVEL_STATE", JsonUtility.ToJson(ll));
        Application.LoadLevel(0);
    }

    public void apply()
    {
        LevelState ll = new LevelState();
        ll.targetListActive = false;
        ll.newTargetMenu = true;
        ll.imageEditor = true;
        ll.image_path = PlayerPrefs.GetString("AR_IMAGE_PATH");
        PlayerPrefs.DeleteKey("AR_IMAGE_PATH");
        PlayerPrefs.SetString("LEVEL_STATE", JsonUtility.ToJson(ll));
        Application.LoadLevel(0);
    }

    void LateUpdate()
    {
        if (takeScreenShot)
        {
            resWidth = Screen.width;
            resHeight = Screen.height;
            RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
            camInstance.targetTexture = rt;
            Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
            camInstance.Render();
            RenderTexture.active = rt;
            screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
            camInstance.targetTexture = null;
            RenderTexture.active = null; // JC: added to avoid errors
            Destroy(rt);
            byte[] bytes = screenShot.EncodeToJPG();
            string filename = ScreenShotName();
            System.IO.File.WriteAllBytes(filename, bytes);
            Debug.Log(string.Format("Took screenshot to: {0}", filename));
            takeScreenShot = false;
        }
    }
}
