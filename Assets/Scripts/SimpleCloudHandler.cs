﻿using UnityEngine;
using UnityEngine.UI;
using Vuforia;
using System.Collections;

public class SimpleCloudHandler : MonoBehaviour, ICloudRecoEventHandler
{
    private CloudRecoBehaviour mCloudRecoBehaviour;
    public GameObject spearCloudObject;
    public UniversalMediaPlayer ump;
    private bool mIsScanning = false;
    private string mTargetMetadata = "";
    public ImageTargetBehaviour ImageTargetTemplate;
    public Text CloudStatusText;
    public RawImage canvasTest;
    Coroutine statusCorotine;
    Coroutine aspectCoroutine;
    public static string key1 = "";

    // Use this for initialization
    void Start()
    {
       // ump.Path = "file:///" + Application.persistentDataPath + @"\" + "Video1.mp4";
        // register this event handler at the cloud reco behaviour
        
        mCloudRecoBehaviour = GetComponent<CloudRecoBehaviour>();
        mCloudRecoBehaviour.AccessKey = KeyManager.key1;
        mCloudRecoBehaviour.SecretKey = KeyManager.key2;
        if (mCloudRecoBehaviour)
        {
            mCloudRecoBehaviour.RegisterEventHandler(this);
        }
    }

    public void OnInitError(TargetFinder.InitState initError)
    {
        Debug.Log("Cloud initialization error " + initError.ToString());
        CloudStatusText.text = "Cloud initialization error \n" + initError.ToString();
    }

    public void OnInitialized()
    {
        Debug.Log("Cloud Reco initialized");
        if (!CloudStatusText.text.Contains("SUSPENDED"))
        {
            key1 = mCloudRecoBehaviour.AccessKey;
            if (statusCorotine != null)
                StopCoroutine(statusCorotine);
            statusCorotine = StartCoroutine(showStatus(0, "Connecting to " + gameObject.name + "...", 5));
        }
    }

    public void OnNewSearchResult(TargetFinder.TargetSearchResult targetSearchResult)
    {
        CloudStatusText.text = "Detected Target: \n" + targetSearchResult.TargetName + ",\n ID: "+targetSearchResult.UniqueTargetId;
        // do something with the target metadata        
        TargetFinder.CloudRecoSearchResult cloudResult = (TargetFinder.CloudRecoSearchResult)targetSearchResult;
        mTargetMetadata = cloudResult.MetaData;
        // stop the target finder (i.e. stop scanning the cloud)
        mCloudRecoBehaviour.CloudRecoEnabled = true;
       // int linkNum = int.Parse(targetSearchResult.TargetName.Split('_')[1]);
        ImageTargetBehaviour newTarget = Instantiate(ImageTargetTemplate); //MAZ-18 Fix
        // Build augmentation based on target
        if (newTarget)
        {
            // enable the new result with the same ImageTargetBehaviour:
            ObjectTracker tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
            ImageTargetBehaviour imageTargetBehaviour =(ImageTargetBehaviour)tracker.TargetFinder.EnableTracking(targetSearchResult, newTarget.gameObject);
            newTarget.gameObject.name = targetSearchResult.TargetName;
            Debug.Log("NEW TARGET DETECTED: " + targetSearchResult.TargetName);
            newTarget.GetComponent<VideoTrackableEventHandler>().DeserializeMetadata(cloudResult.MetaData, targetSearchResult.UniqueTargetId);
            newTarget.GetComponent<VideoTrackableEventHandler>().ump = ump;            
            addObjectToUmpArray(newTarget.GetComponent<VideoTrackableEventHandler>().PhisicalObjCanvas.gameObject);      //Why do this?
        }
    }

    public void addObjectToUmpArray(GameObject newObject) //TODO : FIX Memory leak semi source
    {
        GameObject[] newArray = new GameObject[ump.RenderingObjects.Length+1];
        for (int i = 0; i < ump.RenderingObjects.Length; i++)
            newArray[i] = ump.RenderingObjects[i];
        newArray[newArray.Length - 1] = newObject;
        ump.RenderingObjects = newArray;
    }

    public void OnStateChanged(bool scanning)
    {
    }

    public void OnUpdateError(TargetFinder.UpdateState updateError)
    {
        Debug.Log("Cloud Reco update error " + updateError.ToString());
        if (spearCloudObject != null && updateError.ToString().Contains("SUSPEND"))
        {
           // spearCloudObject.SetActive(true);
           // gameObject.SetActive(false);
            if (!CloudStatusText.text.Contains("SUSPENDED"))
            {
                if (statusCorotine != null)
                    StopCoroutine(statusCorotine);
                statusCorotine = StartCoroutine(showStatus(0, gameObject.name + " is <b><color=red>SUSPENDED!</color></b>\nTrying to reconnect in ", 10, 10));
            }
        }
        else if (updateError.ToString().Contains("SERVICE_NOT_AVAILABLE"))
        {
            if (!CloudStatusText.text.Contains("No Internet"))
            {
                if (statusCorotine != null)
                    StopCoroutine(statusCorotine);
                statusCorotine = StartCoroutine(showStatus(0, "<b><color=red>No Internet Connection!</color></b>\nTrying to reconnect in ", 10, 10));
            }
        }
        else
        {
            if (statusCorotine != null)
                StopCoroutine(statusCorotine);
            statusCorotine = StartCoroutine(showStatus(0, "<b><color=red>ERROR!\n" + updateError.ToString() + "</color></b>", 5));
        }
        CloudStatusText.text = updateError.ToString();
    }

    public void updateAspectRatio(DefaultTrackableEventHandler dteh)
    {
        if (aspectCoroutine != null)
            StopCoroutine(aspectCoroutine);
        Vector2 videoSize = new Vector2(ump.VideoWidth, ump.VideoHeight);
        if (Application.isEditor)
            videoSize = new Vector2(ump.VideoHeight, ump.VideoWidth);
       // dteh.videoCanvas.transform.localScale = new Vector3(1.0f, videoSize.y/ videoSize.x, 1.0f);
        ump.RenderingObjects[1].transform.parent.GetComponent<AspectRatioFitter>().aspectRatio = videoSize.x / videoSize.y;
    }

    IEnumerator showStatus(float waitBefore, string shownText, float showTime)
    {
        yield return new WaitForSeconds(waitBefore);
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / showTime)
        {
            CloudStatusText.text = shownText;
            yield return null;
        }
        CloudStatusText.text = "";
    }

    IEnumerator showStatus(float waitBefore, string shownText, float showTime, float retryConnectionTime)
    {
        yield return new WaitForSeconds(waitBefore);
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / showTime)
        {
            CloudStatusText.text = shownText + (int)(showTime - (showTime * t)) + "...";
            yield return null;
        }
        mCloudRecoBehaviour.RegisterEventHandler(this);
        CloudStatusText.text = "";
    }

    IEnumerator getAspect(DefaultTrackableEventHandler defaultTrackable)
    {
        for (float t = 0.0f; t < 20.0f; t += Time.deltaTime)
        {
            if (ump.IsPlaying)
                t = 20.0f;
            yield return null;
        }
        if (ump.IsPlaying)
            updateAspectRatio(defaultTrackable);
    }

    public void OnInitialized(TargetFinder targetFinder)
    {
        //throw new System.NotImplementedException();
    }
}
