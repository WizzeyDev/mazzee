﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class SceneBuilder : MonoBehaviour, IUserDefinedTargetEventHandler
{
    UserDefinedTargetBuildingBehaviour targetBuilder;
    ObjectTracker ot;
    DataSet ds;
    ImageTargetBuilder.FrameQuality quality;
    public Color[] qualityColors;
    public ImageTargetBehaviour itb;
    public RectTransform qulaityDebug;
    Coroutine animateBar;
    int count = 0;
    void Start()
    {
        Application.targetFrameRate = 60;
        if (GetComponent<UserDefinedTargetBuildingBehaviour>() != null)
        {
            targetBuilder = GetComponent<UserDefinedTargetBuildingBehaviour>();
            targetBuilder.RegisterEventHandler(this);
        }
    }
    public void OnFrameQualityChanged(ImageTargetBuilder.FrameQuality frameQuality)
    {
        quality = frameQuality;
        //DebugText.text = frameQuality.ToString();
        if (frameQuality == ImageTargetBuilder.FrameQuality.FRAME_QUALITY_NONE)
            updateQualityBar(0);
        else if (frameQuality == ImageTargetBuilder.FrameQuality.FRAME_QUALITY_LOW)
            updateQualityBar(1);
        else if (frameQuality == ImageTargetBuilder.FrameQuality.FRAME_QUALITY_MEDIUM)
            updateQualityBar(2);
        else if (frameQuality == ImageTargetBuilder.FrameQuality.FRAME_QUALITY_HIGH)
            updateQualityBar(3);
    }

    public void OnInitialized()
    {
        if (TrackerManager.Instance.GetTracker<ObjectTracker>() != null)
        {
            ot = TrackerManager.Instance.GetTracker<ObjectTracker>();
            ds = ot.CreateDataSet();
            ot.ActivateDataSet(ds);
            targetBuilder.StartScanning();
        }
    }

    public void OnNewTrackableSource(TrackableSource trackableSource)
    {
        count++;
        ot.DeactivateDataSet(ds);
        ds.CreateTrackable(trackableSource, itb.gameObject);
        ot.ActivateDataSet(ds);
    }

    public void BuildTarget()
    {
        targetBuilder.BuildNewTarget("target "+count, itb.GetSize().x);
    }

    void updateQualityBar(int quality)
    {
        try
        {
            switch (quality)
            {
                case 0:
                    for (int i = 0; i < qulaityDebug.transform.childCount; i++)
                        qulaityDebug.GetChild(i).GetComponent<RawImage>().color = qualityColors[0];
                    break;
                case 1:
                    for (int i = 0; i < qulaityDebug.transform.childCount; i++)
                        if (i < 2)
                            qulaityDebug.GetChild(i).GetComponent<RawImage>().color = qualityColors[1];
                        else qulaityDebug.GetChild(i).GetComponent<RawImage>().color = qualityColors[0];
                    break;
                case 2:
                    for (int i = 0; i < qulaityDebug.transform.childCount; i++)
                        if (i < 3)
                            qulaityDebug.GetChild(i).GetComponent<RawImage>().color = qualityColors[2];
                        else qulaityDebug.GetChild(i).GetComponent<RawImage>().color = qualityColors[0];
                    break;
                case 3:
                    for (int i = 0; i < qulaityDebug.transform.childCount; i++)
                        qulaityDebug.GetChild(i).GetComponent<RawImage>().color = qualityColors[3];
                    break;
            }
        }
        catch { }
    }
}
