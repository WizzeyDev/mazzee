﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SpawnFocusPoint : MonoBehaviour {

    public GameObject focusPoint;
    Vector3 position;
    // Update is called once per frame
    public void spawnFocusPoint()
    {
        if (Input.touchCount > 0)
            position = Input.GetTouch(0).position;
        else position = Input.mousePosition;
        GameObject spawn = Instantiate(focusPoint);
        spawn.transform.SetParent(transform.parent);
        spawn.transform.localScale = new Vector3(1, 1, 1);
        spawn.transform.position = position;
    }

    public void flash()
    {
        StartCoroutine(showFlash());
    }

    IEnumerator showFlash()
    {
        for (float i = 0; i < 0.4f; i += Time.deltaTime)
        {
            GetComponent<Image>().color = new Color(1, 1, 1, Mathf.Lerp(0.4f, 0.0f, i / 0.4f));
            yield return null;
        }
    }
}

public class LevelState
{
    public bool targetListActive;
    public bool newTargetMenu;
    public bool imageEditor;
    public string image_path;
}
