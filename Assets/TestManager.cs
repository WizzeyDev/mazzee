﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestManager : MonoBehaviour
{

    public TMPro.TextMeshProUGUI mVerstionTxt;
    public Transform mTargetsContainer;
    public bool isDeb = true;

    private static TestManager instance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        if (isDeb)
        {
            mVerstionTxt.text = "0.09";
        }
        else
        {
            mVerstionTxt.text = "";
            mVerstionTxt.gameObject.SetActive(false);         
        }
      
        //Debug.Log("<color=red> Transformer: </color>" + RequestFlowManager.GetMyTransform.root  );
    
    }

 

    public static bool GetIsDevVerstion { get { return instance.isDeb; } }

    public static Transform TargetsContainer
    {
        get
        {
            if (instance.mTargetsContainer != null)
            {
                return instance.mTargetsContainer;
            }
            else
            {
                Debug.LogError("Cant Find mTargetsContainer"); // TODO: maybe add a way to find the container??
                return null;
            }
        } }

    public void ReSyncTargets()
    {
        //LoginManager.FetchProfilePic(); //For Pic
        foreach (Transform editTran in EditPicture.GetTransform)
        {
            Destroy(editTran.gameObject);
        }
            
        foreach (Transform tran in mTargetsContainer)
        {
            Destroy(tran.gameObject);
        }
        Invoke("ReLoadTargets",0.5f);
    }

    private void ReLoadTargets()
    {
        EditPicture.ReSyncTargets();
    }
}
