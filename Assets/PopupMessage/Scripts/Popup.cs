﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security.Cryptography;
using UnityEngine.UI;
using TMPro;

public class Popup : MonoBehaviour {
    [SerializeField]
    private TMP_Text text;
    [SerializeField]
    private Image dialogIcon;
    [SerializeField]
    private TMP_InputField inputField;
    [SerializeField]
    private Sprite[] icons;
    [SerializeField]
    private Button[] buttons;

    private void Start()
    {
        Message.referance = GetComponent<Popup>();
        GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
        gameObject.SetActive(false);
        //genHash("Arik2272"); c87e6d499e49888d1b70f3b67c47b410
    }

    public void showDialog(string message)
    {
        text.text = message;
        text.rectTransform.anchoredPosition = new Vector2(0.0f, text.rectTransform.anchoredPosition.y);
        dialogIcon.gameObject.SetActive(false);
        text.alignment = TextAlignmentOptions.Center;
        setType(DialogType.Ok);
        buttons[2].onClick.RemoveAllListeners();
        buttons[2].onClick.AddListener(() => closeDialog());
        StartCoroutine(refreshSizeFitter());
    }

    public bool equal(string pass, string requiredhash)
    {
        MD5 md5 = MD5.Create();
        var contentMD5bytes = md5.ComputeHash(System.Text.Encoding.ASCII.GetBytes(pass));
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        for (int i = 0; i < contentMD5bytes.Length; i++)
            sb.Append(contentMD5bytes[i].ToString("x2"));
        Debug.Log("<color=red><b>REQUIRED: " + requiredhash + "</color>, <color=blue>RECEIVED HASH:" + sb.ToString() + "</b></color>");
        return (sb.ToString()==requiredhash);
    }

    public void genHash(string pass)
    {
        MD5 md5 = MD5.Create();
        var contentMD5bytes = md5.ComputeHash(System.Text.Encoding.ASCII.GetBytes(pass));
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        for (int i = 0; i < contentMD5bytes.Length; i++)
            sb.Append(contentMD5bytes[i].ToString("x2"));
        Debug.Log("<color=red><b>PASS: " + pass + ", GENERATED HASH:" + sb.ToString() + "</b></color>");
    }

    public void showDialog(string message, DialogIcon icon)
    {
        text.text = message;
        setIcon(icon);
        setType(DialogType.Ok);
        buttons[2].onClick.RemoveAllListeners();
        buttons[2].onClick.AddListener(() => closeDialog());
        StartCoroutine(refreshSizeFitter());
    }

    public void showDialog(string message, Action OkAction)
    {
        text.text = message;
        text.rectTransform.anchoredPosition = new Vector2(0.0f, text.rectTransform.anchoredPosition.y);
        dialogIcon.gameObject.SetActive(false);
        text.alignment = TextAlignmentOptions.Center;
        setType(DialogType.Ok);
        buttons[2].onClick.RemoveAllListeners();
        if (OkAction != null)
            buttons[2].onClick.AddListener(() => OkAction());
        buttons[2].onClick.AddListener(() => closeDialog());
        StartCoroutine(refreshSizeFitter());
    }

    public void showDialog(string message, DialogIcon icon, Action OkAction)
    {
        text.text = message;
        setIcon(icon);
        setType(DialogType.Ok);
        buttons[2].onClick.RemoveAllListeners();
        if(OkAction != null)
        buttons[2].onClick.AddListener(() => OkAction());
        buttons[2].onClick.AddListener(() => closeDialog());
        StartCoroutine(refreshSizeFitter());
    }

    public void showDialog(string message, DialogIcon icon, DialogType type, Action YesOrOkAction)
    {
        text.text = message;
        setIcon(icon);
        setType(type);
        buttons[0].onClick.RemoveAllListeners();
        if (YesOrOkAction != null)
            buttons[0].onClick.AddListener(() => YesOrOkAction());
        buttons[0].onClick.AddListener(() => closeDialog());
        buttons[1].onClick.RemoveAllListeners();
        buttons[1].onClick.AddListener(() => closeDialog());
        buttons[2].onClick.RemoveAllListeners();
        if (YesOrOkAction != null)
            buttons[2].onClick.AddListener(() => YesOrOkAction());
        buttons[2].onClick.AddListener(() => closeDialog());
        StartCoroutine(refreshSizeFitter());
    }

    public void showDialog(string message, DialogIcon icon, DialogType type, Action YesOrOkAction, bool closeDialogCompletely)
    {
        text.text = message;
        setIcon(icon);
        setType(type);
        buttons[0].onClick.RemoveAllListeners();
        if (YesOrOkAction != null)
            buttons[0].onClick.AddListener(() => YesOrOkAction());
        if (closeDialogCompletely)
            buttons[0].onClick.AddListener(() => closeDialog());
        buttons[1].onClick.RemoveAllListeners();
        buttons[1].onClick.AddListener(() => closeDialog());
        buttons[2].onClick.RemoveAllListeners();
        if (YesOrOkAction != null)
            buttons[2].onClick.AddListener(() => YesOrOkAction());
        if (closeDialogCompletely)
            buttons[2].onClick.AddListener(() => closeDialog());
        StartCoroutine(refreshSizeFitter());
    }

    public void showDialog(string message, DialogIcon icon, DialogType type, Action YesAction, Action NoAction)
    {
        text.text = message;
        setIcon(icon);
        setType(type);
        buttons[0].onClick.RemoveAllListeners();
        if (YesAction != null)
            buttons[0].onClick.AddListener(() => YesAction());
        buttons[0].onClick.AddListener(() => closeDialog());
        buttons[1].onClick.RemoveAllListeners();
        if (NoAction != null)
            buttons[0].onClick.AddListener(() => NoAction());
        buttons[1].onClick.AddListener(() => closeDialog());
        buttons[2].onClick.RemoveAllListeners();
        if (YesAction != null)
            buttons[2].onClick.AddListener(() => YesAction());
        buttons[2].onClick.AddListener(() => closeDialog());
        StartCoroutine(refreshSizeFitter());
    }

    public void showDialog(string message, DialogIcon icon, string HASH, Action SuccessAction)
    {
        text.text = message;
        setIcon(icon);
        setType(DialogType.ConnectPass);
        buttons[0].onClick.RemoveAllListeners();
        if (SuccessAction != null)  
            buttons[0].onClick.AddListener(() => checkPassAndGoOn(HASH, SuccessAction));
        buttons[1].onClick.RemoveAllListeners();
        buttons[1].onClick.AddListener(() => closeDialog());
        buttons[2].onClick.RemoveAllListeners();
        StartCoroutine(refreshSizeFitter());
    }


    public void showDialog(string message, DialogIcon icon, string HASH, Action SuccessAction, Action WrongPassAction)
    {
        text.text = message;
        setIcon(icon);
        setType(DialogType.ConnectPass);
        buttons[0].onClick.RemoveAllListeners();
        if (SuccessAction != null)
            buttons[0].onClick.AddListener(() => checkPassAndGoOn(HASH, SuccessAction, WrongPassAction));

        buttons[1].onClick.RemoveAllListeners();
        buttons[1].onClick.AddListener(() => closeDialog());
        buttons[2].onClick.RemoveAllListeners();
        StartCoroutine(refreshSizeFitter());
    }

    IEnumerator refreshSizeFitter()
    {
        GetComponent<ContentSizeFitter>().enabled = false;
        yield return new WaitForEndOfFrame();
        GetComponent<ContentSizeFitter>().enabled = true;
    }

    private void setIcon(DialogIcon icon)
    {
        text.rectTransform.anchoredPosition = new Vector2(110.0f, text.rectTransform.anchoredPosition.y);
        dialogIcon.gameObject.SetActive(true);
        text.alignment = TextAlignmentOptions.MidlineLeft;
        switch (icon)
        {
            case DialogIcon.Success:
                dialogIcon.overrideSprite = icons[0];
                break;
            case DialogIcon.Error:
                dialogIcon.overrideSprite = icons[1];
                break;
            case DialogIcon.Warning:
                dialogIcon.overrideSprite = icons[2];
                break;
            case DialogIcon.Info:
                dialogIcon.overrideSprite = icons[3];
                break;
            case DialogIcon.Caution:
                dialogIcon.overrideSprite = icons[4];
                break;
        }
    }

    public void checkPassAndGoOn(string requestedHash, Action passWordIsRight)
    {
        if (equal(inputField.text, requestedHash))
            passWordIsRight.Invoke();
        else Message.showDialog("Ops! Worng Password!", DialogIcon.Error);
        inputField.text = "";
    }

    public void checkPassAndGoOn(string requestedHash, Action RightPass, Action WrongPass)
    {
        if (equal(inputField.text, requestedHash))
            RightPass.Invoke();
        else WrongPass();
        inputField.text = "";
    }

    private void setType(DialogType type)
    {
        switch (type)
        {
            case DialogType.Ok:
                buttons[0].gameObject.SetActive(false);
                buttons[1].gameObject.SetActive(false);
                buttons[2].gameObject.SetActive(true);
                buttons[2].transform.GetChild(0).GetComponent<TMP_Text>().text = "Ok";
                inputField.gameObject.SetActive(false);
                break;
            case DialogType.YesNo:
                buttons[0].gameObject.SetActive(true);
                buttons[1].gameObject.SetActive(true);
                buttons[0].transform.GetChild(0).GetComponent<TMP_Text>().text = "Yes";
                buttons[1].transform.GetChild(0).GetComponent<TMP_Text>().text = "No";
                buttons[2].gameObject.SetActive(false);
                inputField.gameObject.SetActive(false);
                break;
            case DialogType.ConnectPass:
                buttons[0].gameObject.SetActive(true);
                buttons[1].gameObject.SetActive(true);
                buttons[0].transform.GetChild(0).GetComponent<TMP_Text>().text = "Connect";
                buttons[1].transform.GetChild(0).GetComponent<TMP_Text>().text = "Cancle";
                buttons[2].gameObject.SetActive(false);
                inputField.gameObject.SetActive(true);
                break;
        }
    }

    public void resetYesButton()
    {
        GetComponent<Button>().onClick.RemoveAllListeners();
        gameObject.SetActive(false);
    }

    private void closeDialog()
    {
        gameObject.SetActive(false);
    }
}
