﻿using System;

public static class Message {

    public static Popup referance;
    
    // Use this for initialization
    public static void showDialog(string message)
    {
        referance.gameObject.SetActive(true);
        referance.showDialog(message);
    }

    public static bool equal(string password, string request_hash)
    {
       return referance.equal(password, request_hash);
    }

    public static void showDialog(string message, DialogIcon icon)
    {
        referance.gameObject.SetActive(true);
        referance.showDialog(message, icon);
    }

    public static void showDialog(string message, DialogIcon icon, Action OkAction)
    {
        referance.gameObject.SetActive(true);
        referance.showDialog(message, icon, OkAction);
    }

    public static void showDialog(string message, DialogIcon icon, DialogType type, Action YesOrOkAction)
    {
        referance.gameObject.SetActive(true);
        referance.showDialog(message, icon, type, YesOrOkAction);
    }

    public static void showDialog(string message, DialogIcon icon, DialogType type, Action YesOrOkAction, bool closeDialog)
    {
        referance.gameObject.SetActive(true);
        referance.showDialog(message, icon, type, YesOrOkAction, closeDialog);
    }

    public static void showDialog(string message, DialogIcon icon, DialogType type, Action YesAction, Action NoAction)
    {
        referance.gameObject.SetActive(true);
        referance.showDialog(message, icon, type, YesAction, NoAction);
    }
    public static void showDialog(string message, DialogIcon icon, string HASH, Action SuccessAction)
    {
        referance.gameObject.SetActive(true);
        referance.showDialog(message, icon, HASH, SuccessAction);
    }

    public static void showPassDialog(string message, string HASH, Action onOkPressed)
    {
        referance.gameObject.SetActive(true);
        referance.showDialog(message, DialogIcon.Info, HASH, onOkPressed);
    }

    public static void showPassDialog(string message, string HASH, Action onPassIsOk, Action onPassIsWrong)
    {
        referance.gameObject.SetActive(true);
        referance.showDialog(message, DialogIcon.Info, HASH, onPassIsOk, onPassIsWrong);
    }

    public static void showDialog(string message, DialogIcon icon, string HASH, Action SuccessAction, Action FailAction)
    {
        referance.gameObject.SetActive(true);
        referance.showDialog(message, icon, HASH, SuccessAction, FailAction);
    }

    public static void hideDialog()
    {
        referance.gameObject.SetActive(false);
    }
}
public enum DialogIcon
{
    Success, Error, Warning, Info, Caution
}

public enum DialogType
{
    Ok, YesNo, ConnectPass
}