﻿using UnityEngine;

public class YouTubeTutorial : MonoBehaviour
{
    public Texture[] mTexture;
    public EditPicture mEditPicture;
    string mYTURL = "https://www.youtube.com/?gl=IL&hl=en";
    int mPhaseNum;
    enum TutorialPhase { first,seconed,third,fourth}
    TutorialPhase mTutorialPhase;
    public void OpenYouTube()
    {
        if (PlayerPrefs.GetInt("YTTutorial", 1) == 1)
        {
            mEditPicture.OpenURL(mYTURL);
        }
        else
        {
            NextTutorialPhase();
        }
    }

    public void NextTutorialPhase()
    {
        if (mPhaseNum < 4)
        {
             //= mTexture[mPhaseNum]

            mPhaseNum++;
        }
        switch (mTutorialPhase)
        {
            case TutorialPhase.first:
                break;
            case TutorialPhase.seconed:
                break;
            case TutorialPhase.third:
                break;
            case TutorialPhase.fourth:
                break;
        }
            
    }

    public void SkipTutorial()
    {

    }
}
