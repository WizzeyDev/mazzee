﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserManager : MonoBehaviour
{
    public static string user_id = "";
    // Start is called before the first frame update
    void Start()
    {
        //string randomId = "user_"+Random.Range(000000000, 999999999);// Fore Tests - Full Offline Anonim user.
        user_id = PlayerPrefs.GetString("USER_ID", "");
        //PlayerPrefs.GetString("USER_ID", "");
        //PlayerPrefs.SetString("USER_ID", user_id);
    }

    public static void ClearUserSettings()
    {
        user_id = "";
        PlayerPrefs.SetString("USER_ID", user_id);
    }

    public static void UpdateId(string newID)
    {
        user_id = newID;
        PlayerPrefs.SetString("USER_ID", newID);
    }
}
