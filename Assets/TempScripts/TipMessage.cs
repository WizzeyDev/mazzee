﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TipMessage : MonoBehaviour
{
    CanvasRenderer[] cr;
    CanvasRenderer me;
    Coroutine cor;
    [SerializeField]
    private TMP_Text tipText;
    private static TipMessage tip;
    public float fadeSpeed = 1.6f;
    // Start is called before the first frame update
    void Start()
    {
        me = GetComponent<CanvasRenderer>();
        cr = GetComponentsInChildren<CanvasRenderer>();
        tip = GetComponent<TipMessage>();
        showForeconds(7.0f);
    }

    public void show()
    {
        if (cor != null)
            StopCoroutine(cor);
        cor = StartCoroutine(FadeObject(false));
    }
    public void hide()
    {
        if (cor != null)
            StopCoroutine(cor);
        cor = StartCoroutine(FadeObject(true));
    }

    public void showForeconds(float seconds)
    {
        StartCoroutine(showdelay(seconds));
    }

    public void showForeconds(float seconds, string tip_text)
    {
        tipText.text = tip_text;
        StartCoroutine(showdelay(seconds));
    }

    public static void showTip(string tip_text, float showTime)
    {
        if (tip != null)
            tip.showForeconds(showTime, tip_text);
        else Debug.LogWarning("current scene has not TipMessage in the scene!");
    }

    IEnumerator showdelay(float seconds)
    {
        show();
        yield return new WaitForSeconds(seconds);
        hide();
    }

    IEnumerator FadeObject(bool hide)
    {
        float a = me.GetAlpha();
        for (float i = 0.0f; i <= 1.0f; i += Time.deltaTime * fadeSpeed)
        {
            if (hide)
                me.SetAlpha(Mathf.Lerp(a, 0.0f, i));
            else me.SetAlpha(Mathf.Lerp(a, 1.0f, i));
            foreach (CanvasRenderer c in cr)
            {
                if (hide)
                    c.SetAlpha(Mathf.Lerp(a, 0.0f, i));
                else c.SetAlpha(Mathf.Lerp(a, 1.0f, i));
            }
            yield return null;
        }
    }
}
