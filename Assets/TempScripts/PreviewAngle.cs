﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreviewAngle : MonoBehaviour
{
    public float current_angle = 0.0f;
    [SerializeField]
    private GameObject button;
    Coroutine rotation;
    // Start is called before the first frame update
    private void Start()
    {
        current_angle = transform.rotation.eulerAngles.z;
    }

    public void Rotate()
    {
        if (rotation != null)
            StopCoroutine(rotation);
        rotation = StartCoroutine(rotateRight());
    }

    public void setRotation(float angle)
    {
        current_angle = angle;
        Quaternion newAngle = new Quaternion();
        newAngle = Quaternion.Euler(0, 0, current_angle);
        Quaternion newAngleB = new Quaternion();
        newAngleB = Quaternion.Euler(0, 0, 0.0f);
    }

    IEnumerator rotateRight()
    {
        current_angle = (current_angle-90.0f)%360.0f;
        Quaternion newAngle = new Quaternion();
        newAngle = Quaternion.Euler(0, 0, current_angle);
        Quaternion newAngleB = new Quaternion();
        newAngleB = Quaternion.Euler(0, 0, 0.0f);
        for (float i = 0.0f; i < 1.0f; i += Time.deltaTime)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, newAngle, i);
            button.transform.rotation = Quaternion.Lerp(button.transform.rotation, newAngleB, i);
            yield return null;
        }
    }
}
