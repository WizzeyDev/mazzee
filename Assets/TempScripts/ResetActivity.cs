﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetActivity : MonoBehaviour
{
    public GameObject slaveObject;
    // Start is called before the first frame update
    void Start()
    {
        if (slaveObject != null)
            StartCoroutine(reset());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator reset()
    {
        yield return new WaitForEndOfFrame();
        slaveObject.SetActive(false);
        yield return new WaitForEndOfFrame();
        slaveObject.SetActive(true);
    }
}