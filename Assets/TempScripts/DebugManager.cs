﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugManager : MonoBehaviour
{
    [SerializeField]
    private Text onScreenDebug;
    public static string text = "";
    
    // Update is called once per frame
    void Update()
    {
        if (text != onScreenDebug.text)
            onScreenDebug.text = text;
        else return;
    }
}
