﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OrientationManager : MonoBehaviour
{
    public UnityEvent onLandscapeSet;
    public UnityEvent onChanged;
    public UnityEvent onPortaraitSet;
    // Update is called once per frame
    void Update()
    {
        if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft)
        {
            onChanged.Invoke();
            onLandscapeSet.Invoke();
        }
        else if (Input.deviceOrientation == DeviceOrientation.Portrait)
        {
            onChanged.Invoke();
            onPortaraitSet.Invoke();
        }
    }
}
