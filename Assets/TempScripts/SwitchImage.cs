﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SwitchImage : MonoBehaviour
{
    [SerializeField]
    private StickyScrollView PagesContainer;
    [SerializeField]
    private GameObject pointTemplate;
    [SerializeField]
    private Sprite Unselected;
    [SerializeField]
    private Sprite Selected;
    GameObject clonePoint;
    GameObject[] points;
    int currentPage = 0;
    // Start is called before the first frame update
    void Start()
    {
        points = new GameObject[PagesContainer.GetComponent<ScrollRect>().content.childCount];
        for (int i = 0; i < points.Length; i++)
        {
            points[i] = Instantiate(pointTemplate);
            points[i].transform.SetParent(pointTemplate.transform.parent);
            points[i].transform.localScale = new Vector3(1, 1, 1);
            points[i].GetComponent<Image>().sprite = Unselected;
        }
        clonePoint = Instantiate(pointTemplate);
        clonePoint.transform.SetParent(pointTemplate.transform.parent.parent);
        clonePoint.transform.localScale = new Vector3(1, 1, 1);
        clonePoint.GetComponent<Image>().sprite = Selected;
        clonePoint.GetComponent<Image>().rectTransform.sizeDelta = pointTemplate.GetComponent<Image>().rectTransform.sizeDelta;
        pointTemplate.SetActive(false);
        clonePoint.transform.position = points[0].transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        currentPage = PagesContainer.currentStickOn;
        clonePoint.transform.position = Vector2.Lerp(clonePoint.transform.position, points[currentPage%points.Length].transform.position, 0.5f);
    }
}
