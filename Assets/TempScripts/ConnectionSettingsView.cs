﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.IO;

public class ConnectionSettingsView : MonoBehaviour
{
    [SerializeField]
    private InputField ServerName;
    public string server_access_hashmd5;
    [SerializeField]
    private InputField dropbox_token;
    [SerializeField]
    private InputField client_access;
    [SerializeField]
    private InputField client_secret;
    [SerializeField]
    private InputField server_access;
    [SerializeField]
    private InputField server_secret;
    [SerializeField]
    private UploadManager UP;
    [SerializeField]
    private GameObject UploadView;

    [SerializeField]
    private RawImage AccessKeyImage;
    string fuckingSlash = "/";//@"\"

    Texture2D downloadedTex;
    ServerData data;
    // Start is called before the first frame update

    private void Start()
    {
        AccessKeyImage.transform.GetChild(0).gameObject.SetActive(false);
    }

    private Sprite toSprite(Texture2D tex)
    {
        return Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
    }

    public void setPassword(string newHash)
    {
        server_access_hashmd5 = newHash;
    }

    private IEnumerator LoadImage(string path)
    {

        var url = "file://" + path;
        var www = new WWW(url);
        yield return www;
        var texture = www.texture;
        if (texture == null)
        {
            Debug.LogError("Failed to load texture url:" + url);
        }
        else
        {
            downloadedTex = texture;
            AccessKeyImage.GetComponent<AspectRatioFitter>().aspectRatio = ((float)texture.width / (float)texture.height);
            AccessKeyImage.texture = texture;
        }
    }

    public void PickPicture()
    {
        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
        {
            Debug.Log("Video path: " + path);
            if (path != null)
            {
                StartCoroutine(LoadImage(path));
            }
        }, "Select a video");
        Debug.Log("Permission result: " + permission);
    }

    public IEnumerator DownloadFromDropbox(string id)
    {
        AccessKeyImage.transform.GetChild(0).gameObject.SetActive(true);
        string requestPath = "/download";
        string serviceURI = @"https://content.dropboxapi.com/2/files" + requestPath;//postRequest.setURI(new URI(url + "/targets"));
        string contentType = "application/octet-stream";
        DropboxPath dropData = new DropboxPath();
        dropData.path = "/" + id + ".jpg";
        var request = new UnityWebRequest(serviceURI, "POST");
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Authorization", string.Format("Bearer {0}", "92jHmcyE6aAAAAAAAAACZNX41T8r3aQGz7VNIq2jzezBD3fUMyuVNIp2wdXEA7EX"));
        request.SetRequestHeader("Content-Type", contentType);
        request.SetRequestHeader("Dropbox-API-Arg", JsonUtility.ToJson(dropData));
        yield return request.Send().progress;
        while (!request.isDone)
        {
            yield return null;
        }
        switch (request.responseCode)
        {
            case 200:
                Debug.Log("KEY IMAGE DOWNLOAD STARTED");
                string path = Application.persistentDataPath + fuckingSlash + id + ".jpg";
                File.WriteAllBytes(path, request.downloadHandler.data);
                yield return null;
                StartCoroutine(LoadImage(path));
                break;
            default:
                Debug.Log("PATH: "+ dropData.path + ", DOWNLOAD ERROR: " + request.downloadHandler.text);
                break;
        }
        AccessKeyImage.transform.GetChild(0).gameObject.SetActive(false);
    }
}
