﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateScale : MonoBehaviour
{
    public Vector3 MinScale = new Vector3(0.95f, 0.95f, 0.95f);
    public Vector3 MaxScale = new Vector3(1.05f, 1.05f, 1.05f);
    public float AnimationSpeed = 1.0f;
    // Update is called once per frame
    void Update()
    {
        transform.localScale = MinScale + (MaxScale - MinScale) * Mathf.Abs(Mathf.Sin((Time.time * AnimationSpeed)%Mathf.PI));
    }
}
