﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(ScrollRect))]
public class StickyScrollView : MonoBehaviour
{
    private float pos;
    float inertionDelay = 0.0f;
    public int currentStickOn = 0;
    RectTransform Content;
    bool forcedStickInProces = false;
    [SerializeField]
    private Button Skip;
    // Start is called before the first frame update
    void Start()
    {
        Content = GetComponent<ScrollRect>().content;
    }

    void getStickyOn()
    {
        pos = 999999.0f;
        for (int i = 0; i < Content.childCount; i++)
            if (Vector3.Distance(Content.GetChild(i).position, Vector3.zero) < pos)
            {
                pos = Vector3.Distance(Content.GetChild(i).position, Vector3.zero);
                currentStickOn = i;
            }
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Linear Velocity: " + Vector2.Distance(GetComponent<ScrollRect>().velocity, Vector2.zero));
        if (Input.touchCount == 0 && !Input.GetMouseButton(0))
        {
            if(!forcedStickInProces)
            getStickyOn();
            if(Vector2.Distance(GetComponent<ScrollRect>().velocity, Vector2.zero) < 1800.0f)
            {
                GetComponent<ScrollRect>().velocity = new Vector2(0, 0);
                Content.anchoredPosition = Vector3.Lerp(Content.anchoredPosition, new Vector2(-Content.GetChild(currentStickOn).GetComponent<RectTransform>().anchoredPosition.x, 0), 0.1f);
            }
        }
        else inertionDelay = 1.0f;
    }

    public void NextPage()
    {
        if (currentStickOn + 1 <= Content.childCount - 1)
            StartCoroutine(setPage(currentStickOn + 1));
        else Skip.onClick.Invoke();
    }

    IEnumerator setPage(int page)
    {
        forcedStickInProces = true;
        currentStickOn = page % Content.childCount;
        yield return new WaitForSeconds(0.3f);
        forcedStickInProces = false;
    }
}
