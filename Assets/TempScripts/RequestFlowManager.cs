﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RequestFlowManager : MonoBehaviour
{
    public static bool sendRequestAllowed = true;
    private static int activeRequests = 0;
    public static int maxRequestsCount = 3;

    public Transform mTran;
    public static RequestFlowManager instance;

    private void Awake()
    {
        instance = this;
        mTran = transform;
    }
   

    public static bool SendAllowed()
    {
        if (activeRequests < maxRequestsCount)
        {
            activeRequests++;
            Debug.Log("Active Flows: " + activeRequests);
            return true;
        }
        else return false;
    }
    public static void CloseFlow()
    {
        activeRequests--;
        Debug.Log("Active Flows: " + activeRequests);
    }
}
