﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyManager : MonoBehaviour
{
    public static string server_name = "Default Server";
    //HijZltjcDaAAAAAAAAAAQie6rsuHJpmzk-QFlJzVBrsrDQalQYys3MLvjij6G0dA - Arie Hashcode
    //92jHmcyE6aAAAAAAAAACZNX41T8r3aQGz7VNIq2jzezBD3fUMyuVNIp2wdXEA7EX - Theo Hashcode
    public static string cloud_hash = "92jHmcyE6aAAAAAAAAACZNX41T8r3aQGz7VNIq2jzezBD3fUMyuVNIp2wdXEA7EX";
    public static string pass_hash = "";
    public static string key1 = "958085c3444c61585acf73ee5e5f3c2f43b84155";
    public static string key2 = "c628dbdc6011f4cca22a9b68230c65f1c1b51abe";
    public static string key3 = "457fbde493da747fd83efaaa722cf240dd2d70c4";
    public static string key4 = "744a954f9b98d8d61031d4f04aa37b2b76e1fe5a";
    public static string key5 = "7746bf4466334f258daee668116300ca";
    [SerializeField]
    private GameObject ServersContainer;

    // Start is called before the first frame update
    public void Start()
    {
        load();
    }

    public static void load()
    {
        ServerData s_default = new ServerData();
        s_default.server_name = server_name;
        s_default.cloud_hash = cloud_hash;
        s_default.pass_hash = pass_hash;
        s_default.key1 = key1;
        s_default.key2 = key2;
        s_default.key3 = key3;
        s_default.key4 = key4;
        s_default.key5 = key5;
        ServerData l = new ServerData();
        l = JsonUtility.FromJson<ServerData>(PlayerPrefs.GetString("SERVER_KEY", JsonUtility.ToJson(s_default)));
        server_name = l.server_name;
        cloud_hash = l.cloud_hash;
        pass_hash = l.pass_hash;
        key1 = l.key1;
        key2 = l.key2;
        key3 = l.key3;
        key4 = l.key4;
        key5 = l.key5;
    }

    public static void setDefault()
    {
        ServerData s_default = new ServerData();
        s_default.server_name = server_name;
        s_default.cloud_hash = cloud_hash;
        s_default.pass_hash = pass_hash;
        s_default.key1 = key1;
        s_default.key2 = key2;
        s_default.key3 = key3;
        s_default.key4 = key4;
        s_default.key5 = key5;
        PlayerPrefs.SetString("SERVER_KEY", JsonUtility.ToJson(s_default));
    }
}
