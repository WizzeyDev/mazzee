﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowServerSettings : MonoBehaviour
{
    public GameObject targetActivity;
    public void activateForPassword()
    {
        Message.showDialog("Enter administrator password for settings screen:", DialogIcon.Info, "c87e6d499e49888d1b70f3b67c47b410", () =>
        {
            targetActivity.SetActive(true);
            Message.hideDialog();
        });
    }
}
