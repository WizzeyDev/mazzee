﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonEffect : MonoBehaviour
{
    [SerializeField]
    private RawImage ScanLine;
    [SerializeField]
    private RawImage flash;
    [SerializeField]
    private GameObject text;
    public float TopScale = 1.06f;
    public float FlashBrightness = 0.4f;
    int animRot = 0;

    // Start is called before the first frame update
    void Start()
    {
       // StartCoroutine(playAnimation());
    }

    private void OnEnable()
    {
        StartCoroutine(playAnimation());
    }

    IEnumerator playAnimation()
    {
        int breathtimes = Random.Range(1, 3);
        for (int b = 0; b < breathtimes; b++)
        {
            for (float i = 0; i < 1.0f; i += Time.deltaTime)
            {
                text.transform.localScale = Vector3.Lerp(new Vector3(1, 1, 1), new Vector3(TopScale, TopScale, TopScale), i);
                yield return null;
            }
            for (float i = 0; i < 1.0f; i += Time.deltaTime)
            {
                text.transform.localScale = Vector3.Lerp(new Vector3(TopScale, TopScale, TopScale), new Vector3(1, 1, 1), i);
                yield return null;
            }
        }
        for (float i = 0; i < 1.0f; i += Time.deltaTime*3)
        {
            text.transform.localScale = Vector3.Lerp(new Vector3(1, 1, 1), new Vector3(TopScale, TopScale, TopScale), i);
            yield return null;
        }
        animRot = Random.Range(0, 4);
        for (float i = 0; i < 1.0f; i += Time.deltaTime)
        {
            flash.color = Color.Lerp(new Color(1, 1, 1, FlashBrightness), new Color(1, 1, 1, 0.02f), i);
            text.transform.localScale = Vector3.Lerp(new Vector3(TopScale, TopScale, TopScale), new Vector3(1, 1, 1), i);
            switch(animRot)
            {
                case 0:
                    ScanLine.transform.rotation = Quaternion.Euler(0, 0, 180);
                ScanLine.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(new Vector2(0, 360.0f), new Vector2(0, -360.0f), i);
                    break;
                case 1:
                    ScanLine.transform.rotation = Quaternion.Euler(0, 0, 0);
                    ScanLine.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(new Vector2(0, -360.0f), new Vector2(0, 360.0f), i);
                    break;
                case 2:
                    ScanLine.transform.rotation = Quaternion.Euler(0, 0, 90);
                    ScanLine.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(new Vector2(360.0f, 0.0f), new Vector2(-360.0f, 0.0f), i);
                    break;
                case 3:
                    ScanLine.transform.rotation = Quaternion.Euler(0, 0, 270);
                    ScanLine.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(new Vector2(-360.0f, 0.0f), new Vector2(360.0f, 0.0f), i);
                    break;
                default:
                    ScanLine.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(new Vector2(0, 360.0f), new Vector2(0, -360.0f), i);
                    break;
            }
            yield return null;
        }
        StartCoroutine(playAnimation());
    }
}
