﻿using UnityEngine;

public class Delta : MonoBehaviour
{
    Vector2 currentPosition = Vector2.zero;
    public static Vector2 deltaPositon = Vector2.zero;
    Vector2 lastPositon = Vector2.zero;
    public static Vector2 positionDelta = Vector2.zero;
    public static Vector2 verticalDelta = Vector2.zero;
    public static Vector2 horizontalDelta = Vector2.zero;

    // Update is called once per frame
    void LateUpdate()
    {
        currentPosition = Input.mousePosition;
        deltaPositon = currentPosition - lastPositon;
        lastPositon = currentPosition;
        if (Input.touchCount > 0)
            positionDelta = Input.GetTouch(0).deltaPosition;
        else positionDelta = deltaPositon;
        verticalDelta = new Vector2(0.0f, positionDelta.y);
        horizontalDelta = new Vector2(positionDelta.x, 0.0f);
    }
}
