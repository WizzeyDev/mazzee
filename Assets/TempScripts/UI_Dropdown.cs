﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(VerticalLayoutGroup))]
public class UI_Dropdown : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject rotateButton;
    public bool DisableOnHide = false;
    CanvasRenderer[] cr;
    VerticalLayoutGroup v;
    Coroutine anim;
    bool tvisible = false;
    public float AnimationSpeed = 1.0f;
    public Vector2 MaximumMinimum = new Vector2(2.0f, -100.0f); 
    void Start()
    {
        cr = GetComponentsInChildren<CanvasRenderer>();
        v = GetComponent<VerticalLayoutGroup>();
        setVisiblityState(false);
    }

    public void SwitchVisible()
    {
        SetVisible(!tvisible);
    }

    public void SetVisible(bool visible)
    {
        tvisible = visible;
        if (anim != null)
            StopCoroutine(anim);
        anim = StartCoroutine(fadeVisibility(visible));
    }


    public void setVisiblityState(bool visible)
    {
        tvisible = visible;
        if (visible)
        {
            if (rotateButton != null)
                rotateButton.transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(rotateButton.transform.rotation.eulerAngles.z, 0, 1));
            v.spacing = Mathf.Lerp(v.spacing, MaximumMinimum.x, 1);
            foreach (CanvasRenderer c in cr)
            {
                c.SetAlpha(1.0f - v.spacing / MaximumMinimum.y);
                c.gameObject.SetActive(true);
            }
        }
        else
        {
            if(rotateButton != null)
                rotateButton.transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(rotateButton.transform.rotation.eulerAngles.z, 45.0f, 1));
            v.spacing = Mathf.Lerp(v.spacing, MaximumMinimum.y, 1);
            foreach (CanvasRenderer c in cr)
            {
                c.SetAlpha(1.0f - v.spacing / MaximumMinimum.y);
                c.gameObject.SetActive(false);
            }
        }
    }

    IEnumerator fadeVisibility(bool visible)
    {
        if (visible)
        {
            foreach (CanvasRenderer c in cr)
                c.gameObject.SetActive(true);
        }
        for (float i = 0; i < 1.0f; i += Time.deltaTime * AnimationSpeed)
        {
            if (visible)
            {
                if (rotateButton != null)
                    rotateButton.transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(rotateButton.transform.rotation.eulerAngles.z, 0, i));
                v.spacing = Mathf.Lerp(v.spacing, MaximumMinimum.x, i);
                foreach (CanvasRenderer c in cr)
                    c.SetAlpha(1.0f - v.spacing / MaximumMinimum.y);
            }
            else
            {
                if (rotateButton != null)
                    rotateButton.transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(rotateButton.transform.rotation.eulerAngles.z, 45.0f, i));
                v.spacing = Mathf.Lerp(v.spacing, MaximumMinimum.y, i);
                foreach (CanvasRenderer c in cr)
                    c.SetAlpha(1.0f - v.spacing / MaximumMinimum.y);
            }

            yield return null;
        }
        if (!visible)
        {
            foreach (CanvasRenderer c in cr)
                c.gameObject.SetActive(false);
            if (DisableOnHide)
                gameObject.SetActive(false);
        }
    }


}
