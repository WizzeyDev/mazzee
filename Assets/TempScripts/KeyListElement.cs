﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Networking;
using System.Security.Cryptography;
using System.IO;

public class KeyListElement : MonoBehaviour
{
    // Start is called before the first frame update
    //HijZltjcDaAAAAAAAAAAQie6rsuHJpmzk-QFlJzVBrsrDQalQYys3MLvjij6G0dA - Arie Hashcode
    //92jHmcyE6aAAAAAAAAACZNX41T8r3aQGz7VNIq2jzezBD3fUMyuVNIp2wdXEA7EX - Theo Hashcode
    public string server_name = "Default Server";
    public string cloud_hash = "92jHmcyE6aAAAAAAAAACZNX41T8r3aQGz7VNIq2jzezBD3fUMyuVNIp2wdXEA7EX";
    public string pass_hash = "";
    public string key1 = "958085c3444c61585acf73ee5e5f3c2f43b84155";
    public string key2 = "c628dbdc6011f4cca22a9b68230c65f1c1b51abe";
    public string key3 = "457fbde493da747fd83efaaa722cf240dd2d70c4";
    public string key4 = "744a954f9b98d8d61031d4f04aa37b2b76e1fe5a";
    public string key5 = "";
    [SerializeField]
    TMP_Text server_title;
    [SerializeField]
    Button connect;
    TMP_Text buttonText;
    Coroutine connecting;
    private bool isConnecting = false;
    void Start()
    {
        buttonText = connect.transform.GetChild(0).GetComponent<TMP_Text>();
        KeyManager.load();
        if (key1 == KeyManager.key1 && server_name == KeyManager.server_name)
        {
            connect.interactable = false;
            buttonText.text = "Connected";
        }
        else
        {
            connect.interactable = true;
            buttonText.text = "Connect";
        }

    }

    IEnumerator ShowConnect()
    {
        isConnecting = true;
        buttonText.text = "Connecting";
        bool pass = false;
        if (!string.IsNullOrEmpty(pass_hash))
            Message.showPassDialog("Enter the password for " + server_name, pass_hash, () => {pass = true; Message.hideDialog(); }, () => 
            {
                isConnecting = false;
                buttonText.text = "Connect";
                Message.showDialog("Wrong password entered!", DialogIcon.Error);
            });
        else pass = true;
        yield return new WaitForSeconds(0.5f);
        while (!pass)
        {
            if (!isConnecting)
                yield break;
            yield return null;
        }
        yield return null;
        for (int i = 0; i < 3; i++)
        {
            buttonText.text += ".";
            yield return new WaitForSeconds(0.5f);
        }
        ServerData l = new ServerData();
        l.server_name = server_name;
        l.cloud_hash = cloud_hash;
        l.pass_hash = pass_hash;
        l.key1 = key1;
        l.key2 = key2;
        l.key3 = key3;
        l.key4 = key4;
        l.key5 = key5;
        PlayerPrefs.SetString("SERVER_KEY", JsonUtility.ToJson(l));
        Application.LoadLevel(0);
        isConnecting = false;
    }

    public void onConnectPressed()
    {
        if (isConnecting)
        {
            StopCoroutine(connecting);
            connect.interactable = true;
            buttonText.text = "Connect";
            isConnecting = false;
        }
        else connecting = StartCoroutine(ShowConnect());
    }

    public void startSecretSync()
    {
        StartCoroutine(selfSyncVuforia());
    }

    IEnumerator selfSyncVuforia()
    {
        ServerData l = new ServerData();
        l.server_name = server_name;
        l.cloud_hash = cloud_hash;
        l.pass_hash = pass_hash;
        l.key1 = key1;
        l.key2 = key2;
        l.key3 = key3;
        l.key4 = key4;
        l.key5 = key5;
        UnityWebRequest request = UploadManager.ServerKey_Update(key5, l);
        yield return new WaitForEndOfFrame();
        yield return request.Send().progress;
        while (!request.isDone)
            yield return null;
        switch (request.responseCode)
        {
            case 200:
                Debug.Log("Vuforia key: " + server_name + ", was updated successfully!");
                break;
            default:
                // No Internet Connection!
                Debug.Log(request.responseCode +": Vuforia key: " + server_name + ", update failed!");
                break;
        }
    }
}
