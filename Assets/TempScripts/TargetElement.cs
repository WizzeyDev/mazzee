﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class TargetElement : MonoBehaviour
{
    public float progress = 0.0f;
    public string log = "";
    public Sprite[] sprites;

    private Transform mTargetsContainer;
    [SerializeField]
    private RawImage TargetPicture; // The preview of an image.
    [SerializeField]
    private RawImage Shadow; // A dark layer over image, to see the LoadingBar
    [SerializeField]
    private GameObject DeleteButton; // Propably should be seen after long press.
    [SerializeField]
    private Image LoadingBar; // Loading circle bar , shows when ever the target is loading, or processing.
    [SerializeField]
    private EditPicture editorPrefab;
    [SerializeField]
    private GameObject syncIcon;
    public GameObject UIInstance;
    private float LastUpdated, LastModified;

    bool isPressed = false;
    bool isSyncRequested = false;
    string slash = @"/";

    public VuforiaTargetMetadata vuforiaMetadata;
    public LocalTargetElement targetElement;
    public int TargetNumber = 0;

 
    #region Status Dinamic Data
    private bool Loading_LocalData = false;
    private bool Posting_Target = false;
    private bool Loading_Missing_Data = false;
    private bool Updating_Target = false;
    private bool Uploading_File = false;
    private bool Deleting_Target = false;
    private bool Processing_Checking = false;
    private bool shake = false;
    private int Retrieve_Data_Active_Flows = 0;
    private bool Retrieve_Data_Search_Active = false;
    #endregion

    private void Awake()
    {
        mTargetsContainer = TestManager.TargetsContainer;
    }

    void Start()
    {
        #if UNITY_ANDROID
        slash = "/";
#elif UNITY_IOS
        slash = "/";
#endif
        UIInstance = transform.GetChild(0).gameObject;
        UIInstance.transform.SetParent(mTargetsContainer);
        UIInstance.transform.localScale = new Vector3(1, 1, 1);
        DeleteButton.SetActive(false);
        LoadingBar.fillAmount = 0.0f;
        StartCoroutine(LoadTarget());
    }

    private void Update()
    {
        if (isSyncRequested)
            return;
        else
        {
            StartLoad(TargetNumber);
            isSyncRequested = true;
        }
    }

    public bool RetrieveAllowed()
    {
        if (Retrieve_Data_Active_Flows < 3)
        {
            Retrieve_Data_Active_Flows++;
            return true;
        }
        else return false;
    }

    public void RetrieveEnd()
    {
        Retrieve_Data_Active_Flows--;
    }


    IEnumerator LoadTarget()
    {
        mTargetsContainer.GetComponent<ContentSizeFitter>().verticalFit = ContentSizeFitter.FitMode.MinSize;
        yield return new WaitForEndOfFrame();
        mTargetsContainer.GetComponent<ContentSizeFitter>().verticalFit = ContentSizeFitter.FitMode.PreferredSize;
    }

    public void StartSync(int targetNum)
    {
        Debug.Log("<color=red> 2: </color>");
        TargetNumber = targetNum;
        targetElement = JsonUtility.FromJson<LocalTargetElement>(PlayerPrefs.GetString(KeyManager.key1+"_TARGET_NUM_" + TargetNumber));
        gameObject.name = "TargetElement_" + TargetNumber;
        if (!targetElement.delete_requested)
            StartCoroutine(SyncTarget());
        else DeleteTarget();
    }

    public void StartSync(LocalTargetElement targetEl, int targetNum)
    {
        Debug.Log("<color=red> 3: </color>");
        TargetNumber = targetNum;
        targetElement = targetEl;
        StartCoroutine(SyncTarget());
    }

    public void EditElement()
    {
    
        EditPicture.LoadImageFromList(targetElement);
    }

    public void UpdateSync(LocalTargetElement targetEl)
    {

        targetElement.UpdatePrefix = "M";
        if (targetElement.TargetPicture_Size != targetEl.TargetPicture_Size || targetElement.Force_TargetPicture_Update)
        {
            targetElement.Local_TargetPicture_Path = targetEl.Local_TargetPicture_Path;
            targetElement.TargetPicture_Uploaded = false;
            targetElement.UpdatePrefix += "I";
        }
        targetElement.UpdateRequested = true;
        targetElement.First_Processing_Done = false;
        UpdateElement();
        Debug.Log("<color=red> 4: </color>");
        PlayerPrefs.SetString(KeyManager.key1 + "_UPDATE_NUM_" + TargetNumber, targetElement.UpdatePrefix);
        StartCoroutine(SyncTarget());
    }

    public void StartLoad(int targetNum)
    {
        Debug.Log("<color=red> 1: </color>");
        TargetNumber = targetNum;
        if (!isSyncRequested)
        {
            targetElement = new LocalTargetElement();//PlayerPrefs.SetString("TARGET_NUM_" + TargetNumber, JsonUtility.ToJson(targetElement))
            targetElement = JsonUtility.FromJson<LocalTargetElement>(PlayerPrefs.GetString(KeyManager.key1 + "_TARGET_NUM_" + TargetNumber));
            if (targetElement != null && !targetElement.delete_requested)
                StartCoroutine(SyncTarget());
            else DeleteTarget();
        }
    }

    IEnumerator BlackOut(bool visible)
    {
        float a = Shadow.color.a;
        LoadingBar.gameObject.SetActive(visible);
        for (float i = 0.0f; i < 1.0f; i += Time.deltaTime)
        {
            if (visible)
                Shadow.color = new Color(0,0,0, Mathf.Lerp(a, 0.3098039f, i));
            else Shadow.color = new Color(0, 0, 0, Mathf.Lerp(a, 0.0f, i));
            yield return null;
        }

    }


    //Dima: this funcstion called on 4 times one of them is to load the targets and its images from drop box??
    IEnumerator SyncTarget()
    {
        //
        // FIRST STEP: LOADING LOCAL DATA
        //
        isSyncRequested = true; // This line is needed to know if the coroutine of sync has ever started, if not, update function will start it as the script goes active. 
        StartCoroutine(BlackOut(true));
        if (!RequestFlowManager.SendAllowed())
        {
            gameObject.name = "TargetElement_" + TargetNumber + " - Waiting...";
            
            //while (!RequestFlowManager.SendAllowed())
                yield return null;
        }
        gameObject.name = "TargetElement_" + TargetNumber + " - Syncing...";
        Debug.LogWarning("Sync of target_" + TargetNumber + " has started!");
        var url = "file://" + targetElement.Local_TargetPicture_Path;
        var www = new WWW(url);
        yield return www;
        var texture = www.texture;
        if (texture == null) // Loading Preview data from localpath
        {
            Debug.LogError("Target Failed to load preview from: " + targetElement.Local_TargetPicture_Path);
            UpdateStatus(TargetStatus.LocalData_Load_Failed);
            yield break;
        }
        else
        {
            Debug.Log("target_" + TargetNumber + " preview loaded!");
            TargetPicture.texture = texture;
            TargetPicture.GetComponent<AspectRatioFitter>().aspectRatio = ((float)texture.width / (float)texture.height);
            UpdateStatus(TargetStatus.LocalData_Loaded);
        }
        if (isFaulted())
        {
            Debug.LogError("Error! Loading of lcaol data leaded to failure.");
            syncIcon.GetComponent<Image>().sprite = sprites[1];
            syncIcon.SetActive(true);
            RequestFlowManager.CloseFlow();
            yield break;
        }
        progress = 1.0f / 6.0f;
        UpdateElement();
        // SECOND STEP: POSTING THE TARGET IF HAS NO ID
        //
        Debug.LogWarning("target_" + TargetNumber + " No ID?: " + string.IsNullOrEmpty(targetElement.Unique_ID));
        Debug.LogWarning("Has prefix?:" + ", "+ targetElement.UpdatePrefix + "ID : " + targetElement.Unique_ID);
        if (string.IsNullOrEmpty(targetElement.Unique_ID)) // Posting as New Target if has no ID
        {
            StartCoroutine(PostTarget());
            while (Posting_Target || Retrieve_Data_Search_Active)
                yield return null;
        }
        else if (targetElement.UpdateRequested)
        {
            StartCoroutine(UpdateTarget());
            while (Updating_Target)
                yield return null;
        }
        if (isFaulted())
        {
            Debug.LogError("Error! at Creation or Update of the target.");
            syncIcon.GetComponent<Image>().sprite = sprites[1];
            syncIcon.SetActive(true);
            RequestFlowManager.CloseFlow();
            yield break;
        }
        progress = (1.0f / 6.0f)*2;
        //
        // FOURTH STEP: UPLOADING TARGET IMAGE FOR FUTURE SYNC FROM DROPBOX
        //
        Debug.Log("target_" + TargetNumber + " Target Picture Uploaded?: "+ targetElement.TargetPicture_Uploaded + ", Local Target Data Exists: " + !string.IsNullOrEmpty(targetElement.Local_TargetPicture_Path));
        if (!targetElement.TargetPicture_Uploaded && !string.IsNullOrEmpty(targetElement.Local_TargetPicture_Path))
        {
            StartCoroutine(UploadFile(targetElement.Local_TargetPicture_Path,"target.jpg"));
            while (Uploading_File)
                yield return null;
            if(!isFaulted())
            targetElement.TargetPicture_Uploaded = true;
            UpdateElement();
        }
        if (isFaulted())
        {
            Debug.LogError("Error! upload of target picture leaded to failure!");
            syncIcon.GetComponent<Image>().sprite = sprites[1];
            syncIcon.SetActive(true);
            RequestFlowManager.CloseFlow();
            yield break;
        }
        progress = (1.0f / 6.0f) * 3.0f;
        //
        // FIVETH STEP: UPLOADING MEDIA PICTURE TO BE VIEWED WHEN SCANNING
        //
        Debug.Log("target_" + TargetNumber + " regular Picture Uploaded?: " + targetElement.MediaPicture_Uploaded);
        if (!targetElement.MediaPicture_Uploaded && !string.IsNullOrEmpty(targetElement.Local_MediaPicture_Path))
        {
            //TODO: Fix here for MAZ-1
            isSyncRequested = true;
            StopCoroutine(SyncTarget());

            //string[] fileFormat = targetElement.Local_MediaPicture_Path.Split('.');
            //StartCoroutine(UploadFile(targetElement.Local_MediaPicture_Path, "pic."+ fileFormat[fileFormat.Length-1]));
            //while (Uploading_File)
            //    yield return null;
            //if (!isFaulted())
            //    targetElement.MediaPicture_Uploaded = true;
            //UpdateElement();
        }
        if (isFaulted())
        {
            Debug.LogError("Error! upload of picture data leaded to failure!");
            syncIcon.GetComponent<Image>().sprite = sprites[1];
            syncIcon.SetActive(true);
            RequestFlowManager.CloseFlow();
            yield break;
        }
        progress = (1.0f / 6.0f) * 4.0f;
        //
        // SIXTH STEP: UPLOADING VIDEO TO BE VIEWED WHEN SCANNING
        //
        Debug.Log("target_" + TargetNumber + " Video Uploaded?: " + targetElement.Video_Uploaded);
        if (!targetElement.Video_Uploaded && !string.IsNullOrEmpty(targetElement.Local_Video_Path))
        {
            string[] fileFormat = targetElement.Local_Video_Path.Split('.');
            StartCoroutine(UploadFile(targetElement.Local_Video_Path, "video." + fileFormat[fileFormat.Length - 1]));
            while (Uploading_File)
                yield return null;
            if (!isFaulted())
                targetElement.Video_Uploaded = true;
            UpdateElement();
        }
        if (isFaulted())
        {
            Debug.LogError("Error! upload of video file leaded to failure!");
            syncIcon.GetComponent<Image>().sprite = sprites[1];
            syncIcon.SetActive(true);
            RequestFlowManager.CloseFlow();
            yield break;
        }
        progress = (1.0f / 6.0f) * 5.0f;
        //
        // SIXTH STEP: UPLOADING VUFORIA METADATA TO DROPBOX
        //
        Debug.LogWarning("target_" + TargetNumber + " Metadata Uploaded?: " + targetElement.Video_Uploaded);
        if (!targetElement.Metadata_Uploaded)
        {
            byte[] metadata = System.Text.ASCIIEncoding.ASCII.GetBytes(JsonUtility.ToJson(vuforiaMetadata));
            StartCoroutine(UploadFile(metadata, "metadata.txt"));
            while (Uploading_File)
                yield return null;
            if (!isFaulted())
                targetElement.Metadata_Uploaded = true;
            UpdateElement();
        }
        if (isFaulted())
        {
            Debug.LogError("Error! upload of metadata file leaded to failure!");
            syncIcon.GetComponent<Image>().sprite = sprites[1];
            syncIcon.SetActive(true);
            RequestFlowManager.CloseFlow();
            yield break;
        }
        //
        // THIRD STEP: GETTING TARGET STATUS, IF STATUS NEVER RETRIEVED BEFORE.
        //
        if (!targetElement.First_Processing_Done)
        {
            StartCoroutine(GetStatus());
        }
        if (isFaulted())
        {
            Debug.LogError("Error! target status is 'Failed'.");
            syncIcon.GetComponent<Image>().sprite = sprites[1];
            syncIcon.SetActive(true);
            RequestFlowManager.CloseFlow();
            yield break;
        }
        LoadingBar.fillAmount = 1.0f;
        yield return new WaitForSeconds(2.0f);
        LoadingBar.gameObject.SetActive(false);
        StartCoroutine(BlackOut(false));
        gameObject.name = "TargetElement_" + TargetNumber + " - Ready!";
        RequestFlowManager.CloseFlow();
        yield return null;
    }

    IEnumerator UploadFile(byte[] data, string target_name_format)
    {
        Uploading_File = true;
        UnityWebRequest request = UploadManager.UploadFileDropbox(data, targetElement.Unique_ID + "/" + target_name_format);
        yield return new WaitForEndOfFrame();
        yield return request.Send().progress;
        while (!request.isDone)
        {
            LoadingBar.fillAmount = progress + request.uploadProgress / 6.0f;
            yield return null;
        }
        switch (request.responseCode)
        {
            case 200:
                UpdateStatus(TargetStatus.File_Uploaded);
                break;
            default:
                // No Internet Connection!
                Debug.LogError("Couldnt upload the file: " + request.downloadHandler.text);
                UpdateStatus(TargetStatus.File_Upload_Failed);
                break;
        }
        Uploading_File = false;
    }

    IEnumerator UploadFile(string source_path, string target_name_format)
    {
        Uploading_File = true;
        byte[] data;
        try
        {
            data = File.ReadAllBytes(source_path);
        }
        catch
        {
            Debug.Log("Uploadable file doesnt exists!");
            UpdateStatus(TargetStatus.File_Upload_Failed);
            yield break;
        }
        UnityWebRequest request = UploadManager.UploadFileDropbox(data, targetElement.Unique_ID + "/"+ target_name_format);
        yield return new WaitForEndOfFrame();
        yield return request.Send().progress;
        while (!request.isDone)
        {
            LoadingBar.fillAmount = progress + request.uploadProgress/6.0f;
            yield return null;
        }
        switch (request.responseCode)
        {
            case 200:
                UpdateStatus(TargetStatus.File_Uploaded);
                break;
            default:
                // No Internet Connection!
                UpdateStatus(TargetStatus.File_Upload_Failed);
                break;
        }
        Uploading_File = false;
    }

    IEnumerator PostTarget()
    {
        Posting_Target = true;
        targetElement.Last_Modified_Date = string.Format("{0:r}", System.DateTime.Now.ToUniversalTime());
        generateFromLocalTarget();
        UnityWebRequest request = UploadManager.PostNewTarget(targetElement.Local_TargetPicture_Path, vuforiaMetadata);
        yield return new WaitForEndOfFrame();
        yield return request.Send().progress;
        while (!request.isDone)
        {
            LoadingBar.fillAmount = progress + request.uploadProgress / 6.0f;
            yield return null;
        }
        switch (request.responseCode)
        {
            case 201:
                PostTargetResponse lastPost = new PostTargetResponse();
                lastPost = JsonUtility.FromJson<PostTargetResponse>(request.downloadHandler.text);
                if (lastPost != null)
                {
                    targetElement.Unique_ID = lastPost.target_id;
                    UpdateStatus(TargetStatus.Target_Posted);
                }
                break;
            default:
                // No Internet Connection!
                Debug.LogError("POST TARGET ERROR: " + request.downloadHandler.text);
                if (request.downloadHandler.text.Contains("TargetNameExist"))
                {
                    byte[] image = File.ReadAllBytes(targetElement.Local_TargetPicture_Path);
                    Texture2D tex = new Texture2D(2, 2, TextureFormat.RGB24, false);
                    tex.LoadImage(image);
                    image = tex.EncodeToJPG(98);
                    Debug.Log("Image Size Added: " + image.Length);
                    if (image.Length > 1153433)
                    {
                        image = tex.EncodeToJPG(96);
                        Debug.Log("Image New Size: " + image.Length);
                    }
                    targetElement.TargetPicture_Size = image.Length;
                    UpdateElement();
                    image = null;
                    tex = null;
                    Debug.LogError("CONFLICT TARGET NAME: " + "u_" + UserManager.user_id + "t_" + targetElement.TargetPicture_Size);
                    Retrieve_Data_Search_Active = true;
                    StartCoroutine(FindTargetByName("u_" + UserManager.user_id + "t_" + targetElement.TargetPicture_Size));
                }
                break;
        }
        Posting_Target = false;
    }

    IEnumerator UpdateTarget()
    {
        Updating_Target = true;
        targetElement.Last_Modified_Date = string.Format("{0:r}", System.DateTime.Now.ToUniversalTime());
        generateFromLocalTarget();
        UnityWebRequest request = UploadManager.PutUpdateTarget(targetElement.Unique_ID, targetElement.Local_TargetPicture_Path, JsonUtility.ToJson(vuforiaMetadata), targetElement.UpdatePrefix);
        yield return new WaitForEndOfFrame();
        yield return request.Send().progress;
        while (!request.isDone)
        {
            LoadingBar.fillAmount = progress + request.uploadProgress / 6.0f;
            yield return null;
        }
        switch (request.responseCode)
        {
            case 200:
                PostTargetResponse lastPost = new PostTargetResponse();
                lastPost = JsonUtility.FromJson<PostTargetResponse>(request.downloadHandler.text);
                if (lastPost != null)
                {

                    Debug.Log("UPDATE TARGET SUCCESS: " + request.downloadHandler.text);
                    UpdateStatus(TargetStatus.Target_Posted);
                }
                break;
            default:
                // No Internet Connection!
                Debug.LogError("UPDATE TARGET ERROR: " + request.downloadHandler.text);
                UpdateStatus(TargetStatus.Target_Post_Failed);
                break;
        }
        Updating_Target = false;
    }

    IEnumerator FindTargetByName(string targetName)
    {

        Debug.Log("NAME SEARCH TARGET STARTED!");
        UnityWebRequest request = UploadManager.GetTargetsIDs();
        yield return request.Send().progress;
        while (!request.isDone)
        {
            LoadingBar.fillAmount = progress + request.uploadProgress / 6.0f;
            yield return null;
        }
        switch (request.responseCode)
        {
            case 200:
                TargetsIdList targets = new TargetsIdList();
                targets = JsonUtility.FromJson<TargetsIdList>(request.downloadHandler.text);
                for (int i = 0; i < targets.results.Length; i++)
                {
                    if (!RetrieveAllowed())
                    {
                        while (!RetrieveAllowed())
                            yield return null;
                    }
                    StartCoroutine(RetrieveDataByName(targetName, targets.results[i]));
                    if (!Retrieve_Data_Search_Active)
                        break;
                }
                Debug.Log("NAME SEARCH TARGET ENDED SUCCESSFULLY!");
                break;
            default:
                // No Internet Connection!
                Debug.Log("FIND TARGET ERROR: " + request.downloadHandler.text);
                break;
        }
    }

    IEnumerator RetrieveDataByName(string targetName, string targetID)
    {
        UnityWebRequest request;
        request = UploadManager.RetrieveTargetRecord(targetID);
        yield return request.Send().progress;
        while (!request.isDone) yield return null;
        switch (request.responseCode)
        {
            case 200:
                VudoriaResponse lastResp = new VudoriaResponse();
                lastResp = JsonUtility.FromJson<VudoriaResponse>(request.downloadHandler.text);
                if (lastResp.target_record.name == targetName)
                {
                    targetElement.Unique_ID = lastResp.target_record.target_id;
                    UpdateElement();
                    Retrieve_Data_Search_Active = false;
                    Debug.Log("TARGET DATA FOUND BY NAME: " + targetName);
                }
                break;
            default:
                // No Internet Connection!
                Debug.LogError("DATA RETRIEVAL ERROR: " + request.downloadHandler.text);
                break;
        }
        RetrieveEnd();
    }

    public void onClickDown()
    {
        isPressed = true;
        StartCoroutine(onClickDelay());
    }
    public void onClickUp()
    {
        isPressed = false;
    }

    public void setTargetsState(bool on)
    {
        TargetElement[] targets = transform.parent.GetComponentsInChildren<TargetElement>();
        foreach (TargetElement t in targets)
            if(!t.Deleting_Target)
            t.Shake(on);
    }

    public void Shake(bool on)
    {
        shake = on;
        if(on)
        StartCoroutine(shaker());
    }

    IEnumerator shaker()
    {
        Vector2 initialPos = TargetPicture.transform.parent.GetComponent<RectTransform>().anchoredPosition;
        Quaternion initialRot = TargetPicture.transform.parent.rotation;
        Vector2 targetPos = initialPos + new Vector2(Random.Range(-10.0f, +10.0f), Random.Range(-10.0f, +10.0f));
        float zRot = Random.Range(-8.0f, 8.0f);
        while (shake)
        {
            for (float i = 0; i < 1.0f; i += Time.deltaTime*5.0f)
            {
                TargetPicture.transform.parent.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(initialPos, targetPos, i);
                TargetPicture.transform.parent.rotation = Quaternion.Lerp(initialRot, Quaternion.Euler(0, 0, zRot), i);
                yield return null;
            }
            targetPos = initialPos + new Vector2(Random.Range(-6.0f, +6.0f), Random.Range(-6.0f, +6.0f));
            zRot = Random.Range(-3.0f, 3.0f);   
        }
        DeleteButton.gameObject.SetActive(false);
        for (float i = 0; i < 1.0f; i += Time.deltaTime*3.0f)
        {
            TargetPicture.transform.parent.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(targetPos, initialPos, i);
            TargetPicture.transform.parent.rotation = Quaternion.Lerp(Quaternion.Euler(0, 0, zRot), initialRot, i);
            yield return null;
        }
    }

    IEnumerator onClickDelay()
    {
        float t = 0.0f;
        while (isPressed)
        {
            t += Time.deltaTime;
            yield return null;
            if (t >= 1.5f)
                break;
            else
            {

            }
        }
        if (t < 1.5f)
        {
            if (shake)
                setTargetsState(false);
            else if (targetElement.lastStatus != TargetStatus.Target_Processing && targetElement.lastStatus != TargetStatus.Process_Failed)
                EditElement();
            if(targetElement.lastStatus == TargetStatus.Target_Processing)
            {
                Debug.LogWarning("Cannot update target while it's processing!! please wait");
            }
            else if (targetElement.lastStatus == TargetStatus.Process_Failed)
            {
                Debug.LogWarning("Target is corrupted, cannot update!! please delte the target.");
            }
            yield break;
        }
        setTargetsState(true);
        TargetElement[] targets = transform.parent.GetComponentsInChildren<TargetElement>();
        foreach (TargetElement xt in targets)
        {
            xt.DeleteButton.gameObject.SetActive(true);
            xt.DeleteButton.transform.localScale = new Vector3(0, 0, 0);
        }
        for (float i = 0; i < 1.0f; i += Time.deltaTime)
        {
            foreach (TargetElement xt in targets)
                xt.DeleteButton.transform.localScale = Vector3.Lerp(DeleteButton.transform.localScale, new Vector3(1, 1, 1), i);
            yield return null;
        }
    }

    public void DeleteTarget()
    {
        StartCoroutine(deleteTarget());
    }

    IEnumerator deleteTarget()
    {
        Shake(false);
        for (float i = 0.0f; i < 1.0f; i += Time.deltaTime)
        {
            TargetPicture.GetComponent<CanvasRenderer>().SetAlpha(1.0f - i*0.8f);
            yield return null;
        }
        Deleting_Target = true;
        UIInstance.gameObject.SetActive(false);
        string local_target_path = Application.persistentDataPath + slash + UserManager.user_id + slash + "Downloaded" + slash + targetElement.Unique_ID;
        Debug.Log("DELETE PROCESS, Local Folder path: " + local_target_path);
        Debug.Log("DELETE PROCESS, Local Folder exists: " + Directory.Exists(local_target_path));
        Debug.Log("DELETE PROCESS, Deleting UI element...");
        Destroy(TargetPicture.transform.parent.gameObject);
        Debug.Log("DELETE PROCESS, Flow premission requested...");
        if (!RequestFlowManager.SendAllowed())
        {
            gameObject.name = "TargetElement_" + TargetNumber + " - Delete requested...";
            while (!RequestFlowManager.SendAllowed())
                yield return null;
        }
        Debug.Log("DELETE PROCESS, Flow allowed!");
        gameObject.name = "TargetElement_" + TargetNumber + " - DELETING";
        UnityWebRequest request;
        Debug.Log("DELETE PROCESS, Deleting target with ID: " + targetElement.Unique_ID);
        if (!string.IsNullOrEmpty(targetElement.Unique_ID))
        {
            request = UploadManager.RequestDeleteTarget(targetElement.Unique_ID);
            yield return new WaitForEndOfFrame();
            Debug.Log("DELETE REQ SENT, Deleting target with ID: " + targetElement.Unique_ID);
            yield return request.Send().progress;
            while (!request.isDone)
                yield return null;
            switch (request.responseCode)
            {
                case 200:
                    Debug.Log("DELETE PROCESS, Target Deleted successfully!");
                    break;
                default:
                    // No Internet Connection!
                    Debug.LogWarning("DELETE TARGET ERROR: " + request.downloadHandler.text);
                    break;
            }

            Debug.Log("DELETE PROCESS, attempt deleting local folder..");
            if (Directory.Exists(local_target_path))
                Directory.Delete(local_target_path);
            request = null;
            Debug.Log("DELETE FROM DROPBOX, Deleting target with ID: " + targetElement.Unique_ID);
            request = UploadManager.DeleteFileFromDropBox(targetElement.Unique_ID);
            yield return new WaitForEndOfFrame();
            yield return request.Send().progress;
            while (!request.isDone)
                yield return null;
            switch (request.responseCode)
            {
                case 200:
                    Debug.Log("DELETE FROM DROPBOX, Target Deleted successfully!");
                    break;
                default:
                    // No Internet Connection!
                    Debug.LogWarning("DELETE DROPBOX ERROR: " + request.downloadHandler.text);
                    break;
            }
        }
        Debug.Log("DELETE LOCAL FILE: path: " + targetElement.Local_TargetPicture_Path);
        Debug.Log("DELETE LOCAL FILE, Target Image... Conatins id+localdata?: " + targetElement.Local_TargetPicture_Path.Contains(UserManager.user_id + slash + "LocalData") + ", Local file exists?: "+ File.Exists(targetElement.Local_TargetPicture_Path));
        if (File.Exists(targetElement.Local_TargetPicture_Path))
            File.Delete(targetElement.Local_TargetPicture_Path);
        yield return new WaitForEndOfFrame();
        Debug.Log("DELETE LOCAL FILE: path: " + targetElement.Local_MediaPicture_Path);
        Debug.Log("DELETE LOCAL FILE, Media Picture... Conatins id+localdata?: " + targetElement.Local_MediaPicture_Path.Contains(UserManager.user_id + slash + "LocalData") + ", Local file exists?: " + File.Exists(targetElement.Local_MediaPicture_Path));
        if (File.Exists(targetElement.Local_MediaPicture_Path))
            File.Delete(targetElement.Local_MediaPicture_Path);
        yield return new WaitForEndOfFrame();
        Debug.Log("DELETE LOCAL FILE: path: " + targetElement.Local_Video_Path);
        Debug.Log("DELETE LOCAL FILE, Video File... Conatins id+localdata?: " + targetElement.Local_Video_Path.Contains(UserManager.user_id + slash + "LocalData") + ", Local file exists?: " + File.Exists(targetElement.Local_Video_Path));
        if (File.Exists(targetElement.Local_Video_Path))
            File.Delete(targetElement.Local_Video_Path);
        yield return new WaitForEndOfFrame();
        Debug.Log("DELETE LOCAL FILE: path: " + targetElement.Local_TargetPicture_Path);
        Debug.Log("DELETE JSON DATA, Key structure: Key1,TargetNum,TargetNumber");
        PlayerPrefs.DeleteKey(KeyManager.key1 + "_TARGET_NUM_" + TargetNumber);
        yield return new WaitForEndOfFrame();
        Debug.Log("DELETE Process, Ending process, closing flow.");
        RequestFlowManager.CloseFlow();
        Debug.Log("DELETE Process, Deleting target element...");
        Destroy(gameObject);
    }


    IEnumerator GetStatus()
    {
        Processing_Checking = true;
        UnityWebRequest request;
        if (!string.IsNullOrEmpty(targetElement.Unique_ID))
            request = UploadManager.RetrieveTargetRecord(targetElement.Unique_ID);
        else
        {
            UpdateStatus(TargetStatus.Target_StatusRetrieval_Failed);
            Posting_Target = false;
            yield break;
        }
        yield return new WaitForEndOfFrame();
        yield return request.Send().progress;
        while (!request.isDone){}
        switch (request.responseCode)
        {
            case 200:
                VudoriaResponse lastResp = new VudoriaResponse();
                lastResp = JsonUtility.FromJson<VudoriaResponse>(request.downloadHandler.text);
                Debug.Log("Target Status: " + lastResp.status);
                if (lastResp.status == "processing")
                {
                    syncIcon.SetActive(true);
                    UpdateStatus(TargetStatus.Target_Processing);
                    syncIcon.GetComponent<Image>().sprite = sprites[0];
                    yield return new WaitForSeconds(3.0f);
                    StartCoroutine(GetStatus());
                    yield break;
                }
                else if (lastResp.status == "success")
                {
                    // Successful response means you can now open this target in editor.
                    targetElement.First_Processing_Done = true;
                    UpdateStatus(TargetStatus.Process_Done);
                }
                else
                {
                    targetElement.First_Processing_Done = true;
                    Debug.LogError("Target Processing Failed");
                    UpdateStatus(TargetStatus.Process_Failed);
                }
                break;
            default:
                // No Internet Connection!
                UpdateStatus(TargetStatus.Target_StatusRetrieval_Failed);
                break;
        }
        syncIcon.SetActive(false);
        Processing_Checking = false;
    }

    bool isFaulted()
    {
        switch (targetElement.lastStatus)
        {
            case TargetStatus.File_Upload_Failed:
                return true;
            case TargetStatus.Process_Failed:
                return true;
            case TargetStatus.Target_Post_Failed:
                return true;
            case TargetStatus.Target_StatusRetrieval_Failed:
                return true;
        }
        return false;
    }

    public void UpdateStatus(TargetStatus newStatus)
    {
        targetElement.lastStatus = newStatus;
        PlayerPrefs.SetString(KeyManager.key1 + "_TARGET_NUM_" + TargetNumber, JsonUtility.ToJson(targetElement));
    }
    public void UpdateElement()
    {
        PlayerPrefs.SetString(KeyManager.key1 + "_TARGET_NUM_" + TargetNumber, JsonUtility.ToJson(targetElement));
    }

    public void generateFromLocalTarget()
    {
        if (vuforiaMetadata == null)
            vuforiaMetadata = new VuforiaTargetMetadata();
        vuforiaMetadata.video_angle = targetElement.video_angle;
        if (!string.IsNullOrEmpty(targetElement.Local_URL_Link))
            vuforiaMetadata.URL_Path = targetElement.Local_URL_Link;

        if (!string.IsNullOrEmpty(targetElement.Local_MediaPicture_Path))
        {
            string[] splitFormatName = targetElement.Local_MediaPicture_Path.Split('.');
            vuforiaMetadata.MediaPicture_Name = "pic." + splitFormatName[splitFormatName.Length - 1];
        }
        if (!string.IsNullOrEmpty(targetElement.Local_Video_Path))
        {
            string[] splitFormatName = targetElement.Local_Video_Path.Split('.');
            vuforiaMetadata.Video_Name = "video." + splitFormatName[splitFormatName.Length - 1];
        }
        vuforiaMetadata.aspect_ratio = TargetPicture.GetComponent<AspectRatioFitter>().aspectRatio;
        vuforiaMetadata.Last_Modified_Date = string.Format("{0:r}", System.DateTime.Now.ToUniversalTime());
        UpdateElement();
    }
}

public enum TargetStatus
{
    LocalData_Loaded,
    LocalData_Load_Failed,
    Target_Posted,
    Target_Post_Failed,
    File_Uploaded,
    File_Upload_Failed,
    Target_Processing,
    Target_StatusRetrieval_Failed,
    Process_Done,
    Process_Failed
}

public class LocalTargetElement
{
    public string Local_TargetPicture_Path = "";
    public int TargetPicture_Size = 0;
    public float video_angle = 0.0f;
    public bool TargetPicture_Uploaded = false;
    public bool UpdateRequested = false;
    public bool Force_TargetPicture_Update = false;
    public string UpdatePrefix = "";
    public string Local_MediaPicture_Path = "";
    public bool MediaPicture_Uploaded = false;
    public string Local_Video_Path = "";
    public bool Video_Uploaded = false;
    public string Local_URL_Link = "";
    public bool Metadata_Uploaded = false;
    public bool First_Processing_Done = false;
    public string Last_Modified_Date = "";
    public string Unique_ID = "";
    public bool delete_requested = false;
    public TargetStatus lastStatus;
}

public class DropboxTargetElement
{
    public string Dropbox_MediaPicture_Path = "";
    public bool MediaPicture_Exists = true;
    public string Dropbox_Video_Path = "";
    public bool Video_Exists = true;
}

public class VuforiaTargetMetadata
{
    public string MediaPicture_Name = "";
    public string Video_Name = "";
    public string URL_Path = "";
    public string Last_Modified_Date = "";
    public float aspect_ratio = 1.777f;
    public float video_angle = 0.0f;
}
