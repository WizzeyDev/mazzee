﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITransactionManager : MonoBehaviour
{
    public float TransactionsSpeed = 1.0f;
    Vector2 CanvasSize = Vector2.zero;
    private static CanvasRenderer stargetCanvas = null;
    private static CanvasTransaction stransactionType;
    private static bool ssetVisibility;
    private void Start()
    {
        CanvasSize = GetComponent<RectTransform>().sizeDelta;
        Debug.Log("Canvas Size: " + CanvasSize);
    }

    private void Update()
    {
        if (stargetCanvas != null)
        {
            StartCoroutine(Transaction(stargetCanvas, stransactionType, ssetVisibility));
            stargetCanvas = null;
        }
        else return;
    }


    public void DownSizeFadeOut(CanvasRenderer targetCanvas)
    {
        StartCoroutine(Transaction(targetCanvas, CanvasTransaction.FadeZoomOut, false));
    }

    public void DownSizeFadeIn(CanvasRenderer targetCanvas)
    {
        StartCoroutine(Transaction(targetCanvas, CanvasTransaction.FadeZoomOut, true));
    }

    public void GrowFadeOut(CanvasRenderer targetCanvas)
    {
        StartCoroutine(Transaction(targetCanvas, CanvasTransaction.FadeZoomIn, false));
    }

    public void GrowFadeIn(CanvasRenderer targetCanvas)
    {
        StartCoroutine(Transaction(targetCanvas, CanvasTransaction.FadeZoomIn, true));
    }

    public void LeftFadeOut(CanvasRenderer targetCanvas)
    {
        StartCoroutine(Transaction(targetCanvas, CanvasTransaction.FadeLeft, false));
    }

    public void LeftFadeIn(CanvasRenderer targetCanvas)
    {
        StartCoroutine(Transaction(targetCanvas, CanvasTransaction.FadeLeft, true));
    }
    public void RightFadeOut(CanvasRenderer targetCanvas)
    {
        StartCoroutine(Transaction(targetCanvas, CanvasTransaction.FadeRight, false));
    }

    public void RightFadeIn(CanvasRenderer targetCanvas)
    {
        StartCoroutine(Transaction(targetCanvas, CanvasTransaction.FadeRight, true));
    }
    public void UpFadeOut(CanvasRenderer targetCanvas)
    {
        StartCoroutine(Transaction(targetCanvas, CanvasTransaction.FadeTop, false));
    }

    public void UpFadeIn(CanvasRenderer targetCanvas)
    {
        StartCoroutine(Transaction(targetCanvas, CanvasTransaction.FadeTop, true));
    }

    public void DownFadeOut(CanvasRenderer targetCanvas)
    {
        StartCoroutine(Transaction(targetCanvas, CanvasTransaction.FadeBottom, false));
    }

    public void DownFadeIn(CanvasRenderer targetCanvas)
    {
        StartCoroutine(Transaction(targetCanvas, CanvasTransaction.FadeBottom, true));
    }

    public static void Transact(CanvasRenderer targetCanvas, CanvasTransaction transactionType, bool setVisibility)
    {
        stargetCanvas = targetCanvas;
        stransactionType = transactionType;
        ssetVisibility = setVisibility;
    }

     IEnumerator Transaction(CanvasRenderer targetCanvas, CanvasTransaction transactionType, bool setVisibility)
    {
        CanvasRenderer[] children = targetCanvas.GetComponentsInChildren<CanvasRenderer>();
        Vector2 initialPosition = targetCanvas.GetComponent<RectTransform>().anchoredPosition;
        Vector3 initialScale = targetCanvas.transform.localScale;
        Vector3 ZoomInSize = initialScale * 1.45f;
        Vector3 ZoomOutSize = initialScale * 0.45f;
        if (!setVisibility && !targetCanvas.gameObject.active)
            yield break;
        if (setVisibility)
        {
            while (targetCanvas.GetAlpha() < 1.0f)
                yield return null;
            targetCanvas.gameObject.SetActive(true);
            targetCanvas.SetAlpha(0.0f);
            foreach (CanvasRenderer child in children)
                child.SetAlpha(0.0f);
            switch (transactionType)
            {
                case CanvasTransaction.FadeZoomIn:
                    targetCanvas.transform.localScale = ZoomOutSize;
                    break;
                case CanvasTransaction.FadeZoomOut:
                    targetCanvas.transform.localScale = ZoomInSize;
                    break;
                case CanvasTransaction.FadeLeft:
                    targetCanvas.GetComponent<RectTransform>().anchoredPosition = new Vector2(CanvasSize.x, 0);
                    break;
                case CanvasTransaction.FadeRight:
                    targetCanvas.GetComponent<RectTransform>().anchoredPosition = new Vector2(-CanvasSize.x, 0);
                    break;
                case CanvasTransaction.FadeTop:
                    targetCanvas.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -CanvasSize.y);
                    break;
                case CanvasTransaction.FadeBottom:
                    targetCanvas.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, CanvasSize.y);
                    break;
            }
        }
        for (float i = 0; i < TransactionsSpeed; i += Time.deltaTime * TransactionsSpeed)
        {
            if (setVisibility)
            {
                switch (transactionType)
                {
                    case CanvasTransaction.FadeZoomIn:
                        targetCanvas.transform.localScale = Vector3.Lerp(targetCanvas.transform.localScale, initialScale, i);
                        break;
                    case CanvasTransaction.FadeZoomOut:
                        targetCanvas.transform.localScale = Vector3.Lerp(targetCanvas.transform.localScale, initialScale, i);
                        break;
                    default:
                        targetCanvas.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(targetCanvas.GetComponent<RectTransform>().anchoredPosition, initialPosition, i);
                        break;
                }
                targetCanvas.SetAlpha(Mathf.Lerp(targetCanvas.GetAlpha(), 1.0f, i));
                foreach (CanvasRenderer child in children)
                    child.SetAlpha(Mathf.Lerp(targetCanvas.GetAlpha(), 1.0f, i));
            }
            else
            {
                switch (transactionType)
                {
                    case CanvasTransaction.FadeZoomIn:
                        targetCanvas.transform.localScale = Vector3.Lerp(targetCanvas.transform.localScale, ZoomInSize, i);
                        break;
                    case CanvasTransaction.FadeZoomOut:
                        targetCanvas.transform.localScale = Vector2.Lerp(targetCanvas.transform.localScale, ZoomOutSize, i);
                        break;
                    case CanvasTransaction.FadeLeft:
                        targetCanvas.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(targetCanvas.GetComponent<RectTransform>().anchoredPosition, initialPosition-new Vector2(CanvasSize.x, 0.0f), i);
                        break;
                    case CanvasTransaction.FadeRight:
                        targetCanvas.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(targetCanvas.GetComponent<RectTransform>().anchoredPosition, initialPosition + new Vector2(CanvasSize.x, 0.0f), i);
                        break;
                    case CanvasTransaction.FadeTop:
                        targetCanvas.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(targetCanvas.GetComponent<RectTransform>().anchoredPosition, initialPosition + new Vector2(0.0f,CanvasSize.y), i);
                        break;                                                                                                                                                                    
                    case CanvasTransaction.FadeBottom:                                                                                                                                            
                        targetCanvas.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(targetCanvas.GetComponent<RectTransform>().anchoredPosition, initialPosition - new Vector2(0.0f, CanvasSize.y), i);
                        break;
                }
                targetCanvas.SetAlpha(Mathf.Lerp(targetCanvas.GetAlpha(), 0.0f, i));
                foreach (CanvasRenderer child in children)
                    child.SetAlpha(Mathf.Lerp(targetCanvas.GetAlpha(), 0.0f, i));
            }
            yield return null;
        }
        targetCanvas.SetAlpha(1.0f);
        foreach (CanvasRenderer child in children)
            child.SetAlpha(1.0f);
        targetCanvas.GetComponent<RectTransform>().anchoredPosition = initialPosition;
        targetCanvas.transform.localScale = initialScale;
        if (!setVisibility)
        {
            targetCanvas.gameObject.SetActive(false);
        }
    }

    public enum CanvasTransaction { FadeZoomIn, FadeZoomOut, FadeLeft, FadeRight, FadeTop, FadeBottom, Fade };
}
