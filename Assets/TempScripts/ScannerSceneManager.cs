﻿using System.Collections;
using System.Collections.Generic;
using Vuforia;
using UnityEngine;

public class ScannerSceneManager : MonoBehaviour {

    // Use this for initialization
    public GameObject[] LogoKeyScannerActivity;
    public GameObject[] ScannerActivity;
    public GameObject[] NativeCameraObjects;
    public CloudRecoBehaviour cloudReco;
    void Start () {
        KeyManager.load();
            cloudReco.AccessKey = KeyManager.key1;
            cloudReco.SecretKey = KeyManager.key2;
        Screen.orientation = ScreenOrientation.AutoRotation;
        if (PlayerPrefs.GetString("SCENE_STATE", "SCANNER") == "SCANNER")
        {
            startAsScanner();
            TipMessage.showTip("Scan a picture target", 4.0f);
        }
        else if (PlayerPrefs.GetString("SCENE_STATE", "SCANNER") == "ACCESS")
        {
            startAsServerAccessScanner();
            TipMessage.showTip("Scan company logo, or server key", 5.0f);
        }
        else startAsNativeCamera();
    }

    public void receive_package(string pack)
    {
        for (int i = 0; i < ScannerActivity.Length; i++)
            ScannerActivity[i].SetActive(true);
        for (int i = 0; i < LogoKeyScannerActivity.Length; i++)
            LogoKeyScannerActivity[i].SetActive(true);
        for (int i = 0; i < NativeCameraObjects.Length; i++)
            NativeCameraObjects[i].SetActive(true);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Application.Quit();
        KeyManager.load();
        cloudReco.AccessKey = KeyManager.key1;
        cloudReco.SecretKey = KeyManager.key2;
        Screen.orientation = ScreenOrientation.AutoRotation;
    }

    public void showCustomTip(string tip_text)
    {
        TipMessage.showTip(tip_text, 5.0f);
    }

    void startAsScanner()
    {
        for (int i = 0; i < ScannerActivity.Length; i++)
            ScannerActivity[i].SetActive(true);
    }

    void startAsServerAccessScanner()
    {
        for (int i = 0; i < LogoKeyScannerActivity.Length; i++)
            LogoKeyScannerActivity[i].SetActive(true);
    }

    void startAsNativeCamera()
    {
        for (int i = 0; i < NativeCameraObjects.Length; i++)
            NativeCameraObjects[i].SetActive(true);
    }
}
