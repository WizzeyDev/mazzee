﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIRectSlide : MonoBehaviour
{
    bool isZero = true;
    public bool targetRequest = true;
    public Vector2 targetPos = Vector2.zero;
    private RectTransform me;
    Coroutine sliding;
    // Start is called before the first frame update
    void Start()
    {
        me = GetComponent<RectTransform>();
    }

    private void Update()
    {
        if (targetRequest != isZero)
        {
            switchSilde();
        }
        else return;
    }

    public void switchSilde()
    {
        isZero = !isZero;
        targetRequest = isZero;
        if (sliding != null)
            StopCoroutine(sliding);
        sliding = StartCoroutine(moveSlider(isZero));
    }

    public void switchSilde(bool zero)
    {
        gameObject.SetActive(true);
        isZero = zero;
        if (sliding != null)
            StopCoroutine(sliding);
        sliding = StartCoroutine(moveSlider(zero));
    }

    IEnumerator moveSlider(bool zero)
    {
        for (float i = 0; i < 1.0f; i += Time.deltaTime)
        {
            if(!zero)
            me.anchoredPosition = Vector2.Lerp(me.anchoredPosition, targetPos, i);
            else me.anchoredPosition = Vector2.Lerp(me.anchoredPosition, Vector2.zero, i);
            yield return null;
        }
    }
}
