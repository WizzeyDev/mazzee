﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
//using Firebase.Auth;
// Include Facebook namespace
using Facebook.Unity;

public class LoginManager : MonoBehaviour
{
    public UnityEvent ShowHome;
    public UnityEvent ShowLoginReqired;
    public UnityEvent NewTarget;
    public UnityEvent ShowList;
    public EditPicture editorRef;
    public RawImage avatar;
    private int lastAction = 0;
    // No Action 0 - shows home screen.
    // Show list 1 - shows the targets list.
    // Create new 2 - Creates new target;
    // Start is called before the first frame update
    public string googleIdToken = "34254111402-bhvcv1gga8fgfeof7ank70b3vo6jll2t.apps.googleusercontent.com";
    public string googleAccessToken = "AIzaSyAJwBRGbSYAXUlAZOpJCEkMqwfnwZZtG8M";

    private string mFBPotoURL;
    private static LoginManager instance;

    void Awake()
    {
        instance = this;
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
    }

    public static void FetchProfilePic()
    {
        if (FB.IsInitialized)
        {
            instance.StartCoroutine(instance.fetchProfilePic(instance.mFBPotoURL)); //Call the coroutine to download the photo
        }
        else
        {
            Debug.Log("<color=red> FB aint connected Cant fetch Pic </color>");
        }
    }

    public void logOut()
    {
        UserManager.UpdateId("");
        FB.LogOut();
        //Application.LoadLevel(0);
    }

    public void showTargetList()
    {
        if (string.IsNullOrEmpty(UserManager.user_id))
        {
            lastAction = 1;
            ShowLoginReqired.Invoke();
        }
        else ShowList.Invoke();
    }

    public void createNewTarget()
    {
        if (string.IsNullOrEmpty(UserManager.user_id))
        {
            lastAction = 2;
            ShowLoginReqired.Invoke();
        }
        else NewTarget.Invoke();
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
            if (FB.IsLoggedIn)
            {
                FB.API("/me/picture?redirect=false", HttpMethod.GET, ProfilePhotoCallback);
            }
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void ProfilePhotoCallback(IGraphResult result)
    {
        if (string.IsNullOrEmpty(result.Error) && !result.Cancelled)
        { //if there isn't an error
            IDictionary data = result.ResultDictionary["data"] as IDictionary; //create a new data dictionary
            mFBPotoURL = data["url"] as string; //add a URL field to the dictionary
            StartCoroutine(fetchProfilePic(mFBPotoURL)); //Call the coroutine to download the photo
        }
    }

    private IEnumerator fetchProfilePic(string url)
    {
        WWW www = new WWW(url); //use the photo url to get the photo from the web
        yield return www; //wait until it has downloaded
        avatar.texture = www.texture; //set your profilePic Image Component's sprite to the photo
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    public void loginTest()
    {
        List<string> permissions = new List<string>();
        permissions.Add("public_profile");
        permissions.Add("email");
        permissions.Add("user_friends");
        FB.LogInWithReadPermissions(permissions, AuthCallback);
    }



    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
           // Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
            List<string> permissions = new List<string>();

            // AccessToken class will have session details
            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            
            Debug.Log("User ID: " + aToken.UserId);
            UserManager.UpdateId(aToken.UserId);
            // Print current access token's granted permissions
            foreach (string perm in aToken.Permissions)
            {
                Debug.Log(perm);
            }
            FB.API("/me/picture?redirect=false", HttpMethod.GET, ProfilePhotoCallback);
            //editorRef.LoadLocalTargets();
            if (lastAction == 1)
            {
                ShowList.Invoke();
                editorRef.LoadLocalTargets();
            }
            else if (lastAction == 2)
                NewTarget.Invoke();
        }
        else
        {
            Debug.Log("User cancelled login");
        }
    }

    public void testInvokeNewTarget()
    {
        NewTarget.Invoke();
    }

    public void testInvokeTargetList()
    {
        ShowList.Invoke();
    }
}
