﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FoldUI : MonoBehaviour
{
    // Start is called before the first frame update
    public Vector2 MinMaxDelta = new Vector2(45.0f, 365.0f);
    public UnityEvent onShown;
    public UnityEvent onHidden;
    bool visible = false;
    RectTransform me;
    Coroutine anim;
    void Start()
    {
        me = GetComponent<RectTransform>();
    }

    public void SwitchVisible()
    {
        visible = !visible;
        if (anim != null)
            StopCoroutine(anim);
        anim = StartCoroutine(switchFold(visible));
    }

    IEnumerator switchFold(bool show)
    {
        visible = show;
        if (!show)
            onHidden.Invoke();
        else
            onShown.Invoke();
        for (float i = 0; i < 1.0f; i += Time.deltaTime)
        {
            if (show)
                me.sizeDelta = Vector2.Lerp(me.sizeDelta, new Vector2(MinMaxDelta.y, me.sizeDelta.y), i);
            if (!show)
                me.sizeDelta = Vector2.Lerp(me.sizeDelta, new Vector2(MinMaxDelta.x, me.sizeDelta.y), i);
            yield return null;
        }
    }
}
